	solution "Hostility"
		configurations { "Debug", "Release" }
		
		flags { "StaticRuntime", "No64BitChecks", "Symbols", "Unicode" }
		
		flags { "NoIncrementalLink", "NoEditAndContinue" }
		
		configuration "Debug*"
			targetdir "bin/debug"
			defines "NDEBUG"
			
		configuration "Release*"
			targetdir "bin/release"
			defines "NDEBUG"
			optimize "Speed"
			
	project "Hostility"
		targetname "RIL.Hostility"
		language "C++"
		kind "SharedLib"
		
		files
		{
			"src/**.cpp", "src/**.h"
		}
		
		includedirs { "deps/include/opengl/" }
		libdirs { "deps/lib/opengl/debug/" }
		
		links { "rwcore.lib", "rpworld.lib", "rtpng.lib", "rpskin.lib", "rpanisot.lib", "rphanim.lib", "rtanim.lib", "rpuvanim.lib", "rpmatfx.lib", "rtdict.lib", "rtquat.lib", "rtpng.lib", "regal32.lib" }
		
		postbuildcommands { "copy \"$(TargetPath)\" \"Q:\\Games\\GTA San Andreas\\RIL.Hostility.asi\"" }
				
