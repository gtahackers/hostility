#include "StdInc.h"
#include <assert.h>
#include <sstream>
#include "ScriptRuntime.h"

ScriptParameter scriptParameters[32];
int numScriptParameters;

void HostilityScript::Init(HostilityScriptRuntime* runtime)
{
	m_runtime = runtime;
	m_baseScript = runtime->GetScriptSpace();
	m_curPC = 0;
	m_andOrValue = 0;
	m_wakeTime = 0;

	memset(m_localVars, 0, sizeof(m_localVars));
}

void HostilityScript::Process()
{
	float frameTime = *(float*)0xB7CB58 * 0.02 * 1000;

	m_localVars[16].integer += frameTime;
	m_localVars[17].integer += frameTime;

	if (m_wakeTime > 0)
	{
		m_wakeTime -= frameTime;

		if (m_wakeTime <= 0)
		{
			m_wakeTime = 0;
		}
	}

	if (m_wakeTime > 0)
	{
		return;
	}

	while (!ProcessOneCommand());
}

void HostilityScript::RechainCommand(uint32_t commandID)
{
	m_rechain = true;

	m_runtime->GetChain()->ProcessTasks(commandID, this);
}

bool HostilityScript::ProcessOneCommand()
{
	m_waiting = false;

	bool not = false;
	uint16_t command = *(uint16_t*)&m_baseScript[GetPC()];
	IncrementPC(2);

	if (command & 0x8000)
	{
		command &= ~0x8000;
		not = true;
	}

	bool oldCF = m_compareFlag;

	if (command == 0x04C4)
	{
//		__asm int 3
	}

	m_tailCBs.clear();

	m_rechain = false;

	//printf("0x%04x => running\n", command);
	m_runtime->GetChain()->ProcessTasks(command, this);

	for (auto& cb : m_tailCBs)
	{
		cb();
	}

	if (command != 0x00D6)
	{
		if (not)
		{
			m_compareFlag = !m_compareFlag;
		}

		if (m_andOrValue > 0)
		{
			if (m_andOrValue > 20)
			{
				m_compareFlag = oldCF || m_compareFlag;

				m_andOrValue--;
			}
			else
			{
				m_compareFlag = oldCF && m_compareFlag;

				m_andOrValue--;
			}

			if (m_andOrValue == 20)
			{
				m_andOrValue = 0;
			}
		}
	}

	return m_waiting;
}

void HostilityScript::GoSub(int sub)
{
	m_returnStack.push(GetPC());
	SetPC(sub);
}

void HostilityScript::Return()
{
	SetPC(m_returnStack.top());
	m_returnStack.pop();
}

void HostilityScript::SetAndOr(int value)
{
	if (value > 0)
	{
		m_andOrValue = value + 1;

		if (value > 20)
		{
			m_compareFlag = false;
		}
		else
		{
			m_compareFlag = true;
		}
	}
}

void HostilityScript::CollectParameters(int numParams)
{
	if (m_rechain)
	{
		return;
	}

	numScriptParameters = numParams;

	if (numParams == 0)
	{
		return;
	}

	int i = 0;

	while (true)
	{
		if (numParams > 0 && i >= numParams)
		{
			break;
		}
		
		if (numParams == -1)
		{
			if (m_baseScript[m_curPC] == 0)
			{
				numScriptParameters = i;

				IncrementPC(1);
				break;
			}
		}

		scriptParameters[i].ReadFromScript(this);

		i++;
	}
}

void HostilityScript::SetTailCB(std::function<void()> cb)
{
	m_tailCBs.push_back(cb);
}

void HostilityScript::InvokeGameCommand(int gameID)
{
	char scriptBuffer[350];
	int i = 0;

	scriptBuffer[i++] = gameID & 0xFF;
	scriptBuffer[i++] = (gameID >> 8) & 0xFF;

	for (int p = 0; p < numScriptParameters; p++)
	{
		auto param = scriptParameters[p];

		i = param.AddToBuffer(scriptBuffer, i);
	}

	if (gameID == 0x016A) // DO_FADE
	{
		printf("fading: %s\n", m_name.c_str());
	}

	if (gameID == 0x0214) // DO_FADE
	{
		printf("pickup check: %s -> %d\n", scriptParameters[0].GetId().c_str(), scriptParameters[0].GetIntValue());
	}

	if (gameID == 0x14C)
	{
		//return;
	}

	if (gameID == 0x23C)
	{
		__asm int 3
	}

	m_compareFlag = CRunningScript::ProcessBuffer(m_name, scriptBuffer, (int*)m_localVars);
	//m_compareFlag = false;
	//printf("%04x\n", gameID);
}

std::string ScriptParameter::GetId()
{
	std::stringstream ss;

	if (m_type == Type::GlobalVar)
	{
		ss << "$" << (m_value.integer / 4);
	}
	else if (m_type == Type::LocalVar)
	{
		ss << (m_value.integer) << "@";
	}

	return ss.str();
}

int ScriptParameter::AddToBuffer(char* buffer, int i)
{
	switch (m_type)
	{
		case Type::Integer:
		case Type::Integer16:
		case Type::Integer8:
			buffer[i++] = 1;
			*(int*)(&buffer[i]) = m_value.integer;

			i += 4;
			break;

		case Type::Float:
			buffer[i++] = 6;
			*(float*)(&buffer[i]) = m_value.number;

			i += 4;
			break;

		case Type::GlobalVar:
			// NOTE: this requires changing of the 'put global variable' thing in the game to use our vars when needed
			buffer[i++] = 2;

			*(uint16_t*)(&buffer[i]) = m_value.integer;

			i += 2;
			break;

		case Type::LocalVar:
			buffer[i++] = 3;

			*(uint16_t*)(&buffer[i]) = m_value.integer;

			i += 2;
			break;

		case Type::String:
			buffer[i++] = 0x9;
			memcpy(&buffer[i], m_value.string, 8);

			i += 8;
			break;
	}

	return i;
}

void ScriptParameter::ReadFromScript(HostilityScript* script)
{
	m_script = script;

	char* scriptBase = m_script->GetScript();
	char type = scriptBase[m_script->GetPC()];

	if (type <= 6)
	{
		m_script->IncrementPC(1);

		switch (type)
		{
			case 1:
				m_type = Type::Integer;
				m_value.integer = *(int*)&scriptBase[m_script->GetPC()];

				m_script->IncrementPC(4);
				break;

			case 2:
				m_type = Type::GlobalVar;
				m_value.integer = *(uint16_t*)&scriptBase[m_script->GetPC()]; // raw bytes

				m_script->IncrementPC(2);
				break;

			case 3:
				m_type = Type::LocalVar;
				m_value.integer = *(uint16_t*)&scriptBase[m_script->GetPC()]; // index into array, aka not *4'd

				m_script->IncrementPC(2);
				break;
				
			case 4:
				m_type = Type::Integer8;
				m_value.integer = *(char*)&scriptBase[m_script->GetPC()];

				m_script->IncrementPC(1);
				break;

			case 5:
				m_type = Type::Integer16;
				m_value.integer = *(short*)&scriptBase[m_script->GetPC()];

				m_script->IncrementPC(2);
				break;

			case 6:
				m_type = Type::Float;
				m_value.number = *(float*)&scriptBase[m_script->GetPC()];

				m_script->IncrementPC(4);
				break;
		}
	}
	else
	{
		m_type = Type::String;

		strncpy(m_value.string, &scriptBase[m_script->GetPC()], 8);
		m_script->IncrementPC(8);
	}
}

const char* ScriptParameter::GetStringValue()
{
	switch (m_type)
	{
		case Type::String:
			return m_value.string;

		default:
			assert(!"invalid string type");
	}
}

int ScriptParameter::GetIntValue()
{
	switch (m_type)
	{
		case Type::Float:
			return m_value.number;

		case Type::GlobalVar:
			return *(int*)(&m_script->GetRuntime()->GetScriptSpace()[m_value.integer]);

		case Type::LocalVar:
			return m_script->GetLocalVars()[m_value.integer].integer;

		case Type::Integer:
		case Type::Integer16:
		case Type::Integer8:
			return m_value.integer;
	}
}

float ScriptParameter::GetFloatValue()
{
	switch (m_type)
	{
		case Type::Float:
			return m_value.number;

		case Type::GlobalVar:
			return *(float*)(&m_script->GetRuntime()->GetScriptSpace()[m_value.integer]);

		case Type::LocalVar:
			return m_script->GetLocalVars()[m_value.integer].number;

		case Type::Integer:
		case Type::Integer16:
		case Type::Integer8:
			return m_value.integer;
	}
}

void ScriptParameter::SetFloatValue(float value)
{
	switch (m_type)
	{
		case Type::GlobalVar:
			*(float*)(&m_script->GetRuntime()->GetScriptSpace()[m_value.integer]) = value;
			break;

		case Type::LocalVar:
			m_script->GetLocalVars()[m_value.integer].number = value;
			break;

		default:
			assert(!"not a valid type to set");
	}
}

void ScriptParameter::SetIntValue(int value)
{
	switch (m_type)
	{
		case Type::GlobalVar:
			*(int*)(&m_script->GetRuntime()->GetScriptSpace()[m_value.integer]) = value;
			break;

		case Type::LocalVar:
			m_script->GetLocalVars()[m_value.integer].integer = value;
			break;

		default:
			assert(!"not a valid type to set");
	}
}