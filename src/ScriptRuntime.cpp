#include "StdInc.h"
#include "ScriptRuntime.h"
#include <assert.h>

HostilityScriptRuntime::HostilityScriptRuntime()
{
	memset(m_pIdleScripts, 0, sizeof(m_pIdleScripts));
	memset(m_pActiveScripts, 0, sizeof(m_pActiveScripts));

	memset(m_scriptSpace, 0, sizeof(m_scriptSpace));
}

void HostilityScriptRuntime::Process()
{
	for (auto& script : m_pActiveScripts)
	{
		if (script)
		{
			script->Process();
		}
	}
}

void HostilityScriptRuntime::Init()
{
	// initialize script list
	for (int i = 0; i < _countof(m_scriptsArray); i++)
	{
		m_scriptsArray[i].Init(this);
		m_scriptsArray[i].AddScriptToList(m_pIdleScripts);
	}

	// read multiscript file
	FILE* file = fopen("data/main.scm", "rb");
	fread(m_scriptSpace, 1, sizeof(m_scriptSpace), file);
	fclose(file);

	ReadMultiScriptFileOffsetsFromScript();

	// set chain
	m_chain = GetScriptChainFactory()->GetChain();
}

void HostilityScriptRuntime::StopScript(HostilityScript* script)
{
	script->RemoveScriptFromList(m_pActiveScripts);
	script->AddScriptToList(m_pIdleScripts);

	script->Wait(0);
}

void HostilityScriptRuntime::ReadMultiScriptFileOffsetsFromScript()
{
	int objectStart = *(int32_t*)&m_scriptSpace[3];
	int multiScriptStart = *(int32_t*)&m_scriptSpace[objectStart + 3];

	m_mainScriptSize = *(int*)&m_scriptSpace[multiScriptStart + 8];
	m_largestMissionScriptSize = *(int*)&m_scriptSpace[multiScriptStart + 8 + 4];
	m_numberOfMissionScripts = *(int*)&m_scriptSpace[multiScriptStart + 8 + 8];

	for (int i = 0; i < m_numberOfMissionScripts; i++)
	{
		m_multiScriptArray[i] = *(int*)&m_scriptSpace[multiScriptStart + 8 + 12 + (i * 4)];
	}
}

int HostilityScriptRuntime::GetMainStart()
{
	int objectStart = *(int32_t*)&m_scriptSpace[3];
	int multiScriptStart = *(int32_t*)&m_scriptSpace[objectStart + 3];
	int mainScriptStart = *(int32_t*)&m_scriptSpace[multiScriptStart + 3];

	return mainScriptStart;
}

void HostilityScriptRuntime::StartTestScript()
{
	StartNewScript(0);
}

void HostilityScriptRuntime::StartNewScript(int offset)
{
	HostilityScript** list = m_pIdleScripts;

	while (*list == 0)
	{
		list++;
	}

	HostilityScript* script = *list;
	script->RemoveScriptFromList(m_pIdleScripts);

	script->Init(this);
	script->SetPC(offset);

	script->AddScriptToList(m_pActiveScripts);
}

void HostilityScript::AddScriptToList(HostilityScript** list)
{
	int i = 0;

	while (*list != 0)
	{
		i++;
		list++;
	}

	assert(i < 128);

	*list = this;
}

void HostilityScript::RemoveScriptFromList(HostilityScript** list)
{
	int i = 0;

	while (*list != this)
	{
		assert(i < 128);

		i++;
		list++;
	}

	*list = 0;
}

static HostilityScriptRuntime* g_runtime;
static char* g_curScriptBlock = (char*)0xA49960;

void ScriptProcess()
{
	g_curScriptBlock = (char*)0xA49960;
	((void(*)())0x46A000)();

	g_curScriptBlock = g_runtime->GetScriptSpace();
	g_runtime->Process();

	g_curScriptBlock = (char*)0xA49960;
}

void ScriptStart()
{
	g_runtime->StartTestScript();
}

void ScriptInit()
{
	if (g_runtime)
	{
		delete g_runtime;
	}

	g_runtime = new HostilityScriptRuntime();
	g_runtime->Init();
}

void __declspec(naked) GetScriptSpace()
{
	__asm
	{
		add eax, g_curScriptBlock

		retn
	}
}

void __declspec(naked) GetScriptSpace2()
{
	__asm
	{
		mov edx, g_curScriptBlock
		mov edx, dword ptr [edx + eax]
		retn
	}
}

void __declspec(naked) GetScriptSpace3()
{
	__asm
	{
		push edx
		mov edx, g_curScriptBlock
		mov eax, dword ptr [edx + eax]
		pop edx

		retn
	}
}

void __declspec(naked) GetScriptSpace4()
{
	__asm
	{
		push ecx
		mov ecx, g_curScriptBlock
		mov dword ptr [ecx + eax], edx
		pop ecx

		retn
	}
}

void ScriptRuntimeInit()
{
	using namespace MemoryVP;

	Nop(0x4643C0, 6);
	InjectHook(0x4643C0, GetScriptSpace4, PATCH_CALL);

	Nop(0x4640DE, 6);
	InjectHook(0x4640DE, GetScriptSpace2, PATCH_CALL);

	InjectHook(0x4647CA, GetScriptSpace, PATCH_CALL);

	Nop(0x46429A, 6);
	InjectHook(0x46429A, GetScriptSpace3, PATCH_CALL);

	InjectHook(0x53BFC7, ScriptProcess, PATCH_CALL);
	InjectHook(0x464D4B, ScriptStart, PATCH_JUMP);
	InjectHook(0x469386, ScriptInit, PATCH_CALL);
	InjectHook(0x46938B, 0x4684F0, PATCH_JUMP);
}