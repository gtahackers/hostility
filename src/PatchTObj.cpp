#include "StdInc.h"

#define NUM_TIMED_OBJECTS 750
#define NUM_OBJECTS 28000

void PatchObjects();

// lazy
void PatchTimedObjects()
{
	PatchObjects();

	using namespace MemoryVP;

	DWORD dwAlloc = (DWORD)malloc((0x24 * NUM_TIMED_OBJECTS) + 4);

	memset((LPVOID)dwAlloc, 0x00, (0x24 * NUM_TIMED_OBJECTS) + 4);
	for (DWORD i = dwAlloc + 4; i < (dwAlloc + 4 + (0x24 * NUM_TIMED_OBJECTS)); i += 0x24)
	{
		*(BYTE*)i = 0xB0;
		*(BYTE*)(i + 1) = 0xBC;
		*(BYTE*)(i + 2) = 0x85;
		*(BYTE*)(i + 10) = 0xFF;
		*(BYTE*)(i + 11) = 0xFF;
		*(BYTE*)(i + 34) = 0xFF;
		*(BYTE*)(i + 35) = 0xFF;
	}
//		char* cBytes = (char*)&dwAlloc;
	//BYTE bTimedObjects[] = { cBytes[0], cBytes[1], cBytes[2], cBytes[3] };
	Patch(0x4C66B1, dwAlloc);
	Patch(0x4C66C2, dwAlloc);
	Patch(0x84BC51, dwAlloc);
	Patch(0x856261, dwAlloc);
	Patch(0x4C683B, dwAlloc);
		
	dwAlloc += 4;

	Patch(0x4C6464, dwAlloc);
	Patch(0x4C66BD, dwAlloc);

	Patch(0x4C58A6, NUM_TIMED_OBJECTS);
}

void PatchObjects()
{
	using namespace MemoryVP;

	DWORD dwAlloc = (DWORD)malloc((0x20 * NUM_OBJECTS) + 4);
	memset((LPVOID)dwAlloc, 0x00, (0x20 * NUM_OBJECTS) + 4);
	for (DWORD i = dwAlloc + 4; i < (dwAlloc + 4 + (NUM_OBJECTS * 0x20)); i += 0x20)
	{
		*(BYTE*)i = 0xF0;
		*(BYTE*)(i + 1) = 0xBB;
		*(BYTE*)(i + 2) = 0x85;
		*(BYTE*)(i + 10) = 0xFF;
		*(BYTE*)(i + 11) = 0xFF;
	}
	Patch(0x4C6621, dwAlloc);
	Patch(0x4C6633, dwAlloc);
	Patch(0x4C63E1, dwAlloc);
	Patch(0x4C63FE, dwAlloc);
	Patch(0x84BBF1, dwAlloc);
	Patch(0x856231, dwAlloc);
	Patch(0x4C6865, dwAlloc);
	Patch(0x4C689A, dwAlloc);
	Patch(0x4C68B5, dwAlloc);
	Patch(0x4C68E8, dwAlloc);
	Patch(0x4C68F9, dwAlloc);
	Patch(0x4C6927, dwAlloc);
	Patch(0x4C6938, dwAlloc);
	Patch(0x4C6966, dwAlloc);
	Patch(0x4C6977, dwAlloc);
	Patch(0x4C69A5, dwAlloc);
	Patch(0x4C69B6, dwAlloc);
	Patch(0x4C69E4, dwAlloc);
	Patch(0x4C69F5, dwAlloc);
	Patch(0x4C6A23, dwAlloc);
	Patch(0x4C6A34, dwAlloc);
	Patch(0x4C6865, dwAlloc);
	dwAlloc += 4;
	Patch(0x4C662D, dwAlloc);
	Patch(0x4C6822, dwAlloc);
	Patch(0x4C6829, dwAlloc);
	Patch(0x4C6877, dwAlloc);
	Patch(0x4C6881, dwAlloc);
	Patch(0x4C6890, dwAlloc);
	Patch(0x4C68A5, dwAlloc);
	Patch(0x4C68F3, dwAlloc);
	Patch(0x4C6932, dwAlloc);
	Patch(0x4C6971, dwAlloc);
	Patch(0x4C69B0, dwAlloc);
	Patch(0x4C69EF, dwAlloc);
	Patch(0x4C6A2E, dwAlloc);
	Patch(0x4C63F2, dwAlloc);
	dwAlloc += 24;
	Patch(0x4C68AC, dwAlloc);
	
	Patch(0x4C5846, NUM_OBJECTS);
	Patch(0x4C5CBC, NUM_OBJECTS);

	// info zones
	char* newZones = new char[600 * 32];

	for (char* ptr = (char*)0x400000; ptr < (char*)0x600000; ptr++)
	{
		if (*(int*)ptr == 0xBA3798 || *(int*)ptr == 0xBA37A0 || *(int*)ptr == 0xBA37A8 || *(int*)ptr == 0xBA37AA || *(int*)ptr == 0xBA37AC || *(int*)ptr == 0xBA37AE || *(int*)ptr == 0xBA37B0 || *(int*)ptr == 0xBA37B2 || *(int*)ptr == 0xBA37B4 || *(int*)ptr == 0xBA37B6 || *(int*)ptr == 0xBA37B7 ||
			*(int*)ptr == (0xBA3798 + 32) || *(int*)ptr == (0xBA37A0 + 32) || *(int*)ptr == (0xBA37A8 + 32) || *(int*)ptr == (0xBA37AA + 32) || *(int*)ptr == (0xBA37AC + 32) || *(int*)ptr == (0xBA37AE + 32) || *(int*)ptr == (0xBA37B0 + 32) || *(int*)ptr == (0xBA37B2 + 32) || *(int*)ptr == (0xBA37B4 + 32) || *(int*)ptr == (0xBA37B6 + 32) || *(int*)ptr == (0xBA37B7 + 32))
		{
			int val = *(int*)ptr;
			val -= 0xBA3798;
			val += (int)newZones;

			Patch(ptr, val);
		}
	}

	// don't give gang weapons (crash workaround)
	Nop(0x612968, 35);

	// car generators
	char* carGens = new char[500 * 32];
	memset(carGens, 0, sizeof(carGens));

	for (char* ptr = (char*)0x400100; ptr <= (char*)0x700000; ptr++)
	{
		if (*(int*)ptr == 0xC27AD0 || *(int*)ptr == (0xC27AD0 + 0x1D))
		{
			int val = *(int*)ptr;
			val -= 0xC27AD0;
			val += (int)carGens;

			Patch(ptr, val);
		}

		if (*(int*)ptr == 0xC2B96D)
		{
			Patch(ptr, &carGens[(500 * 32) + 0x1D]);
		}
	}
}