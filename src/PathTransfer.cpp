#include "StdInc.h"

static float g_pathCenterX;
static float g_pathCenterY;
static float g_pathCenterXPos;
static float g_pathCenterYPos;
static char g_curPathPattern[256];

class CPathFind
{
public:
	void LoadPathFindData(int pathIdx);
	void UnloadPathFindData(int pathIdx);
};

CPathFind& ThePaths = *(CPathFind*)0x96F050;

void WRAPPER CPathFind::LoadPathFindData(int pathIdx) { EAXJMP(0x452F40); }
void WRAPPER CPathFind::UnloadPathFindData(int pathIdx) { EAXJMP(0x44D0F0); }

void LoadPathFindData(int modelIdx, int priority)
{
	int pathIdx = modelIdx - STREAMING_NODE_MIN;

	ThePaths.LoadPathFindData(pathIdx);
}

void UnloadPathFindData(int modelIdx)
{
	int pathIdx = modelIdx - STREAMING_NODE_MIN;

	ThePaths.UnloadPathFindData(pathIdx);
}

WRAPPER CVector* FindPlayerCoors(CVector*, int) { EAXJMP(0x56E010); }

CVector* UpdateStreaming_FindPlayerCoorsHook(CVector* out, int playerIdx)
{
	CVector* retval = FindPlayerCoors(out, playerIdx);

	if (retval->x > 3200.f)
	{
		g_pathCenterX = (3200.f + 2300.f) - 3000.0f;
		g_pathCenterY = -3000.f;

		strcpy(g_curPathPattern, "data\\paths\\nodes%d.vc");
	}
	else
	{
		g_pathCenterX = -3000.f;
		g_pathCenterY = -3000.f;

		strcpy(g_curPathPattern, "data\\paths\\nodes%d.dat");
	}

	g_pathCenterXPos = -g_pathCenterX;
	g_pathCenterYPos = -g_pathCenterY;

	return retval;
}

void PatchPathTransfer()
{
	using namespace MemoryVP;

	strcpy(g_curPathPattern, "data\\paths\\nodes%d.dat");
	g_pathCenterX = -3000.f;
	g_pathCenterY = -3000.f;
	g_pathCenterXPos = -g_pathCenterX;
	g_pathCenterYPos = -g_pathCenterY;

	// dynamic center for paths
	Patch(0x44D837, &g_pathCenterX);
	Patch(0x44D862, &g_pathCenterY);

	Patch(0x44D896, &g_pathCenterX);
	Patch(0x44D8C6, &g_pathCenterY);

	Patch(0x44DB6E, &g_pathCenterX);
	Patch(0x44DBB4, &g_pathCenterX);
	Patch(0x44DBFB, &g_pathCenterY);
	Patch(0x44DC41, &g_pathCenterY);

	Patch(0x44DD17, &g_pathCenterX);
	Patch(0x44DD47, &g_pathCenterY);
	Patch(0x44DD72, &g_pathCenterX);
	Patch(0x44DD9B, &g_pathCenterY);

	Patch(0x44F46D, &g_pathCenterX);
	Patch(0x44F4B4, &g_pathCenterY);

	Patch(0x450BCA, &g_pathCenterX);
	Patch(0x450BF7, &g_pathCenterY);
	Patch(0x450C2C, &g_pathCenterX);
	Patch(0x450C59, &g_pathCenterY);

	Patch(0x44D8FC, &g_pathCenterXPos);
	Patch(0x44D91C, &g_pathCenterYPos);

	Patch(0x44F4EF, &g_pathCenterXPos);
	Patch(0x44F507, &g_pathCenterXPos);

	Patch(0x44F530, &g_pathCenterYPos);
	Patch(0x44F55E, &g_pathCenterYPos);

	// path node filename
	Patch<char*>(0x452F50, g_curPathPattern);

	// request/release replacements
	InjectHook(0x44DE51, LoadPathFindData, PATCH_CALL);
	InjectHook(0x450D1F, LoadPathFindData, PATCH_CALL);
	InjectHook(0x450D2C, LoadPathFindData, PATCH_CALL);

	InjectHook(0x450D3B, UnloadPathFindData, PATCH_CALL);
	InjectHook(0x450975, UnloadPathFindData, PATCH_CALL);

	// streaming bits and bytes
	InjectHook(0x450AC3, UpdateStreaming_FindPlayerCoorsHook, PATCH_CALL);
}