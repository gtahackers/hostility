#include "StdInc.h"
#include "ScriptRuntime.h"

class VCScriptChainFactory : public ScriptChainFactory
{
public:
	virtual std::shared_ptr<ScriptChain> GetChain();
};

template<int NumParams>
void CollectParameters(HostilityScript* script)
{
	script->CollectParameters(NumParams);
}

template<int TargetID>
void PassToGame(HostilityScript* script)
{
	script->InvokeGameCommand(TargetID);
}

template<int NumX, int NumY>
void GetCoordinates(HostilityScript* script)
{
	script->SetTailCB([=] ()
	{
		scriptParameters[NumX - 1].SetFloatValue(scriptParameters[NumX - 1].GetFloatValue() - (3200.f + 2300.f));
	});
}

template<int NumX, int NumY, int NumZ>
void GetCoordinates(HostilityScript* script)
{
	script->SetTailCB([=] ()
	{
		scriptParameters[NumX - 1].SetFloatValue(scriptParameters[NumX - 1].GetFloatValue() - (3200.f + 2300.f));
	});
}

template<int NumX, int NumY>
void SetCoordinates(HostilityScript* script)
{
	scriptParameters[NumX - 1] = ScriptParameter(scriptParameters[NumX - 1].GetFloatValue() + (3200.f + 2300.f));
}

template<int NumX, int NumY, int NumZ>
void SetCoordinates(HostilityScript* script)
{
	scriptParameters[NumX - 1] = ScriptParameter(scriptParameters[NumX - 1].GetFloatValue() + (3200.f + 2300.f));
}

template<int Num>
void Object(HostilityScript* script)
{
	int objId = scriptParameters[Num - 1].GetIntValue();

	/*if (objId >= 109 && objId <= 129)
	{
		objId -= 109;

		objId %= 10;

		objId += 290;
	}*/
	if (objId == 169)
	{
		objId = 0x1D7;
	}
	else if (objId >= 1 && objId <= 129)
	{
		objId = 7;
	}
	else if (objId >= 130 && objId <= 236)
	{
		objId = 461;
	}
	else if (objId >= 295 && objId <= 299) // cutobj
	{
		objId = 1207; // tiny_rock
	}

	scriptParameters[Num - 1] = ScriptParameter(objId);
}

template<int Num>
void ObjectRaw(HostilityScript* script)
{
	int objId = scriptParameters[Num - 1].GetIntValue();

	if (objId == 169)
	{
		objId = 0x1D7;
	}
	else if (objId >= 1 && objId <= 129)
	{
		objId = 7;
	}
	else if (objId >= 130 && objId <= 236)
	{
		objId = 461;
	}

	scriptParameters[Num - 1] = ScriptParameter(objId);
}

template<int NumArg, int NewCommand>
void PlayerRechain(HostilityScript* script)
{
	int* nativeScriptSpace = (int*)0xA49960;

	scriptParameters[NumArg - 1] = ScriptParameter(nativeScriptSpace[3]);

	script->RechainCommand(NewCommand);
}

template<int NumArg>
void RemoveArg(HostilityScript* script)
{
	for (int i = NumArg - 1; i < (numScriptParameters - 1); i++)
	{
		scriptParameters[i] = scriptParameters[i + 1];
	}

	numScriptParameters--;
}

template<int NumArg>
void SpecialCharName(HostilityScript* script)
{
	//scriptParameters[NumArg - 1] = ScriptParameter("RYDER");
	script->SetCompareFlag(true);
}

template<int NumArg>
void SpecialChar(HostilityScript* script)
{
	//scriptParameters[NumArg - 1] = ScriptParameter(scriptParameters[NumArg - 1].GetIntValue() % 10);
	script->SetCompareFlag(true);
}

std::shared_ptr<ScriptChain> VCScriptChainFactory::GetChain()
{
	auto chain = std::make_shared<ScriptChain>();

	// collecting parameters for commands
#pragma region CollectParameters
	chain->Add(0x0000, CollectParameters<0>);
	chain->Add(0x0001, CollectParameters<1>);
	chain->Add(0x0002, CollectParameters<1>);
	chain->Add(0x0003, CollectParameters<1>);
	chain->Add(0x0004, CollectParameters<2>);
	chain->Add(0x0005, CollectParameters<2>);
	chain->Add(0x0006, CollectParameters<2>);
	chain->Add(0x0007, CollectParameters<2>);
	chain->Add(0x0008, CollectParameters<2>);
	chain->Add(0x0009, CollectParameters<2>);
	chain->Add(0x000A, CollectParameters<2>);
	chain->Add(0x000B, CollectParameters<2>);
	chain->Add(0x000c, CollectParameters<2>);
	chain->Add(0x000d, CollectParameters<2>);
	chain->Add(0x000E, CollectParameters<2>);
	chain->Add(0x000F, CollectParameters<2>);
	chain->Add(0x0010, CollectParameters<2>);
	chain->Add(0x0011, CollectParameters<2>);
	chain->Add(0x0012, CollectParameters<2>);
	chain->Add(0x0013, CollectParameters<2>);
	chain->Add(0x0014, CollectParameters<2>);
	chain->Add(0x0015, CollectParameters<2>);
	chain->Add(0x0016, CollectParameters<2>);
	chain->Add(0x0017, CollectParameters<2>);
	chain->Add(0x0018, CollectParameters<2>);
	chain->Add(0x0019, CollectParameters<2>);
	chain->Add(0x001a, CollectParameters<2>);
	chain->Add(0x001b, CollectParameters<2>);
	chain->Add(0x001c, CollectParameters<2>);
	chain->Add(0x001d, CollectParameters<2>);
	chain->Add(0x001E, CollectParameters<2>);
	chain->Add(0x001f, CollectParameters<2>);
	chain->Add(0x0020, CollectParameters<2>);
	chain->Add(0x0021, CollectParameters<2>);
	chain->Add(0x0022, CollectParameters<2>);
	chain->Add(0x0023, CollectParameters<2>);
	chain->Add(0x0024, CollectParameters<2>);
	chain->Add(0x0025, CollectParameters<2>);
	chain->Add(0x0026, CollectParameters<2>);
	chain->Add(0x0027, CollectParameters<2>);
	chain->Add(0x0028, CollectParameters<2>);
	chain->Add(0x0029, CollectParameters<2>);
	chain->Add(0x002a, CollectParameters<2>);
	chain->Add(0x002b, CollectParameters<2>);
	chain->Add(0x002c, CollectParameters<2>);
	chain->Add(0x002d, CollectParameters<2>);
	chain->Add(0x002e, CollectParameters<2>);
	chain->Add(0x002F, CollectParameters<2>);
	chain->Add(0x0030, CollectParameters<2>);
	chain->Add(0x0031, CollectParameters<2>);
	chain->Add(0x0032, CollectParameters<2>);
	chain->Add(0x0033, CollectParameters<2>);
	chain->Add(0x0034, CollectParameters<2>);
	chain->Add(0x0035, CollectParameters<2>);
	chain->Add(0x0036, CollectParameters<2>);
	chain->Add(0x0037, CollectParameters<2>);
	chain->Add(0x0038, CollectParameters<2>);
	chain->Add(0x0039, CollectParameters<2>);
	chain->Add(0x003a, CollectParameters<2>);
	chain->Add(0x003b, CollectParameters<2>);
	chain->Add(0x003c, CollectParameters<2>);
	chain->Add(0x0042, CollectParameters<2>);
	chain->Add(0x0043, CollectParameters<2>);
	chain->Add(0x0044, CollectParameters<2>);
	chain->Add(0x0045, CollectParameters<2>);
	chain->Add(0x0046, CollectParameters<2>);
	chain->Add(0x004c, CollectParameters<1>);
	chain->Add(0x004D, CollectParameters<1>);
	chain->Add(0x004E, CollectParameters<0>);
	chain->Add(0x004f, CollectParameters<-1>);
	chain->Add(0x0050, CollectParameters<1>);
	chain->Add(0x0051, CollectParameters<0>);
	chain->Add(0x0052, CollectParameters<6>);
	chain->Add(0x0053, CollectParameters<5>);
	chain->Add(0x0054, CollectParameters<4>);
	chain->Add(0x0055, CollectParameters<4>);
	chain->Add(0x0056, CollectParameters<6>);
	chain->Add(0x0057, CollectParameters<8>);
	chain->Add(0x0058, CollectParameters<2>);
	chain->Add(0x0059, CollectParameters<2>);
	chain->Add(0x005a, CollectParameters<2>);
	chain->Add(0x005b, CollectParameters<2>);
	chain->Add(0x005c, CollectParameters<2>);
	chain->Add(0x005d, CollectParameters<2>);
	chain->Add(0x005e, CollectParameters<2>);
	chain->Add(0x005f, CollectParameters<2>);
	chain->Add(0x0060, CollectParameters<2>);
	chain->Add(0x0061, CollectParameters<2>);
	chain->Add(0x0062, CollectParameters<2>);
	chain->Add(0x0063, CollectParameters<2>);
	chain->Add(0x0064, CollectParameters<2>);
	chain->Add(0x0065, CollectParameters<2>);
	chain->Add(0x0066, CollectParameters<2>);
	chain->Add(0x0067, CollectParameters<2>);
	chain->Add(0x0068, CollectParameters<2>);
	chain->Add(0x0069, CollectParameters<2>);
	chain->Add(0x006a, CollectParameters<2>);
	chain->Add(0x006b, CollectParameters<2>);
	chain->Add(0x006c, CollectParameters<2>);
	chain->Add(0x006d, CollectParameters<2>);
	chain->Add(0x006e, CollectParameters<2>);
	chain->Add(0x006f, CollectParameters<2>);
	chain->Add(0x0070, CollectParameters<2>);
	chain->Add(0x0071, CollectParameters<2>);
	chain->Add(0x0072, CollectParameters<2>);
	chain->Add(0x0073, CollectParameters<2>);
	chain->Add(0x0074, CollectParameters<2>);
	chain->Add(0x0075, CollectParameters<2>);
	chain->Add(0x0076, CollectParameters<2>);
	chain->Add(0x0077, CollectParameters<2>);
	chain->Add(0x0078, CollectParameters<2>);
	chain->Add(0x0079, CollectParameters<2>);
	chain->Add(0x007A, CollectParameters<2>);
	chain->Add(0x007B, CollectParameters<2>);
	chain->Add(0x007C, CollectParameters<2>);
	chain->Add(0x007D, CollectParameters<2>);
	chain->Add(0x007E, CollectParameters<2>);
	chain->Add(0x007F, CollectParameters<2>);
	chain->Add(0x0080, CollectParameters<2>);
	chain->Add(0x0081, CollectParameters<2>);
	chain->Add(0x0082, CollectParameters<2>);
	chain->Add(0x0083, CollectParameters<2>);
	chain->Add(0x0084, CollectParameters<2>);
	chain->Add(0x0085, CollectParameters<2>);
	chain->Add(0x0086, CollectParameters<2>);
	chain->Add(0x0087, CollectParameters<2>);
	chain->Add(0x0088, CollectParameters<2>);
	chain->Add(0x0089, CollectParameters<2>);
	chain->Add(0x008A, CollectParameters<2>);
	chain->Add(0x008B, CollectParameters<2>);
	chain->Add(0x008c, CollectParameters<2>);
	chain->Add(0x008d, CollectParameters<2>);
	chain->Add(0x008E, CollectParameters<2>);
	chain->Add(0x008f, CollectParameters<2>);
	chain->Add(0x0090, CollectParameters<2>);
	chain->Add(0x0091, CollectParameters<2>);
	chain->Add(0x0092, CollectParameters<2>);
	chain->Add(0x0093, CollectParameters<2>);
	chain->Add(0x0094, CollectParameters<1>);
	chain->Add(0x0095, CollectParameters<1>);
	chain->Add(0x0096, CollectParameters<1>);
	chain->Add(0x0097, CollectParameters<1>);
	chain->Add(0x0098, CollectParameters<1>);
	chain->Add(0x0099, CollectParameters<1>);
	chain->Add(0x009A, CollectParameters<6>);
	chain->Add(0x009B, CollectParameters<1>);
	chain->Add(0x009c, CollectParameters<2>);
	chain->Add(0x009d, CollectParameters<0>);
	chain->Add(0x009E, CollectParameters<6>);
	chain->Add(0x009f, CollectParameters<1>);
	chain->Add(0x00A0, CollectParameters<4>);
	chain->Add(0x00a1, CollectParameters<4>);
	chain->Add(0x00A2, CollectParameters<1>);
	chain->Add(0x00A3, CollectParameters<6>);
	chain->Add(0x00A4, CollectParameters<8>);
	chain->Add(0x00A5, CollectParameters<5>);
	chain->Add(0x00A6, CollectParameters<1>);
	chain->Add(0x00A7, CollectParameters<4>);
	chain->Add(0x00A8, CollectParameters<1>);
	chain->Add(0x00a9, CollectParameters<1>);
	chain->Add(0x00AA, CollectParameters<4>);
	chain->Add(0x00AB, CollectParameters<4>);
	chain->Add(0x00AC, CollectParameters<1>);
	chain->Add(0x00ad, CollectParameters<2>);
	chain->Add(0x00AE, CollectParameters<2>);
	chain->Add(0x00AF, CollectParameters<2>);
	chain->Add(0x00B0, CollectParameters<6>);
	chain->Add(0x00B1, CollectParameters<8>);
	chain->Add(0x00BA, CollectParameters<3>);
	chain->Add(0x00BB, CollectParameters<3>);
	chain->Add(0x00BC, CollectParameters<3>);
	chain->Add(0x00be, CollectParameters<0>);
	chain->Add(0x00BF, CollectParameters<2>);
	chain->Add(0x00c0, CollectParameters<2>);
	chain->Add(0x00C1, CollectParameters<3>);
	chain->Add(0x00c2, CollectParameters<4>);
	chain->Add(0x00c3, CollectParameters<0>);
	chain->Add(0x00c4, CollectParameters<0>);
	chain->Add(0x00c5, CollectParameters<0>);
	chain->Add(0x00C6, CollectParameters<0>);
	chain->Add(0x00D6, CollectParameters<1>);
	chain->Add(0x00D7, CollectParameters<1>);
	chain->Add(0x00d8, CollectParameters<0>);
	chain->Add(0x00D9, CollectParameters<2>);
	chain->Add(0x00da, CollectParameters<2>);
	chain->Add(0x00DB, CollectParameters<2>);
	chain->Add(0x00dc, CollectParameters<2>);
	chain->Add(0x00DD, CollectParameters<2>);
	chain->Add(0x00de, CollectParameters<2>);
	chain->Add(0x00DF, CollectParameters<1>);
	chain->Add(0x00e0, CollectParameters<1>);
	chain->Add(0x00E1, CollectParameters<2>);
	chain->Add(0x00E2, CollectParameters<3>);
	chain->Add(0x00e3, CollectParameters<6>);
	chain->Add(0x00e4, CollectParameters<6>);
	chain->Add(0x00e5, CollectParameters<6>);
	chain->Add(0x00e6, CollectParameters<6>);
	chain->Add(0x00e7, CollectParameters<6>);
	chain->Add(0x00e8, CollectParameters<6>);
	chain->Add(0x00e9, CollectParameters<5>);
	chain->Add(0x00ea, CollectParameters<5>);
	chain->Add(0x00eb, CollectParameters<5>);
	chain->Add(0x00ec, CollectParameters<6>);
	chain->Add(0x00ed, CollectParameters<6>);
	chain->Add(0x00EE, CollectParameters<6>);
	chain->Add(0x00ef, CollectParameters<6>);
	chain->Add(0x00f0, CollectParameters<6>);
	chain->Add(0x00f1, CollectParameters<6>);
	chain->Add(0x00F2, CollectParameters<5>);
	chain->Add(0x00F3, CollectParameters<5>);
	chain->Add(0x00F4, CollectParameters<5>);
	chain->Add(0x00f5, CollectParameters<8>);
	chain->Add(0x00f6, CollectParameters<8>);
	chain->Add(0x00F7, CollectParameters<8>);
	chain->Add(0x00f8, CollectParameters<8>);
	chain->Add(0x00f9, CollectParameters<8>);
	chain->Add(0x00fa, CollectParameters<8>);
	chain->Add(0x00fb, CollectParameters<6>);
	chain->Add(0x00fc, CollectParameters<6>);
	chain->Add(0x00fd, CollectParameters<6>);
	chain->Add(0x00fe, CollectParameters<8>);
	chain->Add(0x00ff, CollectParameters<8>);
	chain->Add(0x0100, CollectParameters<8>);
	chain->Add(0x0101, CollectParameters<8>);
	chain->Add(0x0102, CollectParameters<8>);
	chain->Add(0x0103, CollectParameters<8>);
	chain->Add(0x0104, CollectParameters<6>);
	chain->Add(0x0105, CollectParameters<6>);
	chain->Add(0x0106, CollectParameters<6>);
	chain->Add(0x0107, CollectParameters<5>);
	chain->Add(0x0108, CollectParameters<1>);
	chain->Add(0x0109, CollectParameters<2>);
	chain->Add(0x010a, CollectParameters<2>);
	chain->Add(0x010B, CollectParameters<2>);
	chain->Add(0x010C, CollectParameters<5>);
	chain->Add(0x010d, CollectParameters<2>);
	chain->Add(0x010e, CollectParameters<2>);
	chain->Add(0x010f, CollectParameters<2>);
	chain->Add(0x0110, CollectParameters<1>);
	chain->Add(0x0111, CollectParameters<1>);
	chain->Add(0x0112, CollectParameters<0>);
	chain->Add(0x0113, CollectParameters<3>);
	chain->Add(0x0114, CollectParameters<3>);
	chain->Add(0x0117, CollectParameters<1>);
	chain->Add(0x0118, CollectParameters<1>);
	chain->Add(0x0119, CollectParameters<1>);
	chain->Add(0x011a, CollectParameters<2>);
	chain->Add(0x011c, CollectParameters<1>);
	chain->Add(0x011d, CollectParameters<1>);
	chain->Add(0x0121, CollectParameters<2>);
	chain->Add(0x0122, CollectParameters<1>);
	chain->Add(0x0123, CollectParameters<2>);
	chain->Add(0x0126, CollectParameters<1>);
	chain->Add(0x0129, CollectParameters<4>);
	chain->Add(0x012a, CollectParameters<4>);
	chain->Add(0x0130, CollectParameters<1>);
	chain->Add(0x0135, CollectParameters<2>);
	chain->Add(0x0137, CollectParameters<2>);
	chain->Add(0x0149, CollectParameters<1>);
	chain->Add(0x014B, CollectParameters<13>);
	chain->Add(0x014C, CollectParameters<2>);
	chain->Add(0x014d, CollectParameters<4>);
	chain->Add(0x014E, CollectParameters<2>);
	chain->Add(0x014f, CollectParameters<1>);
	chain->Add(0x0151, CollectParameters<1>);
	chain->Add(0x0152, CollectParameters<13>);
	chain->Add(0x0154, CollectParameters<2>);
	chain->Add(0x0156, CollectParameters<3>);
	chain->Add(0x0157, CollectParameters<3>);
	chain->Add(0x0158, CollectParameters<3>);
	chain->Add(0x0159, CollectParameters<3>);
	chain->Add(0x015a, CollectParameters<0>);
	chain->Add(0x015C, CollectParameters<13>);
	chain->Add(0x015D, CollectParameters<1>);
	chain->Add(0x015f, CollectParameters<6>);
	chain->Add(0x0160, CollectParameters<4>);
	chain->Add(0x0161, CollectParameters<4>);
	chain->Add(0x0162, CollectParameters<4>);
	chain->Add(0x0164, CollectParameters<1>);
	chain->Add(0x0165, CollectParameters<2>);
	chain->Add(0x0166, CollectParameters<2>);
	chain->Add(0x0167, CollectParameters<6>);
	chain->Add(0x0168, CollectParameters<2>);
	chain->Add(0x0169, CollectParameters<3>);
	chain->Add(0x016a, CollectParameters<2>);
	chain->Add(0x016b, CollectParameters<0>);
	chain->Add(0x016c, CollectParameters<4>);
	chain->Add(0x016d, CollectParameters<4>);
	chain->Add(0x016E, CollectParameters<4>);
	chain->Add(0x016f, CollectParameters<10>);
	chain->Add(0x0170, CollectParameters<2>);
	chain->Add(0x0171, CollectParameters<2>);
	chain->Add(0x0172, CollectParameters<2>);
	chain->Add(0x0173, CollectParameters<2>);
	chain->Add(0x0174, CollectParameters<2>);
	chain->Add(0x0175, CollectParameters<2>);
	chain->Add(0x0176, CollectParameters<2>);
	chain->Add(0x0177, CollectParameters<2>);
	chain->Add(0x0178, CollectParameters<2>);
	chain->Add(0x0179, CollectParameters<2>);
	chain->Add(0x017a, CollectParameters<3>);
	chain->Add(0x017b, CollectParameters<3>);
	chain->Add(0x0180, CollectParameters<1>);
	chain->Add(0x0181, CollectParameters<2>);
	chain->Add(0x0182, CollectParameters<2>);
	chain->Add(0x0183, CollectParameters<2>);
	chain->Add(0x0184, CollectParameters<2>);
	chain->Add(0x0185, CollectParameters<2>);
	chain->Add(0x0186, CollectParameters<2>);
	chain->Add(0x0187, CollectParameters<2>);
	chain->Add(0x0188, CollectParameters<2>);
	chain->Add(0x0189, CollectParameters<4>);
	chain->Add(0x018a, CollectParameters<4>);
	chain->Add(0x018b, CollectParameters<2>);
	chain->Add(0x018c, CollectParameters<4>);
	chain->Add(0x018d, CollectParameters<5>);
	chain->Add(0x018e, CollectParameters<1>);
	chain->Add(0x018F, CollectParameters<1>);
	chain->Add(0x0190, CollectParameters<1>);
	chain->Add(0x0191, CollectParameters<1>);
	chain->Add(0x0192, CollectParameters<1>);
	chain->Add(0x0193, CollectParameters<1>);
	chain->Add(0x0194, CollectParameters<4>);
	chain->Add(0x0197, CollectParameters<6>);
	chain->Add(0x0198, CollectParameters<6>);
	chain->Add(0x0199, CollectParameters<6>);
	chain->Add(0x019a, CollectParameters<6>);
	chain->Add(0x019b, CollectParameters<6>);
	chain->Add(0x019c, CollectParameters<8>);
	chain->Add(0x019d, CollectParameters<8>);
	chain->Add(0x019e, CollectParameters<8>);
	chain->Add(0x019f, CollectParameters<8>);
	chain->Add(0x01a0, CollectParameters<8>);
	chain->Add(0x01a1, CollectParameters<6>);
	chain->Add(0x01a2, CollectParameters<6>);
	chain->Add(0x01a3, CollectParameters<6>);
	chain->Add(0x01A4, CollectParameters<6>);
	chain->Add(0x01a5, CollectParameters<6>);
	chain->Add(0x01a6, CollectParameters<8>);
	chain->Add(0x01a7, CollectParameters<8>);
	chain->Add(0x01a8, CollectParameters<8>);
	chain->Add(0x01a9, CollectParameters<8>);
	chain->Add(0x01aa, CollectParameters<8>);
	chain->Add(0x01ab, CollectParameters<6>);
	chain->Add(0x01ac, CollectParameters<8>);
	chain->Add(0x01ad, CollectParameters<6>);
	chain->Add(0x01ae, CollectParameters<6>);
	chain->Add(0x01af, CollectParameters<8>);
	chain->Add(0x01b0, CollectParameters<8>);
	chain->Add(0x01b1, CollectParameters<3>);
	chain->Add(0x01b2, CollectParameters<3>);
	chain->Add(0x01b4, CollectParameters<2>);
	chain->Add(0x01b5, CollectParameters<1>);
	chain->Add(0x01b6, CollectParameters<1>);
	chain->Add(0x01b7, CollectParameters<0>);
	chain->Add(0x01b8, CollectParameters<2>);
	chain->Add(0x01b9, CollectParameters<2>);
	chain->Add(0x01bb, CollectParameters<4>);
	chain->Add(0x01bc, CollectParameters<4>);
	chain->Add(0x01bd, CollectParameters<1>);
	chain->Add(0x01be, CollectParameters<4>);
	chain->Add(0x01c0, CollectParameters<2>);
	chain->Add(0x01c1, CollectParameters<1>);
	chain->Add(0x01C2, CollectParameters<1>);
	chain->Add(0x01C3, CollectParameters<1>);
	chain->Add(0x01c4, CollectParameters<1>);
	chain->Add(0x01C5, CollectParameters<1>);
	chain->Add(0x01C6, CollectParameters<1>);
	chain->Add(0x01c7, CollectParameters<1>);
	chain->Add(0x01c8, CollectParameters<5>);
	chain->Add(0x01c9, CollectParameters<2>);
	chain->Add(0x01ca, CollectParameters<2>);
	chain->Add(0x01cb, CollectParameters<2>);
	chain->Add(0x01cc, CollectParameters<2>);
	chain->Add(0x01ce, CollectParameters<2>);
	chain->Add(0x01CF, CollectParameters<2>);
	chain->Add(0x01d0, CollectParameters<2>);
	chain->Add(0x01d1, CollectParameters<2>);
	chain->Add(0x01d2, CollectParameters<2>);
	chain->Add(0x01d3, CollectParameters<2>);
	chain->Add(0x01d4, CollectParameters<2>);
	chain->Add(0x01d5, CollectParameters<2>);
	chain->Add(0x01D8, CollectParameters<2>);
	chain->Add(0x01d9, CollectParameters<2>);
	chain->Add(0x01de, CollectParameters<2>);
	chain->Add(0x01df, CollectParameters<2>);
	chain->Add(0x01e0, CollectParameters<1>);
	chain->Add(0x01e1, CollectParameters<3>);
	chain->Add(0x01e2, CollectParameters<4>);
	chain->Add(0x01E3, CollectParameters<4>);
	chain->Add(0x01e4, CollectParameters<4>);
	chain->Add(0x01e5, CollectParameters<4>);
	chain->Add(0x01e6, CollectParameters<4>);
	chain->Add(0x01e7, CollectParameters<6>);
	chain->Add(0x01E8, CollectParameters<6>);
	chain->Add(0x01E9, CollectParameters<2>);
	chain->Add(0x01ea, CollectParameters<2>);
	chain->Add(0x01EB, CollectParameters<1>);
	chain->Add(0x01EC, CollectParameters<2>);
	chain->Add(0x01ED, CollectParameters<1>);
	chain->Add(0x01ee, CollectParameters<10>);
	chain->Add(0x01ef, CollectParameters<2>);
	chain->Add(0x01f0, CollectParameters<1>);
	chain->Add(0x01f3, CollectParameters<1>);
	chain->Add(0x01f4, CollectParameters<1>);
	chain->Add(0x01F5, CollectParameters<2>);
	chain->Add(0x01f6, CollectParameters<0>);
	chain->Add(0x01f7, CollectParameters<2>);
	chain->Add(0x01f9, CollectParameters<9>);
	chain->Add(0x01fa, CollectParameters<1>);
	chain->Add(0x01fb, CollectParameters<2>);
	chain->Add(0x01fc, CollectParameters<5>);
	chain->Add(0x01fd, CollectParameters<5>);
	chain->Add(0x01fe, CollectParameters<5>);
	chain->Add(0x01FF, CollectParameters<6>);
	chain->Add(0x0200, CollectParameters<6>);
	chain->Add(0x0201, CollectParameters<6>);
	chain->Add(0x0202, CollectParameters<5>);
	chain->Add(0x0203, CollectParameters<5>);
	chain->Add(0x0204, CollectParameters<5>);
	chain->Add(0x0205, CollectParameters<6>);
	chain->Add(0x0206, CollectParameters<6>);
	chain->Add(0x0207, CollectParameters<6>);
	chain->Add(0x0208, CollectParameters<3>);
	chain->Add(0x0209, CollectParameters<3>);
	chain->Add(0x020a, CollectParameters<2>);
	chain->Add(0x020b, CollectParameters<1>);
	chain->Add(0x020c, CollectParameters<4>);
	chain->Add(0x020d, CollectParameters<1>);
	chain->Add(0x020e, CollectParameters<2>);
	chain->Add(0x020f, CollectParameters<2>);
	chain->Add(0x0210, CollectParameters<2>);
	chain->Add(0x0211, CollectParameters<3>);
	chain->Add(0x0213, CollectParameters<6>);
	chain->Add(0x0214, CollectParameters<1>);
	chain->Add(0x0215, CollectParameters<1>);
	chain->Add(0x0216, CollectParameters<2>);
	chain->Add(0x0217, CollectParameters<3>);
	chain->Add(0x0218, CollectParameters<4>);
	chain->Add(0x0219, CollectParameters<10>);
	chain->Add(0x021b, CollectParameters<2>);
	chain->Add(0x021c, CollectParameters<1>);
	chain->Add(0x021d, CollectParameters<1>);
	chain->Add(0x0221, CollectParameters<2>);
	chain->Add(0x0222, CollectParameters<2>);
	chain->Add(0x0223, CollectParameters<2>);
	chain->Add(0x0224, CollectParameters<2>);
	chain->Add(0x0225, CollectParameters<2>);
	chain->Add(0x0226, CollectParameters<2>);
	chain->Add(0x0227, CollectParameters<2>);
	chain->Add(0x0228, CollectParameters<2>);
	chain->Add(0x0229, CollectParameters<3>);
	chain->Add(0x022a, CollectParameters<6>);
	chain->Add(0x022b, CollectParameters<6>);
	chain->Add(0x022c, CollectParameters<2>);
	chain->Add(0x022d, CollectParameters<2>);
	chain->Add(0x022e, CollectParameters<2>);
	chain->Add(0x022f, CollectParameters<1>);
	chain->Add(0x0230, CollectParameters<1>);
	chain->Add(0x0235, CollectParameters<3>);
	chain->Add(0x0236, CollectParameters<2>);
	chain->Add(0x0237, CollectParameters<3>);
	chain->Add(0x0239, CollectParameters<3>);
	chain->Add(0x023a, CollectParameters<2>);
	chain->Add(0x023b, CollectParameters<2>);
	chain->Add(0x023c, CollectParameters<2>);
	chain->Add(0x023d, CollectParameters<1>);
	chain->Add(0x0240, CollectParameters<2>);
	chain->Add(0x0241, CollectParameters<1>);
	chain->Add(0x0242, CollectParameters<2>);
	chain->Add(0x0243, CollectParameters<2>);
	chain->Add(0x0244, CollectParameters<3>);
	chain->Add(0x0245, CollectParameters<2>);
	chain->Add(0x0247, CollectParameters<1>);
	chain->Add(0x0248, CollectParameters<1>);
	chain->Add(0x0249, CollectParameters<1>);
	chain->Add(0x024a, CollectParameters<3>);
	chain->Add(0x024b, CollectParameters<2>);
	chain->Add(0x024C, CollectParameters<2>);
	chain->Add(0x024d, CollectParameters<1>);
	chain->Add(0x024e, CollectParameters<1>);
	chain->Add(0x024f, CollectParameters<9>);
	chain->Add(0x0250, CollectParameters<6>);
	chain->Add(0x0253, CollectParameters<0>);
	chain->Add(0x0254, CollectParameters<0>);
	chain->Add(0x0255, CollectParameters<4>);
	chain->Add(0x0256, CollectParameters<1>);
	chain->Add(0x0291, CollectParameters<2>);
	chain->Add(0x0293, CollectParameters<1>);
	chain->Add(0x0294, CollectParameters<2>);
	chain->Add(0x0296, CollectParameters<1>);
	chain->Add(0x0297, CollectParameters<0>);
	chain->Add(0x0298, CollectParameters<2>);
	chain->Add(0x0299, CollectParameters<1>);
	chain->Add(0x029b, CollectParameters<5>);
	chain->Add(0x029c, CollectParameters<1>);
	chain->Add(0x029f, CollectParameters<1>);
	chain->Add(0x02a0, CollectParameters<1>);
	chain->Add(0x02a1, CollectParameters<2>);
	chain->Add(0x02a2, CollectParameters<5>);
	chain->Add(0x02a3, CollectParameters<1>);
	chain->Add(0x02a7, CollectParameters<5>);
	chain->Add(0x02a8, CollectParameters<5>);
	chain->Add(0x02A9, CollectParameters<2>);
	chain->Add(0x02aa, CollectParameters<2>);
	chain->Add(0x02ab, CollectParameters<6>);
	chain->Add(0x02ac, CollectParameters<6>);
	chain->Add(0x02AD, CollectParameters<7>);
	chain->Add(0x02AE, CollectParameters<7>);
	chain->Add(0x02AF, CollectParameters<7>);
	chain->Add(0x02b0, CollectParameters<7>);
	chain->Add(0x02b1, CollectParameters<7>);
	chain->Add(0x02b2, CollectParameters<7>);
	chain->Add(0x02b3, CollectParameters<9>);
	chain->Add(0x02b4, CollectParameters<9>);
	chain->Add(0x02B5, CollectParameters<9>);
	chain->Add(0x02b6, CollectParameters<9>);
	chain->Add(0x02b7, CollectParameters<9>);
	chain->Add(0x02B8, CollectParameters<9>);
	chain->Add(0x02b9, CollectParameters<1>);
	chain->Add(0x02bc, CollectParameters<1>);
	chain->Add(0x02bf, CollectParameters<1>);
	chain->Add(0x02c0, CollectParameters<6>);
	chain->Add(0x02c1, CollectParameters<6>);
	chain->Add(0x02c2, CollectParameters<4>);
	chain->Add(0x02c3, CollectParameters<1>);
	chain->Add(0x02C5, CollectParameters<1>);
	chain->Add(0x02c6, CollectParameters<0>);
	chain->Add(0x02c7, CollectParameters<5>);
	chain->Add(0x02c8, CollectParameters<1>);
	chain->Add(0x02c9, CollectParameters<0>);
	chain->Add(0x02ca, CollectParameters<1>);
	chain->Add(0x02cb, CollectParameters<1>);
	chain->Add(0x02CC, CollectParameters<1>);
	chain->Add(0x02cd, CollectParameters<2>);
	chain->Add(0x02ce, CollectParameters<4>);
	chain->Add(0x02cf, CollectParameters<4>);
	chain->Add(0x02d0, CollectParameters<1>);
	chain->Add(0x02D1, CollectParameters<1>);
	chain->Add(0x02d3, CollectParameters<4>);
	chain->Add(0x02d4, CollectParameters<1>);
	chain->Add(0x02d5, CollectParameters<6>);
	chain->Add(0x02d7, CollectParameters<2>);
	chain->Add(0x02d8, CollectParameters<2>);
	chain->Add(0x02d9, CollectParameters<0>);
	chain->Add(0x02DB, CollectParameters<2>);
	chain->Add(0x02DD, CollectParameters<5>);
	chain->Add(0x02de, CollectParameters<1>);
	chain->Add(0x02df, CollectParameters<1>);
	chain->Add(0x02e0, CollectParameters<1>);
	chain->Add(0x02E1, CollectParameters<5>);
	chain->Add(0x02e2, CollectParameters<2>);
	chain->Add(0x02e3, CollectParameters<2>);
	chain->Add(0x02e4, CollectParameters<1>);
	chain->Add(0x02E5, CollectParameters<2>);
	chain->Add(0x02E6, CollectParameters<2>);
	chain->Add(0x02e7, CollectParameters<0>);
	chain->Add(0x02e8, CollectParameters<1>);
	chain->Add(0x02e9, CollectParameters<0>);
	chain->Add(0x02ea, CollectParameters<0>);
	chain->Add(0x02EB, CollectParameters<0>);
	chain->Add(0x02ec, CollectParameters<3>);
	chain->Add(0x02ed, CollectParameters<1>);
	chain->Add(0x02ee, CollectParameters<6>);
	chain->Add(0x02EF, CollectParameters<6>);
	chain->Add(0x02F1, CollectParameters<3>);
	chain->Add(0x02F2, CollectParameters<2>);
	chain->Add(0x02F3, CollectParameters<2>);
	chain->Add(0x02f4, CollectParameters<3>);
	chain->Add(0x02F5, CollectParameters<2>);
	chain->Add(0x02F6, CollectParameters<2>);
	chain->Add(0x02F7, CollectParameters<2>);
	chain->Add(0x02F8, CollectParameters<2>);
	chain->Add(0x02F9, CollectParameters<2>);
	chain->Add(0x02fa, CollectParameters<2>);
	chain->Add(0x02fb, CollectParameters<10>);
	chain->Add(0x02fc, CollectParameters<5>);
	chain->Add(0x02fd, CollectParameters<5>);
	chain->Add(0x02fe, CollectParameters<5>);
	chain->Add(0x02FF, CollectParameters<6>);
	chain->Add(0x0300, CollectParameters<6>);
	chain->Add(0x0301, CollectParameters<6>);
	chain->Add(0x0302, CollectParameters<7>);
	chain->Add(0x0303, CollectParameters<7>);
	chain->Add(0x0304, CollectParameters<7>);
	chain->Add(0x0305, CollectParameters<8>);
	chain->Add(0x0306, CollectParameters<8>);
	chain->Add(0x0307, CollectParameters<8>);
	chain->Add(0x0308, CollectParameters<9>);
	chain->Add(0x0309, CollectParameters<9>);
	chain->Add(0x030a, CollectParameters<9>);
	chain->Add(0x030c, CollectParameters<1>);
	chain->Add(0x030d, CollectParameters<1>);
	chain->Add(0x030e, CollectParameters<1>);
	chain->Add(0x030f, CollectParameters<1>);
	chain->Add(0x0310, CollectParameters<1>);
	chain->Add(0x0311, CollectParameters<1>);
	chain->Add(0x0312, CollectParameters<1>);
	chain->Add(0x0313, CollectParameters<0>);
	chain->Add(0x0314, CollectParameters<1>);
	chain->Add(0x0315, CollectParameters<0>);
	chain->Add(0x0316, CollectParameters<1>);
	chain->Add(0x0317, CollectParameters<0>);
	chain->Add(0x0318, CollectParameters<1>);
	chain->Add(0x0319, CollectParameters<2>);
	chain->Add(0x031a, CollectParameters<0>);
	chain->Add(0x031d, CollectParameters<2>);
	chain->Add(0x031E, CollectParameters<2>);
	chain->Add(0x031F, CollectParameters<2>);
	chain->Add(0x0320, CollectParameters<2>);
	chain->Add(0x0321, CollectParameters<1>);
	chain->Add(0x0322, CollectParameters<1>);
	chain->Add(0x0323, CollectParameters<2>);
	chain->Add(0x0324, CollectParameters<3>);
	chain->Add(0x0325, CollectParameters<2>);
	chain->Add(0x0326, CollectParameters<2>);
	chain->Add(0x0327, CollectParameters<6>);
	chain->Add(0x0329, CollectParameters<1>);
	chain->Add(0x032A, CollectParameters<1>);
	chain->Add(0x032b, CollectParameters<7>);
	chain->Add(0x032c, CollectParameters<2>);
	chain->Add(0x032d, CollectParameters<2>);
	chain->Add(0x0330, CollectParameters<2>);
	chain->Add(0x0331, CollectParameters<2>);
	chain->Add(0x0332, CollectParameters<2>);
	chain->Add(0x0335, CollectParameters<1>);
	chain->Add(0x0336, CollectParameters<2>);
	chain->Add(0x0337, CollectParameters<2>);
	chain->Add(0x0339, CollectParameters<11>);
	chain->Add(0x033a, CollectParameters<0>);
	chain->Add(0x033b, CollectParameters<0>);
	chain->Add(0x033c, CollectParameters<0>);
	chain->Add(0x033e, CollectParameters<3>);
	chain->Add(0x033f, CollectParameters<2>);
	chain->Add(0x0340, CollectParameters<4>);
	chain->Add(0x0341, CollectParameters<1>);
	chain->Add(0x0342, CollectParameters<1>);
	chain->Add(0x0343, CollectParameters<1>);
	chain->Add(0x0344, CollectParameters<1>);
	chain->Add(0x0345, CollectParameters<1>);
	chain->Add(0x0346, CollectParameters<4>);
	chain->Add(0x0348, CollectParameters<1>);
	chain->Add(0x0349, CollectParameters<1>);
	chain->Add(0x034a, CollectParameters<0>);
	chain->Add(0x034b, CollectParameters<0>);
	chain->Add(0x034c, CollectParameters<0>);
	chain->Add(0x034d, CollectParameters<4>);
	chain->Add(0x034e, CollectParameters<8>);
	chain->Add(0x034f, CollectParameters<1>);
	chain->Add(0x0350, CollectParameters<2>);
	chain->Add(0x0351, CollectParameters<0>);
	chain->Add(0x0352, CollectParameters<2>);
	chain->Add(0x0353, CollectParameters<1>);
	chain->Add(0x0354, CollectParameters<1>);
	chain->Add(0x0355, CollectParameters<0>);
	chain->Add(0x0356, CollectParameters<7>);
	chain->Add(0x0358, CollectParameters<0>);
	chain->Add(0x0359, CollectParameters<0>);
	chain->Add(0x035a, CollectParameters<3>);
	chain->Add(0x035b, CollectParameters<4>);
	chain->Add(0x035c, CollectParameters<5>);
	chain->Add(0x035d, CollectParameters<1>);
	chain->Add(0x035E, CollectParameters<2>);
	chain->Add(0x035f, CollectParameters<2>);
	chain->Add(0x0360, CollectParameters<1>);
	chain->Add(0x0361, CollectParameters<1>);
	chain->Add(0x0362, CollectParameters<4>);
	chain->Add(0x0363, CollectParameters<6>);
	chain->Add(0x0365, CollectParameters<1>);
	chain->Add(0x0366, CollectParameters<1>);
	chain->Add(0x0367, CollectParameters<9>);
	chain->Add(0x0368, CollectParameters<10>);
	chain->Add(0x0369, CollectParameters<2>);
	chain->Add(0x036a, CollectParameters<2>);
	chain->Add(0x036D, CollectParameters<5>);
	chain->Add(0x036e, CollectParameters<6>);
	chain->Add(0x036f, CollectParameters<7>);
	chain->Add(0x0370, CollectParameters<8>);
	chain->Add(0x0371, CollectParameters<9>);
	chain->Add(0x0372, CollectParameters<3>);
	chain->Add(0x0373, CollectParameters<0>);
	chain->Add(0x0374, CollectParameters<1>);
	chain->Add(0x0375, CollectParameters<4>);
	chain->Add(0x0376, CollectParameters<4>);
	chain->Add(0x0377, CollectParameters<1>);
	chain->Add(0x037E, CollectParameters<6>);
	chain->Add(0x037f, CollectParameters<0>);
	chain->Add(0x0381, CollectParameters<4>);
	chain->Add(0x0382, CollectParameters<2>);
	chain->Add(0x0383, CollectParameters<1>);
	chain->Add(0x0384, CollectParameters<4>);
	chain->Add(0x0385, CollectParameters<4>);
	chain->Add(0x0386, CollectParameters<6>);
	chain->Add(0x0387, CollectParameters<6>);
	chain->Add(0x0388, CollectParameters<7>);
	chain->Add(0x0389, CollectParameters<7>);
	chain->Add(0x038a, CollectParameters<6>);
	chain->Add(0x038B, CollectParameters<0>);
	chain->Add(0x038c, CollectParameters<4>);
	chain->Add(0x038D, CollectParameters<9>);
	chain->Add(0x038E, CollectParameters<8>);
	chain->Add(0x038F, CollectParameters<2>);
	chain->Add(0x0390, CollectParameters<1>);
	chain->Add(0x0391, CollectParameters<0>);
	chain->Add(0x0392, CollectParameters<2>);
	chain->Add(0x0394, CollectParameters<1>);
	chain->Add(0x0395, CollectParameters<5>);
	chain->Add(0x0396, CollectParameters<1>);
	chain->Add(0x0397, CollectParameters<2>);
	chain->Add(0x0398, CollectParameters<7>);
	chain->Add(0x0399, CollectParameters<7>);
	chain->Add(0x039a, CollectParameters<7>);
	chain->Add(0x039b, CollectParameters<7>);
	chain->Add(0x039C, CollectParameters<2>);
	chain->Add(0x039d, CollectParameters<12>);
	chain->Add(0x039E, CollectParameters<2>);
	chain->Add(0x039f, CollectParameters<3>);
	chain->Add(0x03a0, CollectParameters<3>);
	chain->Add(0x03A1, CollectParameters<4>);
	chain->Add(0x03A2, CollectParameters<2>);
	chain->Add(0x03A3, CollectParameters<1>);
	chain->Add(0x03A4, CollectParameters<1>);
	chain->Add(0x03a5, CollectParameters<3>);
	chain->Add(0x03a6, CollectParameters<3>);
	chain->Add(0x03aa, CollectParameters<3>);
	chain->Add(0x03AB, CollectParameters<2>);
	chain->Add(0x03ac, CollectParameters<1>);
	chain->Add(0x03ad, CollectParameters<1>);
	chain->Add(0x03ae, CollectParameters<6>);
	chain->Add(0x03af, CollectParameters<1>);
	chain->Add(0x03b0, CollectParameters<1>);
	chain->Add(0x03b1, CollectParameters<1>);
	chain->Add(0x03b2, CollectParameters<0>);
	chain->Add(0x03b3, CollectParameters<0>);
	chain->Add(0x03b4, CollectParameters<0>);
	chain->Add(0x03b5, CollectParameters<0>);
	chain->Add(0x03b6, CollectParameters<6>);
	chain->Add(0x03b7, CollectParameters<1>);
	chain->Add(0x03b8, CollectParameters<1>);
	chain->Add(0x03b9, CollectParameters<1>);
	chain->Add(0x03ba, CollectParameters<6>);
	chain->Add(0x03bb, CollectParameters<1>);
	chain->Add(0x03bc, CollectParameters<5>);
	chain->Add(0x03bd, CollectParameters<1>);
	chain->Add(0x03bf, CollectParameters<2>);
	chain->Add(0x03C0, CollectParameters<2>);
	chain->Add(0x03c1, CollectParameters<2>);
	chain->Add(0x03c2, CollectParameters<1>);
	chain->Add(0x03C3, CollectParameters<3>);
	chain->Add(0x03c4, CollectParameters<3>);
	chain->Add(0x03c5, CollectParameters<4>);
	chain->Add(0x03C6, CollectParameters<1>);
	chain->Add(0x03C7, CollectParameters<1>);
	chain->Add(0x03C8, CollectParameters<0>);
	chain->Add(0x03c9, CollectParameters<1>);
	chain->Add(0x03ca, CollectParameters<1>);
	chain->Add(0x03CB, CollectParameters<3>);
	chain->Add(0x03cc, CollectParameters<3>);
	chain->Add(0x03cd, CollectParameters<1>);
	chain->Add(0x03ce, CollectParameters<1>);
	chain->Add(0x03CF, CollectParameters<2>);
	chain->Add(0x03D0, CollectParameters<1>);
	chain->Add(0x03D1, CollectParameters<1>);
	chain->Add(0x03D2, CollectParameters<1>);
	chain->Add(0x03d3, CollectParameters<7>);
	chain->Add(0x03d4, CollectParameters<2>);
	chain->Add(0x03D5, CollectParameters<1>);
	chain->Add(0x03d6, CollectParameters<1>);
	chain->Add(0x03D7, CollectParameters<4>);
	chain->Add(0x03d8, CollectParameters<0>);
	chain->Add(0x03d9, CollectParameters<0>);
	chain->Add(0x03da, CollectParameters<1>);
	chain->Add(0x03dc, CollectParameters<2>);
	chain->Add(0x03dd, CollectParameters<3>);
	chain->Add(0x03DE, CollectParameters<1>);
	chain->Add(0x03df, CollectParameters<1>);
	chain->Add(0x03E0, CollectParameters<1>);
	chain->Add(0x03e1, CollectParameters<1>);
	chain->Add(0x03e2, CollectParameters<1>);
	chain->Add(0x03E3, CollectParameters<1>);
	chain->Add(0x03E4, CollectParameters<1>);
	chain->Add(0x03E5, CollectParameters<1>);
	chain->Add(0x03e6, CollectParameters<0>);
	chain->Add(0x03E7, CollectParameters<1>);
	chain->Add(0x03ea, CollectParameters<1>);
	chain->Add(0x03eb, CollectParameters<0>);
	chain->Add(0x03ec, CollectParameters<0>);
	chain->Add(0x03ed, CollectParameters<2>);
	chain->Add(0x03ee, CollectParameters<1>);
	chain->Add(0x03ef, CollectParameters<1>);
	chain->Add(0x03f0, CollectParameters<1>);
	chain->Add(0x03f1, CollectParameters<2>);
	chain->Add(0x03f2, CollectParameters<2>);
	chain->Add(0x03f3, CollectParameters<3>);
	chain->Add(0x03F4, CollectParameters<1>);
	chain->Add(0x03F5, CollectParameters<2>);
	chain->Add(0x03f7, CollectParameters<1>);
	chain->Add(0x03f8, CollectParameters<1>);
	chain->Add(0x03f9, CollectParameters<3>);
	chain->Add(0x03FB, CollectParameters<2>);
	chain->Add(0x03fc, CollectParameters<2>);
	chain->Add(0x03FD, CollectParameters<2>);
	chain->Add(0x03FE, CollectParameters<2>);
	chain->Add(0x03ff, CollectParameters<1>);
	chain->Add(0x0400, CollectParameters<7>);
	chain->Add(0x0401, CollectParameters<0>);
	chain->Add(0x0402, CollectParameters<0>);
	chain->Add(0x0403, CollectParameters<1>);
	chain->Add(0x0404, CollectParameters<0>);
	chain->Add(0x0405, CollectParameters<1>);
	chain->Add(0x0406, CollectParameters<1>);
	chain->Add(0x0407, CollectParameters<7>);
	chain->Add(0x0408, CollectParameters<1>);
	chain->Add(0x0409, CollectParameters<0>);
	chain->Add(0x040a, CollectParameters<1>);
	chain->Add(0x040b, CollectParameters<0>);
	chain->Add(0x040c, CollectParameters<0>);
	chain->Add(0x040D, CollectParameters<1>);
	chain->Add(0x0410, CollectParameters<2>);
	chain->Add(0x0411, CollectParameters<2>);
	chain->Add(0x0414, CollectParameters<2>);
	chain->Add(0x0417, CollectParameters<1>);
	chain->Add(0x0418, CollectParameters<2>);
	chain->Add(0x0419, CollectParameters<3>);
	chain->Add(0x041a, CollectParameters<3>);
	chain->Add(0x041c, CollectParameters<2>);
	chain->Add(0x041d, CollectParameters<1>);
	chain->Add(0x041e, CollectParameters<2>);
	chain->Add(0x041f, CollectParameters<1>);
	chain->Add(0x0420, CollectParameters<1>);
	chain->Add(0x0421, CollectParameters<1>);
	chain->Add(0x0422, CollectParameters<2>);
	chain->Add(0x0423, CollectParameters<2>);
	chain->Add(0x0424, CollectParameters<0>);
	chain->Add(0x0425, CollectParameters<2>);
	chain->Add(0x0426, CollectParameters<6>);
	chain->Add(0x0427, CollectParameters<6>);
	chain->Add(0x0428, CollectParameters<2>);
	chain->Add(0x042a, CollectParameters<2>);
	chain->Add(0x042b, CollectParameters<6>);
	chain->Add(0x042c, CollectParameters<1>);
	chain->Add(0x042d, CollectParameters<2>);
	chain->Add(0x042E, CollectParameters<2>);
	chain->Add(0x042f, CollectParameters<2>);
	chain->Add(0x0431, CollectParameters<2>);
	chain->Add(0x0433, CollectParameters<2>);
	chain->Add(0x0434, CollectParameters<0>);
	chain->Add(0x0435, CollectParameters<0>);
	chain->Add(0x0436, CollectParameters<0>);
	chain->Add(0x0437, CollectParameters<8>);
	chain->Add(0x0438, CollectParameters<2>);
	chain->Add(0x043A, CollectParameters<0>);
	chain->Add(0x043b, CollectParameters<1>);
	chain->Add(0x043C, CollectParameters<1>);
	chain->Add(0x043d, CollectParameters<1>);
	chain->Add(0x043f, CollectParameters<0>);
	chain->Add(0x0440, CollectParameters<0>);
	chain->Add(0x0441, CollectParameters<2>);
	chain->Add(0x0442, CollectParameters<2>);
	chain->Add(0x0443, CollectParameters<1>);
	chain->Add(0x0444, CollectParameters<2>);
	chain->Add(0x0445, CollectParameters<0>);
	chain->Add(0x0446, CollectParameters<2>);
	chain->Add(0x0447, CollectParameters<1>);
	chain->Add(0x0448, CollectParameters<2>);
	chain->Add(0x0449, CollectParameters<1>);
	chain->Add(0x044A, CollectParameters<1>);
	chain->Add(0x044B, CollectParameters<1>);
	chain->Add(0x044C, CollectParameters<1>);
	chain->Add(0x044D, CollectParameters<1>);
	chain->Add(0x044e, CollectParameters<2>);
	chain->Add(0x044f, CollectParameters<2>);
	chain->Add(0x0450, CollectParameters<1>);
	chain->Add(0x0451, CollectParameters<0>);
	chain->Add(0x0453, CollectParameters<4>);
	chain->Add(0x0454, CollectParameters<3>);
	chain->Add(0x0457, CollectParameters<2>);
	chain->Add(0x0459, CollectParameters<1>);
	chain->Add(0x045A, CollectParameters<4>);
	chain->Add(0x045B, CollectParameters<5>);
	chain->Add(0x045F, CollectParameters<2>);
	chain->Add(0x0460, CollectParameters<2>);
	chain->Add(0x0463, CollectParameters<3>);
	chain->Add(0x0464, CollectParameters<8>);
	chain->Add(0x0465, CollectParameters<1>);
	chain->Add(0x0466, CollectParameters<2>);
	chain->Add(0x0467, CollectParameters<1>);
	chain->Add(0x0468, CollectParameters<1>);
	chain->Add(0x0469, CollectParameters<10>);
	chain->Add(0x046B, CollectParameters<2>);
	chain->Add(0x046C, CollectParameters<2>);
	chain->Add(0x046D, CollectParameters<2>);
	chain->Add(0x046E, CollectParameters<6>);
	chain->Add(0x046F, CollectParameters<2>);
	chain->Add(0x0470, CollectParameters<2>);
	chain->Add(0x0471, CollectParameters<5>);
	chain->Add(0x0472, CollectParameters<5>);
	chain->Add(0x0477, CollectParameters<3>);
	chain->Add(0x047A, CollectParameters<1>);
	chain->Add(0x047E, CollectParameters<1>);
	chain->Add(0x0480, CollectParameters<2>);
	chain->Add(0x0481, CollectParameters<1>);
	chain->Add(0x0482, CollectParameters<1>);
	chain->Add(0x0483, CollectParameters<2>);
	chain->Add(0x0484, CollectParameters<2>);
	chain->Add(0x0489, CollectParameters<2>);
	chain->Add(0x048A, CollectParameters<1>);
	chain->Add(0x048B, CollectParameters<2>);
	chain->Add(0x048C, CollectParameters<3>);
	chain->Add(0x048F, CollectParameters<1>);
	chain->Add(0x0490, CollectParameters<2>);
	chain->Add(0x0493, CollectParameters<2>);
	chain->Add(0x0494, CollectParameters<5>);
	chain->Add(0x0495, CollectParameters<1>);
	chain->Add(0x0496, CollectParameters<2>);
	chain->Add(0x049C, CollectParameters<3>);
	chain->Add(0x049D, CollectParameters<2>);
	chain->Add(0x049E, CollectParameters<2>);
	chain->Add(0x049F, CollectParameters<2>);
	chain->Add(0x04A1, CollectParameters<1>);
	chain->Add(0x04A2, CollectParameters<5>);
	chain->Add(0x04A3, CollectParameters<2>);
	chain->Add(0x04A4, CollectParameters<2>);
	chain->Add(0x04A5, CollectParameters<4>);
	chain->Add(0x04A6, CollectParameters<6>);
	chain->Add(0x04A8, CollectParameters<1>);
	chain->Add(0x04AA, CollectParameters<1>);
	chain->Add(0x04AD, CollectParameters<1>);
	chain->Add(0x04AE, CollectParameters<2>);
	chain->Add(0x04AF, CollectParameters<2>);
	chain->Add(0x04B0, CollectParameters<2>);
	chain->Add(0x04B1, CollectParameters<2>);
	chain->Add(0x04B2, CollectParameters<2>);
	chain->Add(0x04B3, CollectParameters<2>);
	chain->Add(0x04B4, CollectParameters<2>);
	chain->Add(0x04B5, CollectParameters<2>);
	chain->Add(0x04B6, CollectParameters<2>);
	chain->Add(0x04B7, CollectParameters<2>);
	chain->Add(0x04B8, CollectParameters<5>);
	chain->Add(0x04B9, CollectParameters<12>);
	chain->Add(0x04BA, CollectParameters<2>);
	chain->Add(0x04BB, CollectParameters<1>);
	chain->Add(0x04BC, CollectParameters<1>);
	chain->Add(0x04BD, CollectParameters<2>);
	chain->Add(0x04BE, CollectParameters<1>);
	chain->Add(0x04BF, CollectParameters<2>);
	chain->Add(0x04C0, CollectParameters<6>);
	chain->Add(0x04C1, CollectParameters<0>);
	chain->Add(0x04C2, CollectParameters<2>);
	chain->Add(0x04C4, CollectParameters<7>);
	chain->Add(0x04C5, CollectParameters<1>);
	chain->Add(0x04C6, CollectParameters<2>);
	chain->Add(0x04C7, CollectParameters<1>);
	chain->Add(0x04C9, CollectParameters<1>);
	chain->Add(0x04CE, CollectParameters<5>);
	chain->Add(0x04CF, CollectParameters<1>);
	chain->Add(0x04D0, CollectParameters<2>);
	chain->Add(0x04D1, CollectParameters<1>);
	chain->Add(0x04D2, CollectParameters<5>);
	chain->Add(0x04D3, CollectParameters<7>);
	chain->Add(0x04D5, CollectParameters<9>);
	chain->Add(0x04D6, CollectParameters<1>);
	chain->Add(0x04D7, CollectParameters<2>);
	chain->Add(0x04D8, CollectParameters<2>);
	chain->Add(0x04D9, CollectParameters<2>);
	chain->Add(0x04DA, CollectParameters<1>);
	chain->Add(0x04DB, CollectParameters<0>);
	chain->Add(0x04DD, CollectParameters<2>);
	chain->Add(0x04DF, CollectParameters<2>);
	chain->Add(0x04E0, CollectParameters<2>);
	chain->Add(0x04E1, CollectParameters<1>);
	chain->Add(0x04E2, CollectParameters<2>);
	chain->Add(0x04E3, CollectParameters<3>);
	chain->Add(0x04E4, CollectParameters<2>);
	chain->Add(0x04E5, CollectParameters<6>);
	chain->Add(0x04E6, CollectParameters<8>);
	chain->Add(0x04E7, CollectParameters<1>);
	chain->Add(0x04E9, CollectParameters<6>);
	chain->Add(0x04EA, CollectParameters<8>);
	chain->Add(0x04EB, CollectParameters<3>);
	chain->Add(0x04EC, CollectParameters<13>);
	chain->Add(0x04ED, CollectParameters<1>);
	chain->Add(0x04EE, CollectParameters<1>);
	chain->Add(0x04EF, CollectParameters<1>);
	chain->Add(0x04F0, CollectParameters<1>);
	chain->Add(0x04F1, CollectParameters<1>);
	chain->Add(0x04F3, CollectParameters<1>);
	chain->Add(0x04F4, CollectParameters<8>);
	chain->Add(0x04F5, CollectParameters<3>);
	chain->Add(0x04F7, CollectParameters<4>);
	chain->Add(0x04F8, CollectParameters<13>);
	chain->Add(0x04F9, CollectParameters<2>);
	chain->Add(0x04FA, CollectParameters<1>);
	chain->Add(0x04FC, CollectParameters<7>);
	chain->Add(0x04FE, CollectParameters<2>);
	chain->Add(0x04FF, CollectParameters<1>);
	chain->Add(0x0500, CollectParameters<2>);
	chain->Add(0x0501, CollectParameters<2>);
	chain->Add(0x0502, CollectParameters<3>);
	chain->Add(0x0503, CollectParameters<3>);
	chain->Add(0x0506, CollectParameters<3>);
	chain->Add(0x0507, CollectParameters<1>);
	chain->Add(0x0508, CollectParameters<1>);
	chain->Add(0x0509, CollectParameters<5>);
	chain->Add(0x050A, CollectParameters<7>);
	chain->Add(0x050B, CollectParameters<1>);
	chain->Add(0x050D, CollectParameters<1>);
	chain->Add(0x050E, CollectParameters<2>);
	chain->Add(0x0510, CollectParameters<5>);
	chain->Add(0x0512, CollectParameters<1>);
	chain->Add(0x0514, CollectParameters<3>);
	chain->Add(0x0517, CollectParameters<5>);
	chain->Add(0x0518, CollectParameters<6>);
	chain->Add(0x0519, CollectParameters<2>);
	chain->Add(0x051A, CollectParameters<2>);
	chain->Add(0x0521, CollectParameters<1>);
	chain->Add(0x0522, CollectParameters<0>);
	chain->Add(0x0523, CollectParameters<3>);
	chain->Add(0x0524, CollectParameters<3>);
	chain->Add(0x0525, CollectParameters<3>);
	chain->Add(0x0526, CollectParameters<2>);
	chain->Add(0x0528, CollectParameters<1>);
	chain->Add(0x0529, CollectParameters<1>);
	chain->Add(0x052B, CollectParameters<2>);
	chain->Add(0x052C, CollectParameters<2>);
	chain->Add(0x0531, CollectParameters<1>);
	chain->Add(0x0533, CollectParameters<1>);
	chain->Add(0x0534, CollectParameters<1>);
	chain->Add(0x0536, CollectParameters<1>);
	chain->Add(0x053C, CollectParameters<2>);
	chain->Add(0x053D, CollectParameters<1>);
	chain->Add(0x053E, CollectParameters<6>);
	chain->Add(0x053F, CollectParameters<2>);
	chain->Add(0x0540, CollectParameters<2>);
	chain->Add(0x0541, CollectParameters<1>);
	chain->Add(0x0542, CollectParameters<1>);
	chain->Add(0x0543, CollectParameters<1>);
	chain->Add(0x0544, CollectParameters<1>);
	chain->Add(0x0545, CollectParameters<0>);
	chain->Add(0x0546, CollectParameters<2>);
	chain->Add(0x0548, CollectParameters<6>);
	chain->Add(0x0549, CollectParameters<1>);
	chain->Add(0x054A, CollectParameters<2>);
	chain->Add(0x054B, CollectParameters<2>);
	chain->Add(0x054C, CollectParameters<1>);
	chain->Add(0x054D, CollectParameters<1>);
	chain->Add(0x054E, CollectParameters<1>);
	chain->Add(0x0550, CollectParameters<2>);
	chain->Add(0x0551, CollectParameters<1>);
	chain->Add(0x0552, CollectParameters<1>);
	chain->Add(0x0556, CollectParameters<8>);
	chain->Add(0x0557, CollectParameters<0>);
	chain->Add(0x055A, CollectParameters<1>);
	chain->Add(0x055B, CollectParameters<5>);
	chain->Add(0x055D, CollectParameters<2>);
	chain->Add(0x055E, CollectParameters<2>);
	chain->Add(0x055F, CollectParameters<2>);
	chain->Add(0x0560, CollectParameters<2>);
	chain->Add(0x0561, CollectParameters<3>);
	chain->Add(0x0562, CollectParameters<2>);
	chain->Add(0x0563, CollectParameters<2>);
	chain->Add(0x0564, CollectParameters<1>);
	chain->Add(0x0565, CollectParameters<4>);
	chain->Add(0x0566, CollectParameters<2>);
	chain->Add(0x0568, CollectParameters<2>);
	chain->Add(0x0569, CollectParameters<1>);
	chain->Add(0x056A, CollectParameters<0>);
	chain->Add(0x056B, CollectParameters<2>);
	chain->Add(0x056C, CollectParameters<1>);
	chain->Add(0x056D, CollectParameters<1>);
	chain->Add(0x0570, CollectParameters<5>);
	chain->Add(0x0571, CollectParameters<1>);
	chain->Add(0x0572, CollectParameters<1>);
	chain->Add(0x0573, CollectParameters<2>);
	chain->Add(0x0574, CollectParameters<2>);
	chain->Add(0x0575, CollectParameters<2>);
	chain->Add(0x0578, CollectParameters<1>);
	chain->Add(0x0579, CollectParameters<1>);
	chain->Add(0x057A, CollectParameters<2>);
	chain->Add(0x057B, CollectParameters<0>);
	chain->Add(0x057C, CollectParameters<1>);
	chain->Add(0x057D, CollectParameters<1>);
	chain->Add(0x057E, CollectParameters<1>);
	chain->Add(0x057F, CollectParameters<2>);
	chain->Add(0x0580, CollectParameters<3>);
	chain->Add(0x0581, CollectParameters<1>);
	chain->Add(0x0582, CollectParameters<2>);
	chain->Add(0x0583, CollectParameters<2>);
	chain->Add(0x0585, CollectParameters<0>);
	chain->Add(0x0586, CollectParameters<1>);
	chain->Add(0x0587, CollectParameters<2>);
	chain->Add(0x0588, CollectParameters<2>);
	chain->Add(0x058A, CollectParameters<6>);
	chain->Add(0x058B, CollectParameters<1>);
	chain->Add(0x058C, CollectParameters<1>);
	chain->Add(0x058D, CollectParameters<4>);
	chain->Add(0x058E, CollectParameters<4>);
	chain->Add(0x058F, CollectParameters<8>);
	chain->Add(0x0591, CollectParameters<4>);
	chain->Add(0x0592, CollectParameters<2>);
	chain->Add(0x0593, CollectParameters<2>);
	chain->Add(0x0594, CollectParameters<2>);
	chain->Add(0x0595, CollectParameters<0>);
	chain->Add(0x0596, CollectParameters<1>);
	chain->Add(0x0598, CollectParameters<3>);
	chain->Add(0x0599, CollectParameters<1>);
	chain->Add(0x059A, CollectParameters<0>);
	chain->Add(0x059B, CollectParameters<1>);
	chain->Add(0x059F, CollectParameters<4>);
	chain->Add(0x05A1, CollectParameters<4>);
	chain->Add(0x05A2, CollectParameters<4>);
	chain->Add(0x05A3, CollectParameters<1>);
	chain->Add(0x05A4, CollectParameters<5>);
	chain->Add(0x05A6, CollectParameters<4>);
	chain->Add(0x05A7, CollectParameters<4>);
	chain->Add(0x05A8, CollectParameters<2>);
	chain->Add(0x05B0, CollectParameters<10>);
#pragma endregion

	//chain->Add(0x0054, GetCoordinates<2, 3, 4>);
	//chain->Add(0x0055, SetCoordinates<2, 3, 4>);
	//chain->Add(0x0056, SetCoordinates<2, 3>);
	//chain->Add(0x0056, SetCoordinates<4, 5>);
	//chain->Add(0x0057, SetCoordinates<2, 3, 4>);
	//chain->Add(0x0057, SetCoordinates<5, 6, 7>);

	chain->Add(0x023C, SpecialCharName<1>);
	chain->Add(0x023C, SpecialChar<2>);

	chain->Add(0x023D, SpecialChar<1>);

	chain->Add(0x0296, SpecialChar<1>);

	chain->Add(0x009A, SetCoordinates<3, 4, 5>);
	chain->Add(0x009E, SetCoordinates<2, 3, 4>);
	chain->Add(0x00A0, GetCoordinates<2, 3, 4>);
	chain->Add(0x00A1, SetCoordinates<2, 3, 4>);
	chain->Add(0x00A3, SetCoordinates<2, 3>);
	chain->Add(0x00A3, SetCoordinates<4, 5>);
	chain->Add(0x00A4, SetCoordinates<2, 3, 4>);
	chain->Add(0x00A4, SetCoordinates<5, 6, 7>);

	chain->Add(0x00A5, SetCoordinates<2, 3, 4>);
	chain->Add(0x00A7, SetCoordinates<2, 3, 4>);
	chain->Add(0x00AA, GetCoordinates<2, 3, 4>);
	chain->Add(0x00AB, SetCoordinates<2, 3, 4>);

	chain->Add(0x00B0, SetCoordinates<2, 3>);
	chain->Add(0x00B0, SetCoordinates<4, 5>);

	chain->Add(0x00B1, SetCoordinates<2, 3, 4>);
	chain->Add(0x00B1, SetCoordinates<5, 6, 7>);

	chain->Add(0x00EC, SetCoordinates<2, 3>);
	chain->Add(0x00ED, SetCoordinates<2, 3>);
	chain->Add(0x00EE, SetCoordinates<2, 3>);
	chain->Add(0x00EF, SetCoordinates<2, 3>);
	chain->Add(0x00F0, SetCoordinates<2, 3>);
	chain->Add(0x00F1, SetCoordinates<2, 3>);

	chain->Add(0x00FE, SetCoordinates<2, 3, 4>);
	chain->Add(0x00FF, SetCoordinates<2, 3, 4>);
	chain->Add(0x0100, SetCoordinates<2, 3, 4>);
	chain->Add(0x0101, SetCoordinates<2, 3, 4>);
	chain->Add(0x0102, SetCoordinates<2, 3, 4>);
	chain->Add(0x0103, SetCoordinates<2, 3, 4>);

	chain->Add(0x0107, SetCoordinates<2, 3, 4>);
	chain->Add(0x010C, SetCoordinates<2, 3, 4>);
	//chain->Add(0x012A, SetCoordinates<2, 3, 4>);
	chain->Add(0x014B, SetCoordinates<1, 2, 3>);
	chain->Add(0x015F, SetCoordinates<1, 2, 3>);
	chain->Add(0x0160, SetCoordinates<1, 2, 3>);

	chain->Add(0x0167, SetCoordinates<1, 2, 3>);
	chain->Add(0x016E, SetCoordinates<1, 2, 3>);

	chain->Add(0x0189, SetCoordinates<1, 2, 3>);
	chain->Add(0x018A, SetCoordinates<1, 2, 3>);

	chain->Add(0x018C, SetCoordinates<1, 2, 3>);
	chain->Add(0x018D, SetCoordinates<1, 2, 3>);

	chain->Add(0x01AD, SetCoordinates<2, 3>);
	chain->Add(0x01AE, SetCoordinates<2, 3>);
	chain->Add(0x01AF, SetCoordinates<2, 3, 4>);
	chain->Add(0x01B0, SetCoordinates<2, 3, 4>);

	chain->Add(0x01BB, GetCoordinates<2, 3, 4>);
	chain->Add(0x01BC, SetCoordinates<2, 3, 4>);

	chain->Add(0x01E2, SetCoordinates<2, 3, 4>);

	chain->Add(0x01E7, SetCoordinates<1, 2, 3>);
	chain->Add(0x01E7, SetCoordinates<4, 5, 6>);
	chain->Add(0x01E8, SetCoordinates<1, 2, 3>);
	chain->Add(0x01E8, SetCoordinates<4, 5, 6>);

	chain->Add(0x020C, SetCoordinates<1, 2, 3>);

	chain->Add(0x0213, SetCoordinates<3, 4, 5>);

	chain->Add(0x022A, SetCoordinates<1, 2, 3>);
	chain->Add(0x022A, SetCoordinates<4, 5, 6>);
	chain->Add(0x022B, SetCoordinates<1, 2, 3>);
	chain->Add(0x022B, SetCoordinates<4, 5, 6>);

	chain->Add(0x0239, SetCoordinates<2, 3>);

	chain->Add(0x0244, SetCoordinates<1, 2, 3>);

	chain->Add(0x024a, SetCoordinates<1, 2>);
	chain->Add(0x024F, SetCoordinates<1, 2, 3>);
	chain->Add(0x0250, SetCoordinates<1, 2, 3>);

	chain->Add(0x0255, SetCoordinates<1, 2, 3>);

	chain->Add(0x029B, SetCoordinates<1, 2, 3>);
	chain->Add(0x024F, SetCoordinates<2, 3, 4>);
	chain->Add(0x02A7, SetCoordinates<1, 2, 3>);
	chain->Add(0x02A8, SetCoordinates<1, 2, 3>);

	chain->Add(0x02C0, SetCoordinates<1, 2, 3>);
	chain->Add(0x02C0, GetCoordinates<4, 5, 6>);

	chain->Add(0x02C1, SetCoordinates<1, 2, 3>);
	chain->Add(0x02C1, GetCoordinates<4, 5, 6>);

	chain->Add(0x02C2, SetCoordinates<2, 3, 4>);

	chain->Add(0x02CE, SetCoordinates<1, 2, 3>);
	chain->Add(0x02CF, SetCoordinates<1, 2, 3>);

	chain->Add(0x02D3, SetCoordinates<2, 3, 4>);

	chain->Add(0x02D5, SetCoordinates<2, 3>);
	chain->Add(0x02D5, SetCoordinates<4, 5>);

	chain->Add(0x02E1, SetCoordinates<1, 2, 3>);
	chain->Add(0x02EC, SetCoordinates<1, 2, 3>);

	chain->Add(0x02EF, SetCoordinates<1, 2, 3>);
	chain->Add(0x02EF, SetCoordinates<4, 5, 6>);

	chain->Add(0x02F1, SetCoordinates<1, 2, 3>);

	chain->Add(0x0327, SetCoordinates<1, 2>);
	chain->Add(0x0327, SetCoordinates<3, 4>);

	chain->Add(0x032B, SetCoordinates<4, 5, 6>);

	chain->Add(0x0339, SetCoordinates<1, 2, 3>);
	chain->Add(0x0339, SetCoordinates<4, 5, 6>);

	chain->Add(0x034E, SetCoordinates<2, 3, 4>);
	chain->Add(0x0362, SetCoordinates<2, 3, 4>);

	chain->Add(0x0363, SetCoordinates<1, 2, 3>);

	chain->Add(0x037E, SetCoordinates<1, 2, 3>);
	chain->Add(0x037E, SetCoordinates<4, 5, 6>);

	chain->Add(0x038A, SetCoordinates<1, 2, 3>);
	chain->Add(0x038A, SetCoordinates<4, 5, 6>);

	chain->Add(0x0395, SetCoordinates<1, 2>);

	chain->Add(0x039F, SetCoordinates<2, 3>);

	chain->Add(0x03A1, SetCoordinates<1, 2, 3>);

	chain->Add(0x03AA, SetCoordinates<1, 2, 3>);

	chain->Add(0x03AE, SetCoordinates<1, 2, 3>);
	chain->Add(0x03AE, SetCoordinates<4, 5, 6>);

	chain->Add(0x03B6, SetCoordinates<1, 2, 3>);
	chain->Add(0x03BA, SetCoordinates<1, 2, 3>);
	chain->Add(0x03BA, SetCoordinates<4, 5, 6>);

	chain->Add(0x03BC, SetCoordinates<1, 2, 3>);

	chain->Add(0x03C5, SetCoordinates<1, 2, 3>);

	chain->Add(0x03CB, SetCoordinates<1, 2, 3>);

	chain->Add(0x03D3, SetCoordinates<1, 2, 3>);
	chain->Add(0x03D3, GetCoordinates<4, 5, 6>);

	chain->Add(0x03D7, SetCoordinates<2, 3, 4>);

	chain->Add(0x0400, GetCoordinates<5, 6, 7>);
	chain->Add(0x0407, GetCoordinates<5, 6, 7>);

	chain->Add(0x042B, SetCoordinates<1, 2, 3>);
	chain->Add(0x042B, SetCoordinates<4, 5, 6>);

	chain->Add(0x0469, SetCoordinates<1, 2>);
	chain->Add(0x0469, SetCoordinates<3, 4>);

	chain->Add(0x046E, SetCoordinates<2, 3, 4>);

	chain->Add(0x048C, SetCoordinates<1, 2, 3>);

	chain->Add(0x04A5, GetCoordinates<2, 3, 4>);
	chain->Add(0x04A6, SetCoordinates<1, 2, 3>);

	chain->Add(0x04B9, SetCoordinates<1, 2, 3>);
	chain->Add(0x04B9, GetCoordinates<6, 7, 8>);
	chain->Add(0x04B9, GetCoordinates<9, 10, 11>);

	chain->Add(0x04C0, SetCoordinates<1, 2, 3>);
	chain->Add(0x04C0, SetCoordinates<4, 5, 6>);

	chain->Add(0x04C4, GetCoordinates<5, 6, 7>);

	chain->Add(0x04CE, SetCoordinates<1, 2, 3>);

	chain->Add(0x04D2, SetCoordinates<2, 3, 4>);
	chain->Add(0x04D3, SetCoordinates<1, 2, 3>);
	chain->Add(0x04D3, GetCoordinates<5, 6, 7>);

	chain->Add(0x04D5, SetCoordinates<1, 2, 3>);

	chain->Add(0x04E5, SetCoordinates<2, 3>);
	chain->Add(0x04E6, SetCoordinates<2, 3, 4>);
	
	chain->Add(0x04E9, SetCoordinates<2, 3>);
	chain->Add(0x04E9, SetCoordinates<4, 5>);

	chain->Add(0x04EA, SetCoordinates<2, 3, 4>);
	chain->Add(0x04EA, SetCoordinates<5, 6, 7>);

	chain->Add(0x04F8, SetCoordinates<2, 3>);
	chain->Add(0x04F8, SetCoordinates<4, 5>);
	chain->Add(0x04F8, SetCoordinates<6, 7>);
	chain->Add(0x04F8, SetCoordinates<8, 9>);
	chain->Add(0x04F8, SetCoordinates<10, 11>);

	chain->Add(0x0509, SetCoordinates<1, 2>);
	chain->Add(0x0509, SetCoordinates<3, 4>);

	chain->Add(0x050A, SetCoordinates<1, 2, 3>);
	chain->Add(0x050A, SetCoordinates<4, 5, 6>);

	chain->Add(0x0510, SetCoordinates<2, 3, 4>);

	chain->Add(0x0517, SetCoordinates<1, 2, 3>);
	chain->Add(0x0518, SetCoordinates<1, 2, 3>);

	chain->Add(0x0523, SetCoordinates<1, 2, 3>);

	chain->Add(0x053E, SetCoordinates<1, 2>);
	chain->Add(0x053E, SetCoordinates<3, 4>);

	chain->Add(0x0570, SetCoordinates<1, 2, 3>);

	chain->Add(0x05FC, SetCoordinates<2, 3, 4>);
	chain->Add(0x05FC, SetCoordinates<5, 6, 7>);

	chain->Add(0x05FD, SetCoordinates<2, 3, 4>);
	chain->Add(0x05FD, SetCoordinates<5, 6, 7>);

	chain->Add(0x05FE, SetCoordinates<2, 3, 4>);
	chain->Add(0x05FE, SetCoordinates<5, 6, 7>);

	chain->Add(0x05F6, SetCoordinates<2, 3>);
	chain->Add(0x05F6, SetCoordinates<4, 5>);

	chain->Add(0x009A, Object<2>);
	chain->Add(0x00A5, Object<1>);
	chain->Add(0x00DD, Object<2>);
	chain->Add(0x0107, Object<1>);
	chain->Add(0x0213, Object<1>);
	chain->Add(0x0247, Object<1>);
	chain->Add(0x0248, Object<1>);
	chain->Add(0x0249, Object<1>);
	chain->Add(0x029b, Object<1>);
	chain->Add(0x02E5, Object<1>);
	chain->Add(0x02F2, Object<2>);
	chain->Add(0x02F3, Object<1>);
	chain->Add(0x02f4, Object<2>);
	chain->Add(0x0363, Object<5>);
	chain->Add(0x03b6, Object<5>);
	chain->Add(0x03b6, Object<6>);

	chain->Add(0x014B, ObjectRaw<5>);

	chain->Add(0x0054, PlayerRechain<1, 0x00A0>);
	chain->Add(0x0055, PlayerRechain<1, 0x00A1>);
	chain->Add(0x0056, PlayerRechain<1, 0x00A3>);
	chain->Add(0x0057, PlayerRechain<1, 0x00A4>);
	chain->Add(0x00DA, PlayerRechain<1, 0x00D9>);
	chain->Add(0x00DC, PlayerRechain<1, 0x00DB>);
	chain->Add(0x00DE, PlayerRechain<1, 0x00DD>);
	chain->Add(0x00E0, PlayerRechain<1, 0x00DF>);
	chain->Add(0x00E3, PlayerRechain<1, 0x00EC>);
	chain->Add(0x00E4, PlayerRechain<1, 0x00ED>);
	chain->Add(0x00E5, PlayerRechain<1, 0x00EE>);
	chain->Add(0x00E6, PlayerRechain<1, 0x00EF>);
	chain->Add(0x00E7, PlayerRechain<1, 0x00F0>);
	chain->Add(0x00E8, PlayerRechain<1, 0x00F1>);
	chain->Add(0x00E9, PlayerRechain<1, 0x00F2>);
	chain->Add(0x00EA, PlayerRechain<1, 0x00F3>);
	chain->Add(0x00EB, PlayerRechain<1, 0x00F4>);
	chain->Add(0x00F5, PlayerRechain<1, 0x00FE>);
	chain->Add(0x00F6, PlayerRechain<1, 0x00FF>);
	chain->Add(0x00F7, PlayerRechain<1, 0x0100>);
	chain->Add(0x00F8, PlayerRechain<1, 0x0101>);
	chain->Add(0x00F9, PlayerRechain<1, 0x0102>);
	chain->Add(0x00FA, PlayerRechain<1, 0x0103>);
	chain->Add(0x00FB, PlayerRechain<1, 0x0104>);
	chain->Add(0x00FC, PlayerRechain<1, 0x0105>);
	chain->Add(0x00FD, PlayerRechain<1, 0x0106>);
	//chain->Add(0x0106, PlayerRechain<1, 0x00FE>);
	chain->Add(0x0113, PlayerRechain<1, 0x0114>);
	chain->Add(0x0121, PlayerRechain<1, 0x0154>);
	
	// needs fix
	//chain->Add(0x0123, PlayerRechain<1, 0x00FE>);

	chain->Add(0x012A, PlayerRechain<1, 0x0362>);
	chain->Add(0x0157, PlayerRechain<1, 0x0159>);
	chain->Add(0x0170, PlayerRechain<1, 0x0172>);
	chain->Add(0x0171, PlayerRechain<1, 0x0173>);
	chain->Add(0x0178, PlayerRechain<1, 0x0179>);
	chain->Add(0x017a, PlayerRechain<1, 0x017b>);

	chain->Add(0x0183, PlayerRechain<1, 0x0184>);

	chain->Add(0x0197, PlayerRechain<1, 0x01A1>);
	chain->Add(0x0198, PlayerRechain<1, 0x01A2>);
	chain->Add(0x0199, PlayerRechain<1, 0x01A3>);
	chain->Add(0x019A, PlayerRechain<1, 0x01A4>);
	chain->Add(0x019B, PlayerRechain<1, 0x01A5>);
	chain->Add(0x019C, PlayerRechain<1, 0x01A6>);
	chain->Add(0x019D, PlayerRechain<1, 0x01A7>);
	chain->Add(0x019E, PlayerRechain<1, 0x01A8>);
	chain->Add(0x019F, PlayerRechain<1, 0x01A9>);
	chain->Add(0x01A0, PlayerRechain<1, 0x01AA>);

	chain->Add(0x01B1, PlayerRechain<1, 0x01B2>);
	chain->Add(0x01B8, PlayerRechain<1, 0x01B9>);

	chain->Add(0x01CA, PlayerRechain<2, 0x01C9>);
	chain->Add(0x01CC, PlayerRechain<2, 0x01CB>);
	//chain->Add(0x01CE, PlayerRechain<1, 0x01C9>);
	//chain->Add(0x01CA, PlayerRechain<1, 0x01C9>);
	chain->Add(0x01D2, PlayerRechain<2, 0x01D1>);
	chain->Add(0x01DF, PlayerRechain<2, 0x01DE>);

	chain->Add(0x01FC, PlayerRechain<1, 0x0202>);
	chain->Add(0x01FD, PlayerRechain<1, 0x0203>);
	chain->Add(0x01FE, PlayerRechain<1, 0x0204>);
	chain->Add(0x01FF, PlayerRechain<1, 0x0205>);
	chain->Add(0x0200, PlayerRechain<1, 0x0206>);
	chain->Add(0x0201, PlayerRechain<1, 0x0207>);

	chain->Add(0x020F, PlayerRechain<2, 0x020E>);
	chain->Add(0x0210, PlayerRechain<1, 0x020E>);

	chain->Add(0x0222, PlayerRechain<1, 0x0223>);
	chain->Add(0x0225, PlayerRechain<1, 0x0226>);

	chain->Add(0x022D, PlayerRechain<2, 0x022C>);
	chain->Add(0x022E, PlayerRechain<1, 0x022C>);

	chain->Add(0x0230, PlayerRechain<1, 0x022F>);

	chain->Add(0x023A, PlayerRechain<1, 0x023B>);

	chain->Add(0x029F, PlayerRechain<1, 0x02A0>);

	//chain->Add(0x02AD, RemoveArg<6>);
	chain->Add(0x02AD, PlayerRechain<1, 0x05F6>);

	//chain->Add(0x02B3, RemoveArg<8>);
	chain->Add(0x02B3, PlayerRechain<1, 0x05FC>);

	//chain->Add(0x02B4, RemoveArg<8>);
	chain->Add(0x02B4, PlayerRechain<1, 0x05FD>);

	//chain->Add(0x02B5, RemoveArg<8>);
	chain->Add(0x02B5, PlayerRechain<1, 0x05FE>);

	chain->Add(0x0322, PlayerRechain<1, 0x0321>);

	chain->Add(0x0336, PlayerRechain<1, 0x0337>);

	chain->Add(0x035E, PlayerRechain<1, 0x035F>);
	chain->Add(0x0369, PlayerRechain<1, 0x036A>);

	chain->Add(0x03C1, PlayerRechain<1, 0x03C0>);
	chain->Add(0x0419, PlayerRechain<1, 0x041A>);

	chain->Add(0x0442, PlayerRechain<1, 0x0448>);
	chain->Add(0x0443, PlayerRechain<1, 0x0449>);
	chain->Add(0x044A, PlayerRechain<1, 0x044B>);

	chain->Add(0x046F, PlayerRechain<1, 0x0470>);

	chain->Add(0x047E, PlayerRechain<1, 0x047A>);

	//chain->Add(0x0442, PlayerRechain<1, 0x0448>);
	
	//chain->Add(0x0570, SetCoordinates<1, 2, 3>);

	chain->Add(0x055B, [] (HostilityScript* script)
	{
		scriptParameters[0] = ScriptParameter(1247);
		scriptParameters[1] = ScriptParameter(15);
		scriptParameters[2] = scriptParameters[0];
		scriptParameters[3] = scriptParameters[1];
		scriptParameters[4] = scriptParameters[2];
		scriptParameters[5] = scriptParameters[4];

		numScriptParameters = 6;

		script->RechainCommand(0x0213);
	});

	chain->Add(0x0001, [] (HostilityScript* script)
	{
		script->Wait(scriptParameters[0].GetIntValue());
	});
	
	chain->Add(0x0002, [] (HostilityScript* script)
	{
		script->SetPC(scriptParameters[0].GetIntValue());
	});

	chain->Add(0x004D, [] (HostilityScript* script)
	{
		if (!script->GetCompareFlag())
		{
			script->SetPC(scriptParameters[0].GetIntValue());
		}
	});

	chain->Add(0x004E, [] (HostilityScript* script)
	{
		script->GetRuntime()->StopScript(script);
	});

	chain->Add(0x004F, [] (HostilityScript* script)
	{
		script->GetRuntime()->StartNewScript(/*script->GetRuntime()->GetMainStart() + */scriptParameters[0].GetIntValue());
	});

	chain->Add(0x03A4, [] (HostilityScript* script)
	{
		script->SetName(scriptParameters[0].GetStringValue());
	});

	chain->Add(0x00D7, [] (HostilityScript* script)
	{
		script->GetRuntime()->StartNewScript(/*script->GetRuntime()->GetMainStart() + */scriptParameters[0].GetIntValue());
	});

	chain->Add(0x0050, [] (HostilityScript* script)
	{
		script->GoSub(scriptParameters[0].GetIntValue());
	});

	chain->Add(0x0051, [] (HostilityScript* script)
	{
		script->Return();
	});
	
	chain->Add(0x00D6, [] (HostilityScript* script)
	{
		script->SetAndOr(scriptParameters[0].GetIntValue());
	});

	chain->Add(0x0053, [] (HostilityScript* script)
	{
		int* nativeScriptSpace = (int*)0xA49960;

		scriptParameters[4].SetIntValue(nativeScriptSpace[2]);
	});

	chain->Add(0x01F5, [] (HostilityScript* script)
	{
		int* nativeScriptSpace = (int*)0xA49960;

		scriptParameters[1].SetIntValue(nativeScriptSpace[3]);
	});

	chain->Add(0x0417, [] (HostilityScript* script)
	{
		int missionStart = script->GetRuntime()->GetMultiScriptStart(scriptParameters[0].GetIntValue());

		FILE* f = fopen("data/main.scm", "rb");
		fseek(f, missionStart, SEEK_SET);
		fread(&script->GetRuntime()->GetScriptSpace()[225512], 1, 35000, f);
		fclose(f);

		script->GetRuntime()->StartNewScript(225512);
	});

	// this should always be last
#pragma region pass to game
	chain->Add(0x05F6, PassToGame<0x05F6>);
	chain->Add(0x05FC, PassToGame<0x05FC>);
	chain->Add(0x05FD, PassToGame<0x05FD>);
	chain->Add(0x05FE, PassToGame<0x05FE>);

	chain->Add(0x0000, PassToGame<0x0000>);
	//chain->Add(0x0001, PassToGame<0x0001>);
	//chain->Add(0x0002, PassToGame<0x0002>);
	chain->Add(0x0003, PassToGame<0x0003>);
	chain->Add(0x0004, PassToGame<0x0004>);
	chain->Add(0x0005, PassToGame<0x0005>);
	chain->Add(0x0006, PassToGame<0x0006>);
	chain->Add(0x0007, PassToGame<0x0007>);
	chain->Add(0x0008, PassToGame<0x0008>);
	chain->Add(0x0009, PassToGame<0x0009>);
	chain->Add(0x000A, PassToGame<0x000A>);
	chain->Add(0x000B, PassToGame<0x000B>);
	chain->Add(0x000c, PassToGame<0x000c>);
	chain->Add(0x000d, PassToGame<0x000d>);
	chain->Add(0x000E, PassToGame<0x000E>);
	chain->Add(0x000F, PassToGame<0x000F>);
	chain->Add(0x0010, PassToGame<0x0010>);
	chain->Add(0x0011, PassToGame<0x0011>);
	chain->Add(0x0012, PassToGame<0x0012>);
	chain->Add(0x0013, PassToGame<0x0013>);
	chain->Add(0x0014, PassToGame<0x0014>);
	chain->Add(0x0015, PassToGame<0x0015>);
	chain->Add(0x0016, PassToGame<0x0016>);
	chain->Add(0x0017, PassToGame<0x0017>);
	chain->Add(0x0018, PassToGame<0x0018>);
	chain->Add(0x0019, PassToGame<0x0019>);
	chain->Add(0x001a, PassToGame<0x001a>);
	chain->Add(0x001b, PassToGame<0x001b>);
	chain->Add(0x001c, PassToGame<0x001c>);
	chain->Add(0x001d, PassToGame<0x001d>);
	chain->Add(0x001E, PassToGame<0x001E>);
	chain->Add(0x001f, PassToGame<0x001f>);
	chain->Add(0x0020, PassToGame<0x0020>);
	chain->Add(0x0021, PassToGame<0x0021>);
	chain->Add(0x0022, PassToGame<0x0022>);
	chain->Add(0x0023, PassToGame<0x0023>);
	chain->Add(0x0024, PassToGame<0x0024>);
	chain->Add(0x0025, PassToGame<0x0025>);
	chain->Add(0x0026, PassToGame<0x0026>);
	chain->Add(0x0027, PassToGame<0x0027>);
	chain->Add(0x0028, PassToGame<0x0028>);
	chain->Add(0x0029, PassToGame<0x0029>);
	chain->Add(0x002a, PassToGame<0x002a>);
	chain->Add(0x002b, PassToGame<0x002b>);
	chain->Add(0x002c, PassToGame<0x002c>);
	chain->Add(0x002d, PassToGame<0x002d>);
	chain->Add(0x002e, PassToGame<0x002e>);
	chain->Add(0x002F, PassToGame<0x002F>);
	chain->Add(0x0030, PassToGame<0x0030>);
	chain->Add(0x0031, PassToGame<0x0031>);
	chain->Add(0x0032, PassToGame<0x0032>);
	chain->Add(0x0033, PassToGame<0x0033>);
	chain->Add(0x0034, PassToGame<0x0034>);
	chain->Add(0x0035, PassToGame<0x0035>);
	chain->Add(0x0036, PassToGame<0x0036>);
	chain->Add(0x0037, PassToGame<0x0037>);
	chain->Add(0x0038, PassToGame<0x0038>);
	chain->Add(0x0039, PassToGame<0x0039>);
	chain->Add(0x003a, PassToGame<0x003a>);
	chain->Add(0x003b, PassToGame<0x003b>);
	chain->Add(0x003c, PassToGame<0x003c>);
	chain->Add(0x0042, PassToGame<0x0042>);
	chain->Add(0x0043, PassToGame<0x0043>);
	chain->Add(0x0044, PassToGame<0x0044>);
	chain->Add(0x0045, PassToGame<0x0045>);
	chain->Add(0x0046, PassToGame<0x0046>);
	chain->Add(0x004D, PassToGame<0x004D>);
	//chain->Add(0x004E, PassToGame<0x004E>);
	//chain->Add(0x004f, PassToGame<0x004f>);
	//chain->Add(0x0050, PassToGame<0x0050>);
	//chain->Add(0x0051, PassToGame<0x0051>);
	chain->Add(0x0052, PassToGame<0x0052>);
	//chain->Add(0x0053, PassToGame<0x0053>);
	chain->Add(0x0058, PassToGame<0x0058>);
	chain->Add(0x0059, PassToGame<0x0059>);
	chain->Add(0x005a, PassToGame<0x005a>);
	chain->Add(0x005b, PassToGame<0x005b>);
	chain->Add(0x005c, PassToGame<0x005c>);
	chain->Add(0x005d, PassToGame<0x005d>);
	chain->Add(0x005e, PassToGame<0x005e>);
	chain->Add(0x005f, PassToGame<0x005f>);
	chain->Add(0x0060, PassToGame<0x0060>);
	chain->Add(0x0061, PassToGame<0x0061>);
	chain->Add(0x0062, PassToGame<0x0062>);
	chain->Add(0x0063, PassToGame<0x0063>);
	chain->Add(0x0064, PassToGame<0x0064>);
	chain->Add(0x0065, PassToGame<0x0065>);
	chain->Add(0x0066, PassToGame<0x0066>);
	chain->Add(0x0067, PassToGame<0x0067>);
	chain->Add(0x0068, PassToGame<0x0068>);
	chain->Add(0x0069, PassToGame<0x0069>);
	chain->Add(0x006a, PassToGame<0x006a>);
	chain->Add(0x006b, PassToGame<0x006b>);
	chain->Add(0x006c, PassToGame<0x006c>);
	chain->Add(0x006d, PassToGame<0x006d>);
	chain->Add(0x006e, PassToGame<0x006e>);
	chain->Add(0x006f, PassToGame<0x006f>);
	chain->Add(0x0070, PassToGame<0x0070>);
	chain->Add(0x0071, PassToGame<0x0071>);
	chain->Add(0x0072, PassToGame<0x0072>);
	chain->Add(0x0073, PassToGame<0x0073>);
	chain->Add(0x0074, PassToGame<0x0074>);
	chain->Add(0x0075, PassToGame<0x0075>);
	chain->Add(0x0076, PassToGame<0x0076>);
	chain->Add(0x0077, PassToGame<0x0077>);
	chain->Add(0x0078, PassToGame<0x0078>);
	chain->Add(0x0079, PassToGame<0x0079>);
	chain->Add(0x007A, PassToGame<0x007A>);
	chain->Add(0x007B, PassToGame<0x007B>);
	chain->Add(0x007C, PassToGame<0x007C>);
	chain->Add(0x007D, PassToGame<0x007D>);
	chain->Add(0x007E, PassToGame<0x007E>);
	chain->Add(0x007F, PassToGame<0x007F>);
	chain->Add(0x0080, PassToGame<0x0080>);
	chain->Add(0x0081, PassToGame<0x0081>);
	chain->Add(0x0082, PassToGame<0x0082>);
	chain->Add(0x0083, PassToGame<0x0083>);
	chain->Add(0x0084, PassToGame<0x0084>);
	chain->Add(0x0085, PassToGame<0x0085>);
	chain->Add(0x0086, PassToGame<0x0086>);
	chain->Add(0x0087, PassToGame<0x0087>);
	chain->Add(0x0088, PassToGame<0x0088>);
	chain->Add(0x0089, PassToGame<0x0089>);
	chain->Add(0x008A, PassToGame<0x008A>);
	chain->Add(0x008B, PassToGame<0x008B>);
	chain->Add(0x008c, PassToGame<0x008c>);
	chain->Add(0x008d, PassToGame<0x008d>);
	chain->Add(0x008E, PassToGame<0x008E>);
	chain->Add(0x008f, PassToGame<0x008f>);
	chain->Add(0x0090, PassToGame<0x0090>);
	chain->Add(0x0091, PassToGame<0x0091>);
	chain->Add(0x0092, PassToGame<0x0092>);
	chain->Add(0x0093, PassToGame<0x0093>);
	chain->Add(0x0094, PassToGame<0x0094>);
	chain->Add(0x0095, PassToGame<0x0095>);
	chain->Add(0x0096, PassToGame<0x0096>);
	chain->Add(0x0097, PassToGame<0x0097>);
	chain->Add(0x0098, PassToGame<0x0098>);
	chain->Add(0x0099, PassToGame<0x0099>);
	chain->Add(0x009A, PassToGame<0x009A>);
	chain->Add(0x009B, PassToGame<0x009B>);
	chain->Add(0x00A0, PassToGame<0x00A0>);
	chain->Add(0x00a1, PassToGame<0x00a1>);
	chain->Add(0x00A3, PassToGame<0x00A3>);
	chain->Add(0x00A4, PassToGame<0x00A4>);
	chain->Add(0x00A5, PassToGame<0x00A5>);
	chain->Add(0x00A6, PassToGame<0x00A6>);
	chain->Add(0x00A7, PassToGame<0x00A7>);
	chain->Add(0x00A8, PassToGame<0x00A8>);
	chain->Add(0x00a9, PassToGame<0x00a9>);
	chain->Add(0x00AA, PassToGame<0x00AA>);
	chain->Add(0x00AB, PassToGame<0x00AB>);
	chain->Add(0x00ad, PassToGame<0x00ad>);
	chain->Add(0x00AE, PassToGame<0x00AE>);
	chain->Add(0x00AF, PassToGame<0x00AF>);
	chain->Add(0x00B0, PassToGame<0x00B0>);
	chain->Add(0x00B1, PassToGame<0x00B1>);
	chain->Add(0x00BA, PassToGame<0x00BA>);
	chain->Add(0x00BB, PassToGame<0x00BB>);
	chain->Add(0x00BC, PassToGame<0x00BC>);
	chain->Add(0x00be, PassToGame<0x00be>);
	chain->Add(0x00BF, PassToGame<0x00BF>);
	chain->Add(0x00c0, PassToGame<0x00c0>);
	chain->Add(0x00C1, PassToGame<0x00C1>);
	chain->Add(0x00c2, PassToGame<0x00c2>);
	//chain->Add(0x00D6, PassToGame<0x00D6>);
	//chain->Add(0x00D7, PassToGame<0x00D7>);
	chain->Add(0x00d8, PassToGame<0x00d8>);
	chain->Add(0x00D9, PassToGame<0x00D9>);
	chain->Add(0x00DB, PassToGame<0x00DB>);
	chain->Add(0x00DD, PassToGame<0x00DD>);
	chain->Add(0x00DF, PassToGame<0x00DF>);
	chain->Add(0x00E1, PassToGame<0x00E1>);
	chain->Add(0x00E2, PassToGame<0x00E2>);
	chain->Add(0x00ec, PassToGame<0x00ec>);
	chain->Add(0x00ed, PassToGame<0x00ed>);
	chain->Add(0x00EE, PassToGame<0x00EE>);
	chain->Add(0x00ef, PassToGame<0x00ef>);
	chain->Add(0x00f0, PassToGame<0x00f0>);
	chain->Add(0x00f1, PassToGame<0x00f1>);
	chain->Add(0x00F2, PassToGame<0x00F2>);
	chain->Add(0x00F3, PassToGame<0x00F3>);
	chain->Add(0x00F4, PassToGame<0x00F4>);
	chain->Add(0x00fe, PassToGame<0x00fe>);
	chain->Add(0x00ff, PassToGame<0x00ff>);
	chain->Add(0x0100, PassToGame<0x0100>);
	chain->Add(0x0101, PassToGame<0x0101>);
	chain->Add(0x0102, PassToGame<0x0102>);
	chain->Add(0x0103, PassToGame<0x0103>);
	chain->Add(0x0104, PassToGame<0x0104>);
	chain->Add(0x0105, PassToGame<0x0105>);
	chain->Add(0x0106, PassToGame<0x0106>);
	chain->Add(0x0107, PassToGame<0x0107>);
	chain->Add(0x0108, PassToGame<0x0108>);
	chain->Add(0x0109, PassToGame<0x0109>);
	chain->Add(0x010a, PassToGame<0x010a>);
	chain->Add(0x010B, PassToGame<0x010B>);
	chain->Add(0x010d, PassToGame<0x010d>);
	chain->Add(0x010e, PassToGame<0x010e>);
	chain->Add(0x010f, PassToGame<0x010f>);
	chain->Add(0x0110, PassToGame<0x0110>);
	chain->Add(0x0111, PassToGame<0x0111>);
	chain->Add(0x0112, PassToGame<0x0112>);
	chain->Add(0x0114, PassToGame<0x0114>);
	chain->Add(0x0117, PassToGame<0x0117>);
	chain->Add(0x0118, PassToGame<0x0118>);
	chain->Add(0x0119, PassToGame<0x0119>);
	chain->Add(0x0122, PassToGame<0x0122>);
	chain->Add(0x0129, PassToGame<0x0129>);
	chain->Add(0x0137, PassToGame<0x0137>);
	chain->Add(0x014B, PassToGame<0x014B>);
	chain->Add(0x014C, PassToGame<0x014C>);
	chain->Add(0x014E, PassToGame<0x014E>);
	chain->Add(0x014f, PassToGame<0x014f>);
	chain->Add(0x0151, PassToGame<0x0151>);
	chain->Add(0x0154, PassToGame<0x0154>);
	chain->Add(0x0158, PassToGame<0x0158>);
	chain->Add(0x0159, PassToGame<0x0159>);
	chain->Add(0x015a, PassToGame<0x015a>);
	chain->Add(0x015D, PassToGame<0x015D>);
	chain->Add(0x015f, PassToGame<0x015f>);
	chain->Add(0x0160, PassToGame<0x0160>);
	chain->Add(0x0161, PassToGame<0x0161>);
	chain->Add(0x0164, PassToGame<0x0164>);
	chain->Add(0x0165, PassToGame<0x0165>);
	chain->Add(0x0167, PassToGame<0x0167>);
	chain->Add(0x0168, PassToGame<0x0168>);
	chain->Add(0x0169, PassToGame<0x0169>);
	chain->Add(0x016a, PassToGame<0x016a>);
	chain->Add(0x016b, PassToGame<0x016b>);
	chain->Add(0x016c, PassToGame<0x016c>); // 4 != 5
	chain->Add(0x016d, PassToGame<0x016d>); // 4 != 5
	chain->Add(0x016E, PassToGame<0x016E>);
	chain->Add(0x016f, PassToGame<0x016f>);
	chain->Add(0x0172, PassToGame<0x0172>);
	chain->Add(0x0173, PassToGame<0x0173>);
	chain->Add(0x0174, PassToGame<0x0174>);
	chain->Add(0x0175, PassToGame<0x0175>);
	chain->Add(0x0176, PassToGame<0x0176>);
	chain->Add(0x0177, PassToGame<0x0177>);
	chain->Add(0x0179, PassToGame<0x0179>);
	chain->Add(0x017b, PassToGame<0x017b>);
	chain->Add(0x0180, PassToGame<0x0180>);
	chain->Add(0x0184, PassToGame<0x0184>);
	chain->Add(0x0185, PassToGame<0x0185>);
	chain->Add(0x0186, PassToGame<0x0186>);
	chain->Add(0x0187, PassToGame<0x0187>);
	chain->Add(0x0188, PassToGame<0x0188>);
	chain->Add(0x018a, PassToGame<0x018a>);
	chain->Add(0x018b, PassToGame<0x018b>);
	chain->Add(0x018c, PassToGame<0x018c>);
	chain->Add(0x018e, PassToGame<0x018e>);
	chain->Add(0x018F, PassToGame<0x018F>);
	chain->Add(0x0190, PassToGame<0x0190>);
	chain->Add(0x0191, PassToGame<0x0191>);
	chain->Add(0x01a1, PassToGame<0x01a1>);
	chain->Add(0x01a2, PassToGame<0x01a2>);
	chain->Add(0x01a3, PassToGame<0x01a3>);
	chain->Add(0x01A4, PassToGame<0x01A4>);
	chain->Add(0x01a5, PassToGame<0x01a5>);
	chain->Add(0x01a6, PassToGame<0x01a6>);
	chain->Add(0x01a7, PassToGame<0x01a7>);
	chain->Add(0x01a8, PassToGame<0x01a8>);
	chain->Add(0x01a9, PassToGame<0x01a9>);
	chain->Add(0x01aa, PassToGame<0x01aa>);
	chain->Add(0x01ab, PassToGame<0x01ab>);
	chain->Add(0x01ac, PassToGame<0x01ac>);
	chain->Add(0x01ad, PassToGame<0x01ad>);
	chain->Add(0x01ae, PassToGame<0x01ae>);
	chain->Add(0x01af, PassToGame<0x01af>);
	chain->Add(0x01b0, PassToGame<0x01b0>);
	chain->Add(0x01b2, PassToGame<0x01b2>);
	chain->Add(0x01b4, PassToGame<0x01b4>);
	chain->Add(0x01b5, PassToGame<0x01b5>);
	chain->Add(0x01b6, PassToGame<0x01b6>);
	chain->Add(0x01b7, PassToGame<0x01b7>);
	chain->Add(0x01b9, PassToGame<0x01b9>);
	chain->Add(0x01bb, PassToGame<0x01bb>);
	chain->Add(0x01bc, PassToGame<0x01bc>);
	chain->Add(0x01bd, PassToGame<0x01bd>);
	chain->Add(0x01c0, PassToGame<0x01c0>);
	chain->Add(0x01c1, PassToGame<0x01c1>);
	chain->Add(0x01C2, PassToGame<0x01C2>);
	chain->Add(0x01C3, PassToGame<0x01C3>);
	chain->Add(0x01c4, PassToGame<0x01c4>);
	chain->Add(0x01C5, PassToGame<0x01C5>);
	chain->Add(0x01c7, PassToGame<0x01c7>);
	chain->Add(0x01c8, PassToGame<0x01c8>);
	chain->Add(0x01E3, PassToGame<0x01E3>);
	chain->Add(0x01e4, PassToGame<0x01e4>);
	chain->Add(0x01e5, PassToGame<0x01e5>);
	chain->Add(0x01e7, PassToGame<0x01e7>);
	chain->Add(0x01E8, PassToGame<0x01E8>);
	chain->Add(0x01E9, PassToGame<0x01E9>);
	chain->Add(0x01ea, PassToGame<0x01ea>);
	chain->Add(0x01EB, PassToGame<0x01EB>);
	chain->Add(0x01EC, PassToGame<0x01EC>);
	chain->Add(0x01f0, PassToGame<0x01f0>);
	chain->Add(0x01f3, PassToGame<0x01f3>);
	chain->Add(0x01f4, PassToGame<0x01f4>);
	//chain->Add(0x01F5, PassToGame<0x01F5>);
	chain->Add(0x01f6, PassToGame<0x01f6>);
	chain->Add(0x01f7, PassToGame<0x01f7>);
	chain->Add(0x01f9, PassToGame<0x01f9>);
	chain->Add(0x01fa, PassToGame<0x01fa>);
	chain->Add(0x01fb, PassToGame<0x01fb>);
	chain->Add(0x0202, PassToGame<0x0202>);
	chain->Add(0x0203, PassToGame<0x0203>);
	chain->Add(0x0204, PassToGame<0x0204>);
	chain->Add(0x0205, PassToGame<0x0205>);
	chain->Add(0x0206, PassToGame<0x0206>);
	chain->Add(0x0207, PassToGame<0x0207>);
	chain->Add(0x0208, PassToGame<0x0208>);
	chain->Add(0x0209, PassToGame<0x0209>);
	chain->Add(0x020a, PassToGame<0x020a>);
	chain->Add(0x020b, PassToGame<0x020b>);
	chain->Add(0x020c, PassToGame<0x020c>);
	chain->Add(0x020d, PassToGame<0x020d>);
	chain->Add(0x0213, PassToGame<0x0213>);
	chain->Add(0x0214, PassToGame<0x0214>);
	chain->Add(0x0215, PassToGame<0x0215>);
	chain->Add(0x0216, PassToGame<0x0216>);
	chain->Add(0x0217, PassToGame<0x0217>);
	chain->Add(0x021b, PassToGame<0x021b>);
	chain->Add(0x0221, PassToGame<0x0221>);
	chain->Add(0x0223, PassToGame<0x0223>);
	chain->Add(0x0224, PassToGame<0x0224>);
	chain->Add(0x0226, PassToGame<0x0226>);
	chain->Add(0x0227, PassToGame<0x0227>);
	chain->Add(0x0229, PassToGame<0x0229>);
	chain->Add(0x022a, PassToGame<0x022a>);
	chain->Add(0x022b, PassToGame<0x022b>);
	chain->Add(0x0235, PassToGame<0x0235>);
	chain->Add(0x0236, PassToGame<0x0236>);
	chain->Add(0x0237, PassToGame<0x0237>); // 3 != 4
	chain->Add(0x023b, PassToGame<0x023b>);
	//chain->Add(0x023c, PassToGame<0x023c>);
	//chain->Add(0x023d, PassToGame<0x023d>);
	chain->Add(0x0241, PassToGame<0x0241>);
	chain->Add(0x0244, PassToGame<0x0244>);
	chain->Add(0x0245, PassToGame<0x0245>);
	chain->Add(0x0247, PassToGame<0x0247>);
	chain->Add(0x0248, PassToGame<0x0248>);
	chain->Add(0x0249, PassToGame<0x0249>);
	chain->Add(0x024f, PassToGame<0x024f>);
	chain->Add(0x0253, PassToGame<0x0253>);
	chain->Add(0x0254, PassToGame<0x0254>);
	chain->Add(0x0256, PassToGame<0x0256>);
	chain->Add(0x0293, PassToGame<0x0293>);
	chain->Add(0x0294, PassToGame<0x0294>);
	//chain->Add(0x0296, PassToGame<0x0296>);
	chain->Add(0x0297, PassToGame<0x0297>); // 0 != 1
	chain->Add(0x0298, PassToGame<0x0298>); // 2 != 3
	chain->Add(0x0299, PassToGame<0x0299>);
	chain->Add(0x029b, PassToGame<0x029b>);
	chain->Add(0x02a0, PassToGame<0x02a0>);
	chain->Add(0x02a3, PassToGame<0x02a3>);
	chain->Add(0x02a7, PassToGame<0x02a7>);
	chain->Add(0x02a8, PassToGame<0x02a8>);
	chain->Add(0x02A9, PassToGame<0x02A9>);
	chain->Add(0x02aa, PassToGame<0x02aa>);
	chain->Add(0x02ab, PassToGame<0x02ab>);
	chain->Add(0x02ac, PassToGame<0x02ac>);
	chain->Add(0x02b9, PassToGame<0x02b9>);
	chain->Add(0x02bf, PassToGame<0x02bf>);
	chain->Add(0x02c0, PassToGame<0x02c0>);
	chain->Add(0x02c1, PassToGame<0x02c1>);
	chain->Add(0x02c2, PassToGame<0x02c2>);
	chain->Add(0x02ca, PassToGame<0x02ca>);
	chain->Add(0x02cb, PassToGame<0x02cb>);
	chain->Add(0x02CC, PassToGame<0x02CC>);
	chain->Add(0x02ce, PassToGame<0x02ce>);
	chain->Add(0x02cf, PassToGame<0x02cf>); // 4 != 6
	chain->Add(0x02d0, PassToGame<0x02d0>);
	chain->Add(0x02D1, PassToGame<0x02D1>);
	chain->Add(0x02d3, PassToGame<0x02d3>);
	chain->Add(0x02d4, PassToGame<0x02d4>);
	chain->Add(0x02d8, PassToGame<0x02d8>);
	chain->Add(0x02DB, PassToGame<0x02DB>);
	chain->Add(0x02DD, PassToGame<0x02DD>);
	chain->Add(0x02e0, PassToGame<0x02e0>);
	chain->Add(0x02E1, PassToGame<0x02E1>); // 5 != 6
	chain->Add(0x02e2, PassToGame<0x02e2>);
	chain->Add(0x02e3, PassToGame<0x02e3>);
	// CUTSCENES DISABLED
	//chain->Add(0x02e4, PassToGame<0x02e4>);
	chain->Add(0x02E5, PassToGame<0x02E5>); // 2 != 0
	chain->Add(0x02E6, PassToGame<0x02E6>); // 2 != 0
	//chain->Add(0x02e7, PassToGame<0x02e7>);
	//chain->Add(0x02e8, PassToGame<0x02e8>);
	chain->Add(0x02e9, PassToGame<0x02e9>);
	//chain->Add(0x02ea, PassToGame<0x02ea>);
	chain->Add(0x02EB, PassToGame<0x02EB>);
	chain->Add(0x02ed, PassToGame<0x02ed>);
	chain->Add(0x02ee, PassToGame<0x02ee>);
	chain->Add(0x02F2, PassToGame<0x02F2>);
	chain->Add(0x02F6, PassToGame<0x02F6>);
	chain->Add(0x02F7, PassToGame<0x02F7>);
	chain->Add(0x02F8, PassToGame<0x02F8>);
	chain->Add(0x02F9, PassToGame<0x02F9>);
	chain->Add(0x02fa, PassToGame<0x02fa>);
	chain->Add(0x02fd, PassToGame<0x02fd>);
	chain->Add(0x02FF, PassToGame<0x02FF>);
	chain->Add(0x0302, PassToGame<0x0302>);
	chain->Add(0x0303, PassToGame<0x0303>);
	chain->Add(0x0308, PassToGame<0x0308>);
	chain->Add(0x030c, PassToGame<0x030c>);
	chain->Add(0x030d, PassToGame<0x030d>);
	chain->Add(0x0317, PassToGame<0x0317>);
	chain->Add(0x0318, PassToGame<0x0318>);
	chain->Add(0x031a, PassToGame<0x031a>);
	chain->Add(0x031d, PassToGame<0x031d>);
	chain->Add(0x031E, PassToGame<0x031E>);
	chain->Add(0x0321, PassToGame<0x0321>);
	chain->Add(0x0323, PassToGame<0x0323>);
	chain->Add(0x0325, PassToGame<0x0325>);
	chain->Add(0x0326, PassToGame<0x0326>);
	chain->Add(0x0327, PassToGame<0x0327>);
	chain->Add(0x032A, PassToGame<0x032A>);
	chain->Add(0x032b, PassToGame<0x032b>);
	chain->Add(0x0330, PassToGame<0x0330>);
	chain->Add(0x0331, PassToGame<0x0331>);
	chain->Add(0x0332, PassToGame<0x0332>);
	chain->Add(0x0335, PassToGame<0x0335>);
	chain->Add(0x0337, PassToGame<0x0337>);
	chain->Add(0x0339, PassToGame<0x0339>);
	chain->Add(0x033e, PassToGame<0x033e>);
	chain->Add(0x033f, PassToGame<0x033f>);
	chain->Add(0x0340, PassToGame<0x0340>);
	chain->Add(0x0341, PassToGame<0x0341>);
	chain->Add(0x0342, PassToGame<0x0342>);
	chain->Add(0x0343, PassToGame<0x0343>);
	chain->Add(0x0344, PassToGame<0x0344>);
	chain->Add(0x0345, PassToGame<0x0345>);
	chain->Add(0x0348, PassToGame<0x0348>);
	chain->Add(0x0349, PassToGame<0x0349>);
	chain->Add(0x034d, PassToGame<0x034d>);
	chain->Add(0x034e, PassToGame<0x034e>);
	chain->Add(0x034f, PassToGame<0x034f>);
	chain->Add(0x0350, PassToGame<0x0350>);
	chain->Add(0x0356, PassToGame<0x0356>);
	chain->Add(0x035c, PassToGame<0x035c>);
	chain->Add(0x035d, PassToGame<0x035d>); // 1 != 2
	chain->Add(0x035f, PassToGame<0x035f>);
	chain->Add(0x0360, PassToGame<0x0360>);
	chain->Add(0x0361, PassToGame<0x0361>);
	chain->Add(0x0362, PassToGame<0x0362>);
	chain->Add(0x0363, PassToGame<0x0363>);
	chain->Add(0x0366, PassToGame<0x0366>);
	chain->Add(0x036a, PassToGame<0x036a>);
	chain->Add(0x036D, PassToGame<0x036D>);
	chain->Add(0x0373, PassToGame<0x0373>);
	chain->Add(0x0376, PassToGame<0x0376>);
	chain->Add(0x0381, PassToGame<0x0381>);
	chain->Add(0x0382, PassToGame<0x0382>);
	chain->Add(0x0384, PassToGame<0x0384>);
	chain->Add(0x038a, PassToGame<0x038a>);
	chain->Add(0x038B, PassToGame<0x038B>);
	chain->Add(0x038c, PassToGame<0x038c>);
	chain->Add(0x038D, PassToGame<0x038D>);
	chain->Add(0x038E, PassToGame<0x038E>);
	chain->Add(0x038F, PassToGame<0x038F>);
	chain->Add(0x0390, PassToGame<0x0390>);
	chain->Add(0x0391, PassToGame<0x0391>);
	chain->Add(0x0392, PassToGame<0x0392>);
	chain->Add(0x0394, PassToGame<0x0394>);
	chain->Add(0x0395, PassToGame<0x0395>);
	chain->Add(0x0396, PassToGame<0x0396>);
	chain->Add(0x0397, PassToGame<0x0397>);
	chain->Add(0x039C, PassToGame<0x039C>);
	chain->Add(0x039E, PassToGame<0x039E>);
	chain->Add(0x039f, PassToGame<0x039f>);
	chain->Add(0x03A1, PassToGame<0x03A1>);
	chain->Add(0x03A2, PassToGame<0x03A2>);
	chain->Add(0x03A3, PassToGame<0x03A3>);
	chain->Add(0x03A4, PassToGame<0x03A4>);
	chain->Add(0x03AB, PassToGame<0x03AB>);
	chain->Add(0x03af, PassToGame<0x03af>);
	chain->Add(0x03b0, PassToGame<0x03b0>);
	chain->Add(0x03b1, PassToGame<0x03b1>);
	chain->Add(0x03b6, PassToGame<0x03b6>);
	chain->Add(0x03b7, PassToGame<0x03b7>);
	chain->Add(0x03ba, PassToGame<0x03ba>);
	chain->Add(0x03bc, PassToGame<0x03bc>);
	chain->Add(0x03bd, PassToGame<0x03bd>);
	chain->Add(0x03bf, PassToGame<0x03bf>);
	chain->Add(0x03C0, PassToGame<0x03C0>);
	chain->Add(0x03C3, PassToGame<0x03C3>);
	chain->Add(0x03c4, PassToGame<0x03c4>);
	chain->Add(0x03c5, PassToGame<0x03c5>);
	chain->Add(0x03C7, PassToGame<0x03C7>);
	chain->Add(0x03C8, PassToGame<0x03C8>);
	chain->Add(0x03c9, PassToGame<0x03c9>);
	chain->Add(0x03ca, PassToGame<0x03ca>);
	chain->Add(0x03CB, PassToGame<0x03CB>);
	chain->Add(0x03cc, PassToGame<0x03cc>);
	chain->Add(0x03cd, PassToGame<0x03cd>);
	chain->Add(0x03ce, PassToGame<0x03ce>);
	chain->Add(0x03CF, PassToGame<0x03CF>);
	chain->Add(0x03D0, PassToGame<0x03D0>);
	chain->Add(0x03D1, PassToGame<0x03D1>);
	chain->Add(0x03D2, PassToGame<0x03D2>);
	chain->Add(0x03d3, PassToGame<0x03d3>);
	chain->Add(0x03D5, PassToGame<0x03D5>);
	chain->Add(0x03d6, PassToGame<0x03d6>);
	chain->Add(0x03D7, PassToGame<0x03D7>);
	chain->Add(0x03d8, PassToGame<0x03d8>);
	chain->Add(0x03d9, PassToGame<0x03d9>);
	chain->Add(0x03dc, PassToGame<0x03dc>);
	chain->Add(0x03DE, PassToGame<0x03DE>);
	chain->Add(0x03E0, PassToGame<0x03E0>);
	chain->Add(0x03E3, PassToGame<0x03E3>);
	chain->Add(0x03E4, PassToGame<0x03E4>);
	chain->Add(0x03E5, PassToGame<0x03E5>);
	chain->Add(0x03e6, PassToGame<0x03e6>);
	chain->Add(0x03E7, PassToGame<0x03E7>);
	chain->Add(0x03eb, PassToGame<0x03eb>);
	chain->Add(0x03ed, PassToGame<0x03ed>);
	chain->Add(0x03ee, PassToGame<0x03ee>);
	chain->Add(0x03ef, PassToGame<0x03ef>);
	chain->Add(0x03f0, PassToGame<0x03f0>);
	chain->Add(0x03f3, PassToGame<0x03f3>);
	chain->Add(0x03F4, PassToGame<0x03F4>);
	chain->Add(0x03F5, PassToGame<0x03F5>);
	chain->Add(0x03FD, PassToGame<0x03FD>);
	chain->Add(0x03FE, PassToGame<0x03FE>);
	chain->Add(0x0400, PassToGame<0x0400>);
	chain->Add(0x0407, PassToGame<0x0407>);
	chain->Add(0x040c, PassToGame<0x040c>);
	chain->Add(0x040D, PassToGame<0x040D>);
	chain->Add(0x0414, PassToGame<0x0414>);
	//chain->Add(0x0417, PassToGame<0x0417>);
	chain->Add(0x0418, PassToGame<0x0418>);
	chain->Add(0x041a, PassToGame<0x041a>);
	chain->Add(0x041d, PassToGame<0x041d>);
	chain->Add(0x041e, PassToGame<0x041e>); // 2 != 1
	chain->Add(0x0423, PassToGame<0x0423>);
	chain->Add(0x0424, PassToGame<0x0424>);
	chain->Add(0x0425, PassToGame<0x0425>);
	chain->Add(0x0428, PassToGame<0x0428>);
	chain->Add(0x042b, PassToGame<0x042b>);
	chain->Add(0x042c, PassToGame<0x042c>);
	chain->Add(0x042d, PassToGame<0x042d>);
	chain->Add(0x042E, PassToGame<0x042E>);
	chain->Add(0x0431, PassToGame<0x0431>);
	chain->Add(0x0433, PassToGame<0x0433>);
	chain->Add(0x0434, PassToGame<0x0434>);
	chain->Add(0x0435, PassToGame<0x0435>);
	chain->Add(0x0436, PassToGame<0x0436>);
	chain->Add(0x043C, PassToGame<0x043C>);
	chain->Add(0x0441, PassToGame<0x0441>);
	chain->Add(0x0445, PassToGame<0x0445>);
	chain->Add(0x0446, PassToGame<0x0446>);
	chain->Add(0x0448, PassToGame<0x0448>);
	chain->Add(0x0449, PassToGame<0x0449>);
	chain->Add(0x044B, PassToGame<0x044B>);
	chain->Add(0x0453, PassToGame<0x0453>);
	chain->Add(0x0454, PassToGame<0x0454>);
	chain->Add(0x0457, PassToGame<0x0457>);
	chain->Add(0x0459, PassToGame<0x0459>);
	chain->Add(0x045A, PassToGame<0x045A>);
	chain->Add(0x045B, PassToGame<0x045B>);
	chain->Add(0x0460, PassToGame<0x0460>);
	chain->Add(0x0463, PassToGame<0x0463>);
	chain->Add(0x0464, PassToGame<0x0464>);
	chain->Add(0x0465, PassToGame<0x0465>);
	chain->Add(0x0466, PassToGame<0x0466>);
	chain->Add(0x0467, PassToGame<0x0467>);
	chain->Add(0x0468, PassToGame<0x0468>);
	chain->Add(0x046C, PassToGame<0x046C>);
	chain->Add(0x046D, PassToGame<0x046D>);
	chain->Add(0x046E, PassToGame<0x046E>);
	chain->Add(0x0470, PassToGame<0x0470>);
	chain->Add(0x0471, PassToGame<0x0471>);
	chain->Add(0x0472, PassToGame<0x0472>);
	chain->Add(0x0477, PassToGame<0x0477>);
	chain->Add(0x047A, PassToGame<0x047A>);
	chain->Add(0x0480, PassToGame<0x0480>);
	chain->Add(0x0484, PassToGame<0x0484>);
	chain->Add(0x0489, PassToGame<0x0489>);
	chain->Add(0x048A, PassToGame<0x048A>);
	chain->Add(0x048B, PassToGame<0x048B>);
	chain->Add(0x048C, PassToGame<0x048C>);
	chain->Add(0x048F, PassToGame<0x048F>);
	chain->Add(0x0494, PassToGame<0x0494>);
	chain->Add(0x0495, PassToGame<0x0495>);
	chain->Add(0x0496, PassToGame<0x0496>);
	chain->Add(0x04A2, PassToGame<0x04A2>); // 5 != 6
	chain->Add(0x04A3, PassToGame<0x04A3>);
	chain->Add(0x04A4, PassToGame<0x04A4>);
	chain->Add(0x04A5, PassToGame<0x04A5>);
	chain->Add(0x04A6, PassToGame<0x04A6>);
	chain->Add(0x04AD, PassToGame<0x04AD>);
	chain->Add(0x04AE, PassToGame<0x04AE>);
	chain->Add(0x04AF, PassToGame<0x04AF>);
	chain->Add(0x04B0, PassToGame<0x04B0>);
	chain->Add(0x04B1, PassToGame<0x04B1>);
	chain->Add(0x04B2, PassToGame<0x04B2>);
	chain->Add(0x04B3, PassToGame<0x04B3>);
	chain->Add(0x04B4, PassToGame<0x04B4>);
	chain->Add(0x04B5, PassToGame<0x04B5>);
	chain->Add(0x04B6, PassToGame<0x04B6>);
	chain->Add(0x04B7, PassToGame<0x04B7>);
	chain->Add(0x04B8, PassToGame<0x04B8>);
	chain->Add(0x04B9, PassToGame<0x04B9>);
	chain->Add(0x04BA, PassToGame<0x04BA>);
	chain->Add(0x04BB, PassToGame<0x04BB>);
	chain->Add(0x04BD, PassToGame<0x04BD>);
	chain->Add(0x04C0, PassToGame<0x04C0>); // 6 != 7
	chain->Add(0x04C1, PassToGame<0x04C1>);
	chain->Add(0x04C4, PassToGame<0x04C4>);
	chain->Add(0x04C5, PassToGame<0x04C5>);
	chain->Add(0x04CE, PassToGame<0x04CE>);
	chain->Add(0x04D0, PassToGame<0x04D0>);
	chain->Add(0x04D1, PassToGame<0x04D1>);
	chain->Add(0x04D2, PassToGame<0x04D2>); // 5 != 6
	chain->Add(0x04D3, PassToGame<0x04D3>);
	chain->Add(0x04D5, PassToGame<0x04D5>);
	chain->Add(0x04D6, PassToGame<0x04D6>);
	chain->Add(0x04D7, PassToGame<0x04D7>);
	chain->Add(0x04D8, PassToGame<0x04D8>);
	chain->Add(0x04D9, PassToGame<0x04D9>);
	chain->Add(0x04DA, PassToGame<0x04DA>);
	chain->Add(0x04DB, PassToGame<0x04DB>);
	chain->Add(0x04DD, PassToGame<0x04DD>);
	chain->Add(0x04DF, PassToGame<0x04DF>);
	chain->Add(0x04E0, PassToGame<0x04E0>);
	chain->Add(0x04E1, PassToGame<0x04E1>);
	chain->Add(0x04E2, PassToGame<0x04E2>);
	chain->Add(0x04E3, PassToGame<0x04E3>);
	chain->Add(0x04E4, PassToGame<0x04E4>);
	chain->Add(0x04E5, PassToGame<0x04E5>);
	chain->Add(0x04E6, PassToGame<0x04E6>);
	chain->Add(0x04E7, PassToGame<0x04E7>);
	chain->Add(0x04E9, PassToGame<0x04E9>);
	chain->Add(0x04EA, PassToGame<0x04EA>);
	chain->Add(0x04EB, PassToGame<0x04EB>); // 3 != 2
	chain->Add(0x04ED, PassToGame<0x04ED>);
	chain->Add(0x04EE, PassToGame<0x04EE>);
	chain->Add(0x04EF, PassToGame<0x04EF>);
	chain->Add(0x04F0, PassToGame<0x04F0>);
	chain->Add(0x04F1, PassToGame<0x04F1>);
	chain->Add(0x04F4, PassToGame<0x04F4>);
	chain->Add(0x04F7, PassToGame<0x04F7>);
	chain->Add(0x04F8, PassToGame<0x04F8>);
	chain->Add(0x04F9, PassToGame<0x04F9>);
	chain->Add(0x04FA, PassToGame<0x04FA>);
	chain->Add(0x04FC, PassToGame<0x04FC>);
	chain->Add(0x04FE, PassToGame<0x04FE>);
	chain->Add(0x0500, PassToGame<0x0500>); // 2 != 3
	chain->Add(0x0501, PassToGame<0x0501>);
	chain->Add(0x0503, PassToGame<0x0503>); // 3 != 6
	chain->Add(0x0506, PassToGame<0x0506>);
	chain->Add(0x0508, PassToGame<0x0508>);
	chain->Add(0x0509, PassToGame<0x0509>);
	chain->Add(0x050A, PassToGame<0x050A>);
	chain->Add(0x050E, PassToGame<0x050E>);
	chain->Add(0x0512, PassToGame<0x0512>);
	chain->Add(0x0517, PassToGame<0x0517>);
	chain->Add(0x0518, PassToGame<0x0518>);
	chain->Add(0x0519, PassToGame<0x0519>);
	chain->Add(0x051A, PassToGame<0x051A>);
	chain->Add(0x0526, PassToGame<0x0526>);
	chain->Add(0x052C, PassToGame<0x052C>);
	chain->Add(0x053E, PassToGame<0x053E>);
	chain->Add(0x053F, PassToGame<0x053F>);
	chain->Add(0x0541, PassToGame<0x0541>);
	chain->Add(0x054A, PassToGame<0x054A>);
	chain->Add(0x054C, PassToGame<0x054C>);
	chain->Add(0x054E, PassToGame<0x054E>);
	chain->Add(0x0550, PassToGame<0x0550>);
	chain->Add(0x055D, PassToGame<0x055D>);
	chain->Add(0x055E, PassToGame<0x055E>);
	chain->Add(0x055F, PassToGame<0x055F>);
	chain->Add(0x0560, PassToGame<0x0560>);
	chain->Add(0x0561, PassToGame<0x0561>);
	chain->Add(0x0563, PassToGame<0x0563>);
	chain->Add(0x0564, PassToGame<0x0564>);
	chain->Add(0x0565, PassToGame<0x0565>);
	chain->Add(0x0566, PassToGame<0x0566>);
	chain->Add(0x0568, PassToGame<0x0568>);
	chain->Add(0x056A, PassToGame<0x056A>);
	chain->Add(0x056C, PassToGame<0x056C>);
	chain->Add(0x056D, PassToGame<0x056D>);
	chain->Add(0x0570, PassToGame<0x0570>);
	chain->Add(0x0572, PassToGame<0x0572>);
	chain->Add(0x0574, PassToGame<0x0574>);
	chain->Add(0x0575, PassToGame<0x0575>);
	chain->Add(0x057E, PassToGame<0x057E>);
	chain->Add(0x0581, PassToGame<0x0581>);
	chain->Add(0x0582, PassToGame<0x0582>);
	chain->Add(0x0583, PassToGame<0x0583>);
	chain->Add(0x0587, PassToGame<0x0587>);
	chain->Add(0x0588, PassToGame<0x0588>);
	chain->Add(0x058A, PassToGame<0x058A>);
	chain->Add(0x058C, PassToGame<0x058C>);
	chain->Add(0x0594, PassToGame<0x0594>);
	chain->Add(0x0595, PassToGame<0x0595>);
	chain->Add(0x059A, PassToGame<0x059A>);
	chain->Add(0x059F, PassToGame<0x059F>);
	chain->Add(0x05A1, PassToGame<0x05A1>);
	chain->Add(0x05A2, PassToGame<0x05A2>);
	chain->Add(0x05A3, PassToGame<0x05A3>);
	chain->Add(0x05A4, PassToGame<0x05A4>);
	chain->Add(0x05A6, PassToGame<0x05A6>);
	chain->Add(0x05A7, PassToGame<0x05A7>);
	chain->Add(0x05A8, PassToGame<0x05A8>);
	chain->Add(0x05B0, PassToGame<0x05B0>);
#pragma endregion

	return chain;
}

void ScriptChain::Add(uint32_t command, ChainTaskCB task)
{
	ChainTask taskData;
	taskData.cb = task;
	taskData.command = command;

	m_tasks[command].push_back(taskData);
}

void ScriptChain::ProcessTasks(uint16_t command, HostilityScript* script)
{
	for (auto& task : m_tasks[command])
	{
		if (task.command == command)
		{
			task.cb(script);
		}
	}
}

VCScriptChainFactory g_vcFactory;

ScriptChainFactory* GetScriptChainFactory()
{
	return &g_vcFactory;
}