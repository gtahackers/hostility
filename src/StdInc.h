#pragma once

// platform primary include
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// MSVC odd defines
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE

// C/C++ headers
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

//#define _DEBUG
//#undef NDEBUG
//#include <assert.h>

#include <map>
#include <unordered_map>
#include <vector>
#include <string>

// our common includes
#include "MemoryMgr.h"

// stuff
struct CVector
{
	float x, y, z;
};

class CSimpleTransform   // 16 bytes
{
public:
	CVector                         m_translate;
	FLOAT                           m_heading;
};

class CPlaceable // 20 bytes
{
public:
	CSimpleTransform     m_transform;
	void                  * matrix; // This is actually XYZ*, change later
};

class CRect
{
public:
	float x1, y1;
	float x2, y2;

	inline CRect() {}
	inline CRect(float a, float b, float c, float d)
		: x1(a), y1(b), x2(c), y2(d)
	{
	}
};

struct CEntity
{
	virtual ~CEntity();
	virtual void	Add_CRect(CRect& rect);
	virtual void	Add();
	virtual void	Remove();
	virtual void	SetIsStatic(bool);
	virtual void	SetModelIndex(int nModelIndex);
	virtual void	SetModelIndexNoCreate(int nModelIndex);
	virtual void	CreateRwObject();
	virtual void	DeleteRwObject();
	virtual CRect	GetBoundRect();
	virtual void	ProcessControl();
	virtual void	ProcessCollision();
	virtual void	ProcessShift();
	virtual void	TestCollision();
	virtual void	Teleport();
	virtual void	SpecialEntityPreCollisionStuff();
	virtual void	SpecialEntityCalcCollisionSteps();
	virtual void	PreRender();
	virtual void	Render();
	virtual void	SetupLighting();
	virtual void	RemoveLighting();
	virtual void	FlagToDestroyWhenNextProcessed();

	CPlaceable   Placeable; // 4

	void     * m_pRwObject; // 24
	/********** BEGIN CFLAGS **************/
	unsigned long bUsesCollision : 1;           // does entity use collision
	unsigned long bCollisionProcessed : 1;  // has object been processed by a ProcessEntityCollision function
	unsigned long bIsStatic : 1;                // is entity static
	unsigned long bHasContacted : 1;            // has entity processed some contact forces
	unsigned long bIsStuck : 1;             // is entity stuck
	unsigned long bIsInSafePosition : 1;        // is entity in a collision free safe position
	unsigned long bWasPostponed : 1;            // was entity control processing postponed
	unsigned long bIsVisible : 1;               //is the entity visible

	unsigned long bIsBIGBuilding : 1;           // Set if this entity is a big building
	unsigned long bRenderDamaged : 1;           // use damaged LOD models for objects with applicable damage
	unsigned long bStreamingDontDelete : 1; // Dont let the streaming remove this 
	unsigned long bRemoveFromWorld : 1;     // remove this entity next time it should be processed
	unsigned long bHasHitWall : 1;              // has collided with a building (changes subsequent collisions)
	unsigned long bImBeingRendered : 1;     // don't delete me because I'm being rendered
	unsigned long bDrawLast : 1;             // draw object last
	unsigned long bDistanceFade : 1;         // Fade entity because it is far away

	unsigned long bDontCastShadowsOn : 1;       // Dont cast shadows on this object
	unsigned long bOffscreen : 1;               // offscreen flag. This can only be trusted when it is set to true
	unsigned long bIsStaticWaitingForCollision : 1; // this is used by script created entities - they are static until the collision is loaded below them
	unsigned long bDontStream : 1;              // tell the streaming not to stream me
	unsigned long bUnderwater : 1;              // this object is underwater change drawing order
	unsigned long bHasPreRenderEffects : 1; // Object has a prerender effects attached to it
	unsigned long bIsTempBuilding : 1;          // whether or not the building is temporary (i.e. can be created and deleted more than once)
	unsigned long bDontUpdateHierarchy : 1; // Don't update the aniamtion hierarchy this frame

	unsigned long bHasRoadsignText : 1;     // entity is roadsign and has some 2deffect text stuff to be rendered
	unsigned long bDisplayedSuperLowLOD : 1;
	unsigned long bIsProcObject : 1;            // set object has been generate by procedural object generator
	unsigned long bBackfaceCulled : 1;          // has backface culling on
	unsigned long bLightObject : 1;         // light object with directional lights
	unsigned long bUnimportantStream : 1;       // set that this object is unimportant, if streaming is having problems
	unsigned long bTunnel : 1;          // Is this model part of a tunnel
	unsigned long bTunnelTransition : 1;        // This model should be rendered from within and outside of the tunnel
	/********** END CFLAGS **************/

	WORD        RandomSeed; //32
	WORD        m_nModelIndex;//34
	void *pReferences; //36

	DWORD       * m_pLastRenderedLink; //   CLink<CEntity*>* m_pLastRenderedLink; +40

	WORD m_nScanCode;           // 44
	BYTE m_iplIndex;            // used to define which IPL file object is in +46
	BYTE m_areaCode;            // used to define what objects are visible at this point +47

	// LOD shit
	CEntity * m_pLod; // 48
	// num child higher level LODs
	BYTE numLodChildren; // 52
	// num child higher level LODs that have been rendered
	signed char numLodChildrenRendered; // 53

	//********* BEGIN CEntityInfo **********//
	BYTE nType : 3; // what type is the entity              // 54 (2 == Vehicle)
	BYTE nStatus : 5;               // control status       // 54
	//********* END CEntityInfo **********//

	uint8_t m_pad0; // 55

};

struct CPtrNodeSingle
{
	CEntity* entity;
	CPtrNodeSingle* next;

	void Add(CEntity* entity);
	void Remove(CEntity* entity);
};

struct CPtrNodeDouble : CPtrNodeSingle
{
	CPtrNodeDouble* prev;

	void Add(CEntity* entity);
	void Remove(CEntity* entity);
};

struct CPtrList
{
	union
	{
		CPtrNodeSingle* sNode;
		CPtrNodeDouble* dNode;
	};

	void Add(CEntity* entity);
	void Remove(CEntity* entity);

	void AddDouble(CEntity* entity);
	void RemoveDouble(CEntity* entity);

	operator CPtrNodeSingle*() { return (CPtrNodeSingle*)this; } // this is wrong, but my old assumption was wrong too :)
};

struct CSector
{
	CPtrList building;
	CPtrList dummy;
};

struct CRepeatSector
{
	CPtrList vehicle;
	CPtrList ped;
	CPtrList object;
};

struct CMatrix
{
	CVector right;
	DWORD pad1;
	CVector top;
	DWORD pad2;
	CVector at;
	DWORD pad3;
	CVector pos;
	DWORD pad4;
};

class CRunningScript	// 0xE0 bytes total
{
public:
	void* pNext;			// 0x00
	void* pPrev;			// 0x04
	char strName[8];		// 0x08
	DWORD dwBaseIP;			// 0x10
	DWORD dwScriptIP;		// 0x14
	DWORD dwReturnStack[8];	// 0x18
	WORD dwStackPointer;	// 0x38
	DWORD dwLocalVar[34];	// 0x3C
	BYTE bStartNewScript;	// 0xC4
	BYTE bCompareFlag;			// 0xC5
	BYTE bIsMissionThread;	// 0xC6
	BYTE bIsExternalScript;	// 0xC7
	BYTE bInMenu;			// 0xC8
	BYTE bUnknown;			// 0xC9
	DWORD dwWakeTime;		// 0xCC
	WORD wIfParam;			// 0xD0
	BYTE bNotFlag;			// 0xD2
	BYTE bWastedBustedCheck;// 0xD3
	BYTE bWastedBustedFlag;	// 0xD4
	DWORD dwSceneSkipIP;	// 0xD8
	BYTE bMissionThread;	// 0xDC

public:
	void ProcessOneCommand();

	static bool ProcessBuffer(std::string name, const char* buffer, int* localVars);
};

#define STREAMING_MODEL_MAX (35000)
#define STREAMING_TXD_MAX (STREAMING_MODEL_MAX + 8000)
#define STREAMING_COL_MAX (STREAMING_TXD_MAX + 512)
#define STREAMING_IPL_MAX (STREAMING_COL_MAX + 256)
#define STREAMING_NODE_MAX (STREAMING_IPL_MAX + 64)
#define STREAMING_IFP_MAX (STREAMING_NODE_MAX + 180)
#define STREAMING_RRR_MAX (STREAMING_IFP_MAX + 475)
#define STREAMING_SCM_MAX (STREAMING_RRR_MAX + 300)

#define STREAMING_MODEL_MIN 0
#define STREAMING_TXD_MIN STREAMING_MODEL_MAX
#define STREAMING_COL_MIN STREAMING_TXD_MAX
#define STREAMING_IPL_MIN STREAMING_COL_MAX
#define STREAMING_NODE_MIN STREAMING_IPL_MAX
#define STREAMING_IFP_MIN STREAMING_NODE_MAX
#define STREAMING_RRR_MIN STREAMING_IFP_MAX
#define STREAMING_SCM_MIN STREAMING_RRR_MAX