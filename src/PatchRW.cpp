﻿#include "StdInc.h"
#if !VERYDEAD
#include <d3d9.h>
#include <gl/gl.h>

//#define RWDEBUG

extern "C"
{
#include <rwcore.h>
#include <rwplcore.h>
#include <rpanisot.h>
#include <rpworld.h>
#include <rphanim.h>
#include <rpskin.h>
#include <rpuvanim.h>
#include <rpmatfx.h>
#include <rtpng.h>
};

void nullsub() {}
int nullsub_ret1() { return 1;  }
void returnFunc() {}

void breakpoint()
{
	__asm int 3
}

// from gtamodding.ru someplace
struct TextureFormat
{
	unsigned int platformId;
	unsigned int filterMode : 8;
	unsigned int uAddressing : 4;
	unsigned int vAddressing : 4;
	char name[32];
	char maskName[32];
};

struct RasterFormat
{
	unsigned int rasterFormat;
	D3DFORMAT d3dFormat;
	unsigned short width;
	unsigned short height;
	unsigned char depth;
	unsigned char numLevels;
	unsigned char rasterType;
	unsigned char alpha : 1;
	unsigned char cubeTexture : 1;
	unsigned char autoMipMaps : 1;
	unsigned char compressed : 1;
};

enum RasterFormat8
{
	FORMATDEFAULT = 0x00,
	FORMAT1555 = 0x01,
	FORMAT565 = 0x02,
	FORMAT4444 = 0x03,
	FORMATLUM8 = 0x04,
	FORMAT8888 = 0x05,
	FORMAT888 = 0x06,
	FORMAT16 = 0x07,
	FORMAT24 = 0x08,
	FORMAT32 = 0x09,
	FORMAT555 = 0x0a,
	FORMATAUTOMIPMAP = 0x10,
	FORMATPAL8 = 0x20,
	FORMATPAL4 = 0x40,
	FORMATMIPMAP = 0x80,
	FORMATFORCEENUMSIZEINT = RWFORCEENUMSIZEINT
};

extern "C" extern int _rwOpenGLRasterExtOffset;

RwBool ReadNativeTextureD3D9(void* pOut, void* pInOut, RwInt32 nI)
{
	RwStream* stream = (RwStream*)pOut;

	TextureFormat textureInfo; RasterFormat rasterInfo;
	unsigned int lengthOut, versionOut, numLevels; unsigned char savedFormat;
	RwRaster *raster; RwTexture *texture;

	// Ошибка чтения или некорректная версия
	if (!RwStreamFindChunk(stream, rwID_STRUCT, &lengthOut, &versionOut) || versionOut < 0x34000 || versionOut > 0x36003 ||
		RwStreamRead(stream, &textureInfo, 72) != 72 || textureInfo.platformId != rwID_PCD3D9 ||
		RwStreamRead(stream, &rasterInfo, 16) != 16)
		return false;

	auto origRasterFormat = rasterInfo.rasterFormat;

	// Рассматриваем 3 варианта - компрессия, кубическая текстура, компрессия+кубическая текстура
	if (rasterInfo.compressed)
	{
		raster = RwRasterCreate(rasterInfo.width, rasterInfo.height, rasterInfo.depth, rasterInfo.rasterFormat |
								rasterInfo.rasterType | rwRASTERDONTALLOCATE);
		if (!raster)
			return false;

		numLevels = (raster->cFormat & FORMATMIPMAP) ? rasterInfo.numLevels : 1;
		if (rasterInfo.cubeTexture)
		{
			__asm int 3
		}
		else
		{
			int dxtFormat = 0;
			int dxtFormatNum = 0;

			if (rasterInfo.d3dFormat == MAKEFOURCC('D', 'X', 'T', '1'))
			{
				dxtFormatNum = 1;
				dxtFormat = 33777;
			}
			else if (rasterInfo.d3dFormat == MAKEFOURCC('D', 'X', 'T', '3'))
			{
				dxtFormatNum = 3;
				dxtFormat = 33778;
			}
			else if (rasterInfo.d3dFormat == MAKEFOURCC('D', 'X', 'T', '5'))
			{
				dxtFormatNum = 5;
				dxtFormat = 33779;
			}
			else
			{
				return false;
			}

			char* openglExt = (char*)raster + _rwOpenGLRasterExtOffset;
			*(int*)(openglExt + 80) = dxtFormat;
			*(int*)(openglExt + 84) = dxtFormatNum;

			typedef void (APIENTRY* PFNGLCOMPRESSEDTEXIMAGE2DARBPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid *data);
			PFNGLCOMPRESSEDTEXIMAGE2DARBPROC glCompressedTexImage2DARB = (PFNGLCOMPRESSEDTEXIMAGE2DARBPROC)wglGetProcAddress("glCompressedTexImage2DARB");

			glBindTexture(GL_TEXTURE_2D, *(unsigned int*)openglExt);

			int newWidth = rasterInfo.width;
			int newHeight = rasterInfo.height;

			for (int i = 0; i < rasterInfo.numLevels; i++)
			{
				if (RwStreamRead(stream, &lengthOut, 4) == 4)
				{
					if (lengthOut > 0)
					{
						char* buffer = new char[lengthOut];

						RwStreamRead(stream, buffer, lengthOut);

						glCompressedTexImage2DARB(GL_TEXTURE_2D, i, dxtFormat, max(newWidth, 1), max(newHeight, 1), 0, lengthOut, buffer);

						auto err = glGetError();

						if (err != 0)
						{
							printf("gl error: %x\n", err);
						}

						delete[] buffer;
					}
				}

				newWidth /= 2;
				newHeight /= 2;
			}

			/*if (gtaD3D9TexCreate(raster->width, raster->height, numLevels, rasterInfo.d3dFormat, &raster->d3dRaster.texture) != D3D_OK)
			{
				RwRasterDestroy(raster);
				return false;
			}*/
		}
		//raster->d3dRaster.hasCompression = 1;
		//raster->d3dRaster.format = rasterInfo.d3dFormat;
	}
	else
	{
		if (!rasterInfo.cubeTexture)
		{
			if ((rasterInfo.rasterFormat & rwRASTERFORMATMIPMAP) && (rasterInfo.rasterFormat & rwRASTERFORMATAUTOMIPMAP))
			{
				raster = RwRasterCreate(rasterInfo.width, rasterInfo.height, rasterInfo.d3dFormat, rasterInfo.rasterType |
											(rasterInfo.rasterFormat & (rwRASTERFORMATMIPMAP | rwRASTERFORMATAUTOMIPMAP)));
				if (!raster)
					return false;
			}
			else
			{
				if (rasterInfo.rasterFormat == rwRASTERFORMAT888 && rasterInfo.depth == 32)
				{
					rasterInfo.rasterFormat = rwRASTERFORMAT8888;
				}

				raster = RwRasterCreate(rasterInfo.width, rasterInfo.height, rasterInfo.depth,
										rasterInfo.rasterFormat | rasterInfo.rasterType);
				if (!raster)
					return false;
			}
		}
		else
		{
			__asm int 3
		}
	}
	//raster->cFlags ^= rwRASTERDONTALLOCATE;
	//raster->d3dRaster.hasAlpha = rasterInfo.alpha;

	// Если у нас что-то не совпало - ну его...
	if (rasterInfo.rasterFormat != ((raster->cFormat) << 8) ||
		rasterInfo.width != raster->width || rasterInfo.height != raster->height)
	{
		RwRasterDestroy(raster);
		return false;
	}

	// Читаем палитру, если надо
	if (rasterInfo.rasterFormat & rwRASTERFORMATPAL4)
	{
		if (RwStreamRead(stream, RwRasterLockPalette(raster, rwRASTERLOCKWRITE), 128) != 128)
		{
			RwRasterUnlockPalette(raster);
			RwRasterDestroy(raster);
			return false;
		}
	}
	else if (rasterInfo.rasterFormat & rwRASTERFORMATPAL8)
	{
		if (RwStreamRead(stream, RwRasterLockPalette(raster, rwRASTERLOCKWRITE), 1024) != 1024)
		{
			RwRasterUnlockPalette(raster);
			RwRasterDestroy(raster);
			return false;
		}
	}
	RwRasterUnlockPalette(raster);

	// Читаем сорфейсы
	if (!rasterInfo.compressed && raster->depth == rasterInfo.depth)
	{
		savedFormat = raster->cFormat; // Зачем-то выключаем автомипмаппинг на время чтения
		raster->cFormat ^= FORMATAUTOMIPMAP;
		//for (int i = 0; i < 1; i++)
		//{
			for (int j = 0; j < rasterInfo.numLevels; j++)
			{
				if (RwStreamRead(stream, &lengthOut, 4) == 4)
				{
					void* buffer = RwRasterLock(raster, j, rwRASTERLOCKWRITE);

					if (RwStreamRead(stream, buffer, lengthOut) == lengthOut)
					{
						if (origRasterFormat == rwRASTERFORMAT888)
						{
							char* buf = (char*)buffer;

							// order: b g r a, i guess
							for (int i = 0; i < lengthOut; i += 4)
							{
								char r = buf[i];
								buf[i] = buf[i + 2];
								buf[i + 2] = r;
							}
						}

						RwRasterUnlock(raster);
						continue;
					}
				}
				RwRasterUnlock(raster);
				RwRasterDestroy(raster);
				return false;
			}
		//}
		raster->cFormat = savedFormat;
	}
	else if (raster->depth != rasterInfo.depth)
	{
		RwStreamRead(stream, &lengthOut, 4);

		static char blah[500000];
		RwStreamRead(stream, blah, lengthOut);
	}

	texture = RwTextureCreate(raster);
	if (!texture)
	{
		RwRasterDestroy(raster);
		return false;
	}
	/*texture->filter = textureInfo.filterMode;
	texture->uAddressing = textureInfo.uAddressing;
	texture->vAddressing = textureInfo.vAddressing;*/
	RwTextureSetFilterMode(texture, (RwTextureFilterMode)textureInfo.filterMode);
	RwTextureSetAddressingU(texture, (RwTextureAddressMode)textureInfo.uAddressing);
	RwTextureSetAddressingV(texture, (RwTextureAddressMode)textureInfo.vAddressing);
	//RwTextureSetAddressingU(texture, rwTEXTUREADDRESSCLAMP);
	//RwTextureSetAddressingV(texture, rwTEXTUREADDRESSCLAMP);
	RwTextureSetName(texture, textureInfo.name);
	RwTextureSetMaskName(texture, textureInfo.maskName);
	*(RwTexture**)pInOut = texture;

	return true;
}

struct RwD3D9Vertex
{
	RwReal      x;              /**< Screen X */
	RwReal      y;              /**< Screen Y */
	RwReal      z;              /**< Screen Z */
	RwReal      rhw;            /**< Reciprocal of homogeneous W */

	RwUInt32    emissiveColor;  /**< Vertex color */

	RwReal      u;              /**< Texture coordinate U */
	RwReal      v;              /**< Texture coordinate V */
};

RwBool RwEngineOpenWrapper(RwEngineOpenParams* params)
{
	using namespace MemoryVP;

	RwBool retval = RwEngineOpen(params);

	RWSRCGLOBAL(stdFunc[rwSTANDARDNATIVETEXTUREREAD]) = ReadNativeTextureD3D9;

	static RwIm2DRenderPrimitiveFunction renderPrimitive = RWSRCGLOBAL(dOpenDevice).fpIm2DRenderPrimitive;
	RwIm2DRenderPrimitiveFunction func1 = [] (RwPrimitiveType primType, RwIm2DVertex *vertices, RwInt32 numVertices) -> RwBool
	{
		RwD3D9Vertex* inVertex = (RwD3D9Vertex*)vertices;
		static RwIm2DVertex outVertices[1024];

		for (int i = 0; i < numVertices; i++)
		{
			RwIm2DVertexSetIntRGBA(&outVertices[i], (inVertex[i].emissiveColor >> 16) & 0xFF, (inVertex[i].emissiveColor >> 8) & 0xFF, (inVertex[i].emissiveColor) & 0xFF, (inVertex[i].emissiveColor >> 24) & 0xFF);
			RwIm2DVertexSetScreenX(&outVertices[i], inVertex[i].x);
			RwIm2DVertexSetScreenY(&outVertices[i], inVertex[i].y);
			RwIm2DVertexSetScreenZ(&outVertices[i], inVertex[i].z);
			RwIm2DVertexSetU(&outVertices[i], inVertex[i].u, 1.0f);
			RwIm2DVertexSetV(&outVertices[i], inVertex[i].v, 1.0f);
		}

		return RWSRCGLOBAL(dOpenDevice).fpIm2DRenderPrimitive(primType, outVertices, numVertices);
	};

	MemoryVP::InjectHook(0x734E90, func1);

	static RwIm2DRenderLineFunction renderLine = RWSRCGLOBAL(dOpenDevice).fpIm2DRenderLine;
	//RWSRCGLOBAL(dOpenDevice).fpIm2DRenderLine = [] (RwIm2DVertex *vertices, RwInt32 numVertices,
	RwIm2DRenderLineFunction func2 = [] (RwIm2DVertex *vertices, RwInt32 numVertices,
													RwInt32 vert1, RwInt32 vert2) -> RwBool
	{
		RwD3D9Vertex* inVertex = (RwD3D9Vertex*)vertices;
		static RwIm2DVertex outVertices[1024];

		for (int i = 0; i < numVertices; i++)
		{
			RwIm2DVertexSetIntRGBA(&outVertices[i], (inVertex[i].emissiveColor >> 16) & 0xFF, (inVertex[i].emissiveColor >> 8) & 0xFF, (inVertex[i].emissiveColor) & 0xFF, (inVertex[i].emissiveColor >> 24) & 0xFF);
			RwIm2DVertexSetScreenX(&outVertices[i], inVertex[i].x);
			RwIm2DVertexSetScreenY(&outVertices[i], inVertex[i].y);
			RwIm2DVertexSetScreenZ(&outVertices[i], inVertex[i].z);
			RwIm2DVertexSetU(&outVertices[i], inVertex[i].u, 1.0f);
			RwIm2DVertexSetV(&outVertices[i], inVertex[i].v, 1.0f);
		}

		return RWSRCGLOBAL(dOpenDevice).fpIm2DRenderLine(outVertices, numVertices, vert1, vert2);
		//return renderLine(outVertices, numVertices, vert1, vert2);
	};

	static RwIm2DRenderTriangleFunction renderTriangle = RWSRCGLOBAL(dOpenDevice).fpIm2DRenderTriangle;
	//RWSRCGLOBAL(dOpenDevice).fpIm2DRenderTriangle = [] (RwIm2DVertex *vertices, RwInt32 numVertices,
	RwIm2DRenderTriangleFunction func3 = [] (RwIm2DVertex *vertices, RwInt32 numVertices,
														RwInt32 vert1, RwInt32 vert2, RwInt32 vert3) -> RwBool
	{
		RwD3D9Vertex* inVertex = (RwD3D9Vertex*)vertices;
		static RwIm2DVertex outVertices[1024];

		for (int i = 0; i < numVertices; i++)
		{
			RwIm2DVertexSetIntRGBA(&outVertices[i], (inVertex[i].emissiveColor >> 16) & 0xFF, (inVertex[i].emissiveColor >> 8) & 0xFF, (inVertex[i].emissiveColor) & 0xFF, (inVertex[i].emissiveColor >> 24) & 0xFF);
			RwIm2DVertexSetScreenX(&outVertices[i], inVertex[i].x);
			RwIm2DVertexSetScreenY(&outVertices[i], inVertex[i].y);
			RwIm2DVertexSetScreenZ(&outVertices[i], inVertex[i].z);
			RwIm2DVertexSetU(&outVertices[i], inVertex[i].u, 1.0f);
			RwIm2DVertexSetV(&outVertices[i], inVertex[i].v, 1.0f);
		}

		return RWSRCGLOBAL(dOpenDevice).fpIm2DRenderTriangle(outVertices, numVertices, vert1, vert2, vert3);
		//return renderTriangle(outVertices, numVertices, vert1, vert2, vert3);
	};

	static RwIm2DRenderIndexedPrimitiveFunction renderIndexedPrimitive = RWSRCGLOBAL(dOpenDevice).fpIm2DRenderIndexedPrimitive;
	//RWSRCGLOBAL(dOpenDevice).fpIm2DRenderIndexedPrimitive = [] (RwPrimitiveType primType, RwIm2DVertex *vertices, RwInt32 numVertices, RwImVertexIndex* indices, RwInt32 numIndices) -> RwBool
	RwIm2DRenderIndexedPrimitiveFunction func4 = [] (RwPrimitiveType primType, RwIm2DVertex *vertices, RwInt32 numVertices, RwImVertexIndex* indices, RwInt32 numIndices) -> RwBool
	{
		RwD3D9Vertex* inVertex = (RwD3D9Vertex*)vertices;
		short* inIndex = (short*)indices;
		static RwIm2DVertex outVertices[1024];
		static RxVertexIndex outIndices[2048];

		for (int i = 0; i < numVertices; i++)
		{
			RwIm2DVertexSetIntRGBA(&outVertices[i], (inVertex[i].emissiveColor >> 16) & 0xFF, (inVertex[i].emissiveColor >> 8) & 0xFF, (inVertex[i].emissiveColor) & 0xFF, (inVertex[i].emissiveColor >> 24) & 0xFF);
			RwIm2DVertexSetScreenX(&outVertices[i], inVertex[i].x);
			RwIm2DVertexSetScreenY(&outVertices[i], inVertex[i].y);
			RwIm2DVertexSetScreenZ(&outVertices[i], inVertex[i].z);
			RwIm2DVertexSetU(&outVertices[i], inVertex[i].u, 1.0f);
			RwIm2DVertexSetV(&outVertices[i], inVertex[i].v, 1.0f);
		}

		for (int i = 0; i < numIndices; i++)
		{
			outIndices[i] = inIndex[i];
		}

		return RWSRCGLOBAL(dOpenDevice).fpIm2DRenderIndexedPrimitive(primType, outVertices, numVertices, outIndices, numIndices);
		//return renderIndexedPrimitive(primType, outVertices, numVertices, outIndices, numIndices);
	};

	InjectHook(0x734EC0, func2);
	InjectHook(0x734EB0, func3);
	InjectHook(0x734EA0, func4);

	/*RwDebugSetHandler([] (RwDebugType type, const RwChar* message)
	{
		OutputDebugStringA(message);
		OutputDebugStringA("\n");
	});*/

	return retval;
}

int WRAPPER gta_ftell(FILE* file) { EAXJMP(0x826261); }
int WRAPPER gta_fseek(FILE* file, int, int) { EAXJMP(0x82374F); }

int CFileMgr__GetTotalSize(FILE* file)
{
	int pos = gta_ftell(file);
	gta_fseek(file, 0, SEEK_END);

	int retval = gta_ftell(file);
	gta_fseek(file, pos, SEEK_SET);

	return retval;
}

extern "C" extern int MatFXMaterialDataOffset;

int RpMatFXPluginAttachWrap()
{
	int rv = RpMatFXPluginAttach();

	*(uint32_t*)0xC9AB74 = MatFXMaterialDataOffset;

	return rv;
}

RwBool RwIm3DRenderIndexedPrimitiveWrap(RwPrimitiveType primType, RwImVertexIndex* indices, RwInt32 numIndices)
{
	static RwImVertexIndex newIndices[16384];
	short* inIndex = (short*)indices;

	for (int i = 0; i < numIndices; i++)
	{
		newIndices[i] = inIndex[i];
	}

	return RwIm3DRenderIndexedPrimitive(primType, newIndices, numIndices);
}

struct RxObjSpace3DVertex_D3D9
{
	RwV3d       objVertex;        /**< position */
	RwV3d       objNormal;        /**< normal */
	RwUInt32    color;            /**< emissive color*/
	RwReal      u;                /**< u */
	RwReal      v;                /**< v */
};

void* RwIm3DTransformWrap(RwIm3DVertex *pVerts, RwUInt32 numVerts, RwMatrix *ltm, RwUInt32 flags)
{
	static RwIm3DVertex newVertices[16384];
	RxObjSpace3DVertex_D3D9* inVerts = (RxObjSpace3DVertex_D3D9*)pVerts;

	for (int i = 0; i < numVerts; i++)
	{
		RwIm3DVertexSetPos(&newVertices[i], inVerts[i].objVertex.x, inVerts[i].objVertex.y, inVerts[i].objVertex.z);
		RwIm3DVertexSetNormal(&newVertices[i], inVerts[i].objNormal.x, inVerts[i].objNormal.y, inVerts[i].objNormal.z);
		RwIm3DVertexSetRGBA(&newVertices[i], (inVerts[i].color >> 16) & 0xFF, (inVerts[i].color >> 8) & 0xFF, (inVerts[i].color) & 0xFF, (inVerts[i].color >> 24) & 0xFF);
		RwIm3DVertexSetU(&newVertices[i], inVerts[i].u);
		RwIm3DVertexSetV(&newVertices[i], inVerts[i].v);
	}

	return RwIm3DTransform(newVertices, numVerts, ltm, flags);
}

extern "C" int _rwStreamReadChunkHeader();

void __declspec(naked) ReadChunkHeaderTail()
{
	__asm
	{
		push eax
		
		mov eax, [esp + 14h]
		test eax, eax

		jz justHaveFun

		mov dword ptr [eax], 36003h

	justHaveFun:
		pop eax

		retn
	}
}

void PatchRW()
{
	using namespace MemoryVP;

	// patching ourselves? FUN
	InjectHook((char*)_rwStreamReadChunkHeader + 0xE6, ReadChunkHeaderTail, PATCH_JUMP);

	Patch(0x619626, 70); // manual frame limiter
	Nop(0x53E94D, 2); // cut broken limiter short

	Patch<uint8_t>(0x745A80, 0xC3); // temp, will leak!

	Patch<uint8_t>(0x6FDD70, 0xC3); // license plate rendering

	Patch<uint8_t>(0x703650, 0xC3); // postfx
	Patch<uint8_t>(0x7046E0, 0xC3); // all of postfx

	InjectHook(0x5389E0, CFileMgr__GetTotalSize);
	
	Patch(0x745AB1, offsetof(RwGlobals, memoryFuncs.rwfree));
	Patch(0x745ACB, offsetof(RwGlobals, memoryFuncs.rwfree));

	InjectHook(0x5D8980, nullsub_ret1);

	Patch<uint8_t>(0x7046E0, 0xC3); // full postfx

	InjectHook(0x7CF9B0, RtPNGImageRead);
	InjectHook(0x748F20, RpAnisotGetMaxSupportedMaxAnisotropy);
	InjectHook(0x748F30, RpAnisotTextureSetMaxAnisotropy);
	InjectHook(0x748F50, RpAnisotTextureGetMaxAnisotropy);
	InjectHook(0x748F70, RpAnisotPluginAttach);
	/*InjectHook(0x749000, anisotOpen);
	InjectHook(0x749010, anisotClose);
	InjectHook(0x749020, anisotConstruct);
	InjectHook(0x749030, anisotDestruct);
	InjectHook(0x749040, anisotCopy);
	InjectHook(0x749060, anisotRead);
	InjectHook(0x7490B0, anisotWrite);
	InjectHook(0x7490F0, anisotGetSize);
	InjectHook(0x749120, _rpReadAtomicRights);
	InjectHook(0x749160, _rpWriteAtomicRights);
	InjectHook(0x7491A0, _rpSizeAtomicRights);*/
	InjectHook(0x7491C0, AtomicDefaultRenderCallBack);
	InjectHook(0x7491F0, _rpAtomicResyncInterpolatedSphere);
	InjectHook(0x749330, RpAtomicGetWorldBoundingSphere);
	/*InjectHook(0x749480, _rpClumpClose);
	InjectHook(0x749520, ClumpTidyDestroyClump);
	InjectHook(0x7496A0, ClumpTidyDestroyAtomic);*/
	InjectHook(0x749720, RpAtomicSetFreeListCreateParams);
	InjectHook(0x749740, RpClumpSetFreeListCreateParams);
	/*InjectHook(0x749760, _rpClumpOpen);
	InjectHook(0x749830, _rpClumpRegisterExtensions);
	InjectHook(0x749880, ClumpInitCameraEx);
	InjectHook(0x7498A0, ClumpDeInitCameraExt);
	InjectHook(0x7498B0, ClumpInitLightExt);
	InjectHook(0x7498D0, ClumpDeInitLightExt);*/
	InjectHook(0x7498E0, RpClumpGetNumAtomics);
	InjectHook(0x749910, RpClumpGetNumLights);
	InjectHook(0x749940, RpClumpGetNumCameras);
	InjectHook(0x749970, RpClumpCreateSpace);
	InjectHook(0x749B20, RpClumpRender);
	InjectHook(0x749B70, RpClumpForAllAtomics);
	InjectHook(0x749BB0, RpClumpForAllCameras);
	InjectHook(0x749C00, RpClumpForAllLights);
	InjectHook(0x749C50, RpAtomicCreate);
	//InjectHook(0x749D20, AtomicSync);
	InjectHook(0x749D40, RpAtomicSetGeometry);
	InjectHook(0x749DC0, RpAtomicDestroy);
	InjectHook(0x749E40, RpLightGetClump);
	InjectHook(0x749E50, RwCameraGetClump);
	InjectHook(0x749E60, RpAtomicClone);
	InjectHook(0x749F70, RpClumpClone);
// 	InjectHook(0x74A1C0, DestroyClumpLight);
// 	InjectHook(0x74A200, DestroyClumpCamera);
	InjectHook(0x74A240, RpClumpSetCallBack);
	/*InjectHook(0x74A260, ClumpCallBack);*/
	InjectHook(0x74A270, RpClumpGetCallBack);
	InjectHook(0x74A290, RpClumpCreate);
	InjectHook(0x74A310, RpClumpDestroy);
	InjectHook(0x74A490, RpClumpAddAtomic);
	InjectHook(0x74A4C0, RpClumpRemoveAtomic);
	InjectHook(0x74A4F0, RpClumpAddLight);
	InjectHook(0x74A520, RpClumpRemoveLight);
	InjectHook(0x74A550, RpClumpAddCamera);
	InjectHook(0x74A580, RpClumpRemoveCamera);
	InjectHook(0x74A5B0, RpAtomicStreamGetSize);
	InjectHook(0x74A5E0, RpClumpStreamGetSize);
	/*InjectHook(0x74A770, GeometryListInitialize);*/
	InjectHook(0x74A850, RpAtomicStreamWrite);
	/*InjectHook(0x74A880, ClumpAtomicStreamWrite);*/
	InjectHook(0x74AA10, RpClumpStreamWrite);
// 	InjectHook(0x74AE40, CountLight);
// 	InjectHook(0x74AE50, GeometryArrayDestroy);
// 	InjectHook(0x74AEA0, ClumpLightStreamWrite);
// 	InjectHook(0x74AF20, ClumpCameraStreamWrite);
// 	InjectHook(0x74AFA0, ClumpLightAddSize);
// 	InjectHook(0x74AFD0, ClumpCameraAddSize);
// 	InjectHook(0x74B000, ClumpAtomicAddSize);
	InjectHook(0x74B030, RpAtomicStreamRead);
/*	InjectHook(0x74B060, ClumpAtomicStreamRead);*/
	InjectHook(0x74B420, RpClumpStreamRead);
/*	InjectHook(0x74BCC0, DestroyClumpAtomic);*/
	InjectHook(0x74BD40, _rpClumpChunkInfoRead);
	InjectHook(0x74BDA0, RpAtomicRegisterPlugin);
	InjectHook(0x74BDD0, RpClumpRegisterPlugin);
	InjectHook(0x74BE00, RpAtomicRegisterPluginStream);
	InjectHook(0x74BE30, RpAtomicSetStreamAlwaysCallBack);
	InjectHook(0x74BE50, RpAtomicSetStreamRightsCallBack);
	InjectHook(0x74BE70, RpClumpRegisterPluginStream);
	InjectHook(0x74BEA0, RpClumpSetStreamAlwaysCallBack);
	InjectHook(0x74BEC0, RpAtomicGetPluginOffset);
	InjectHook(0x74BEE0, RpClumpGetPluginOffset);
	InjectHook(0x74BF00, RpAtomicValidatePlugins);
	InjectHook(0x74BF10, RpClumpValidatePlugins);
	InjectHook(0x74BF20, RpAtomicSetFrame);
	InjectHook(0x74BF40, RpAtomicInstance);
// 	InjectHook(0x74BFA0, _rpGeometryOpen);
// 	InjectHook(0x74BFC0, _rpGeometryClose);
// 	InjectHook(0x74BFD0, _rpGeometryGetTKListFirstRegEntry);
	InjectHook(0x74BFE0, RpGeometryTransform);
	InjectHook(0x74C130, RpGeometryCreateSpace);
	InjectHook(0x74C200, RpMorphTargetCalcBoundingSphere);
	InjectHook(0x74C310, RpGeometryAddMorphTargets);
	InjectHook(0x74C4D0, RpGeometryAddMorphTarget);
	InjectHook(0x74C4E0, RpGeometryRemoveMorphTarget);
	InjectHook(0x74C690, RpGeometryTriangleSetVertexIndices);
	InjectHook(0x74C6C0, RpGeometryTriangleSetMaterial);
	InjectHook(0x74C720, RpGeometryTriangleGetVertexIndices);
	InjectHook(0x74C760, RpGeometryTriangleGetMaterial);
	InjectHook(0x74C790, RpGeometryForAllMaterials);
	InjectHook(0x74C7D0, RpGeometryLock);
	InjectHook(0x74C800, RpGeometryUnlock);
	InjectHook(0x74CA60, RpGeometryForAllMeshes);
	InjectHook(0x74CA90, RpGeometryCreate);
	InjectHook(0x74CCB0, RpGeometryAddRef);
	InjectHook(0x74CCC0, RpGeometryDestroy);
	InjectHook(0x74CD70, RpGeometryRegisterPlugin);
	InjectHook(0x74CDA0, RpGeometryRegisterPluginStream);
	InjectHook(0x74CDD0, RpGeometrySetStreamAlwaysCallBack);
	InjectHook(0x74CDF0, RpGeometryGetPluginOffset);
	InjectHook(0x74CE10, RpGeometryValidatePlugins);
	InjectHook(0x74CE20, RpGeometryStreamGetSize);
	/*InjectHook(0x74CE60, GeometryStreamGetSizeActual);*/
	InjectHook(0x74CED0, RpGeometryStreamWrite);
	InjectHook(0x74D190, RpGeometryStreamRead);
	//InjectHook(0x74D6D0, RpGeometryStreamRead__internal ? );
 	InjectHook(0x74D750, _rpGeometryChunkInfoRead);
// 	InjectHook(0x74D7D0, _rpReadMaterialRights);
// 	InjectHook(0x74D810, _rpWriteMaterialRights);
// 	InjectHook(0x74D850, _rpSizeMaterialRights);
// 	InjectHook(0x74D870, _rpMaterialSetDefaultSurfaceProperties);
	InjectHook(0x74D8C0, RpMaterialSetFreeListCreateParams);
// 	InjectHook(0x74D8E0, _rpMaterialOpen);
// 	InjectHook(0x74D950, _rpMaterialClose);
	InjectHook(0x74D990, RpMaterialCreate);
	InjectHook(0x74DA20, RpMaterialDestroy);
	InjectHook(0x74DA80, RpMaterialClone);
	InjectHook(0x74DBC0, RpMaterialSetTexture);
	InjectHook(0x74DBF0, RpMaterialRegisterPlugin);
	InjectHook(0x74DC20, RpMaterialRegisterPluginStream);
	InjectHook(0x74DC50, RpMaterialSetStreamAlwaysCallBack);
	InjectHook(0x74DC70, RpMaterialSetStreamRightsCallBack);
	InjectHook(0x74DC90, RpMaterialGetPluginOffset);
	InjectHook(0x74DCB0, RpMaterialValidatePlugins);
	InjectHook(0x74DCC0, _rpMaterialChunkInfoRead);
	InjectHook(0x74DD30, RpMaterialStreamRead);
	InjectHook(0x74E010, RpMaterialStreamGetSize);
	InjectHook(0x74E050, RpMaterialStreamWrite);
	InjectHook(0x74E150, _rpMaterialListDeinitialize);
	InjectHook(0x74E1B0, _rpMaterialListInitialize);
	InjectHook(0x74E1C0, _rpMaterialListAlloc);
	InjectHook(0x74E1F0, _rpMaterialListCopy);
	InjectHook(0x74E2B0, _rpMaterialListGetMaterial);
	InjectHook(0x74E2C0, _rpMaterialListSetSize);
	InjectHook(0x74E350, _rpMaterialListAppendMaterial);
	InjectHook(0x74E420, _rpMaterialListFindMaterialIndex);
	InjectHook(0x74E450, _rpMaterialListStreamGetSize);
	InjectHook(0x74E4B0, _rpMaterialListStreamWrite);
	InjectHook(0x74E600, _rpMaterialListStreamRead);
// 	InjectHook(0x74E970, _rpD3D9AddDynamicGeometry);
// 	InjectHook(0x74E9E0, _rpD3D9RemoveDynamicGeometry);
// 	InjectHook(0x74EA40, _rxWorldDevicePluginAttach);
// 	InjectHook(0x74EA80, D3D9WorldRestoreCallback);
// 	InjectHook(0x74EAB0, _rpCreatePlatformMaterialPipelines);
// 	InjectHook(0x74EAC0, _rpDestroyPlatformMaterialPipelines);
// 	InjectHook(0x74EAD0, _rpCreatePlatformWorldSectorPipelines);
// 	InjectHook(0x74EB40, _rpDestroyPlatformWorldSectorPipelines);
// 	InjectHook(0x74EB80, _rpCreatePlatformAtomicPipelines);
// 	InjectHook(0x74EC00, _rpDestroyPlatformAtomicPipelines);
	InjectHook(0x74ECA0, _rpWorldSectorDeinstanceAll);
	InjectHook(0x74ED50, _rpWorldSectorDestroyRecurse);
	InjectHook(0x74EEC0, _rpSectorDefaultRenderCallBack);
	InjectHook(0x74EF10, _rpWorldForAllGlobalLights);
	InjectHook(0x74EF60, _rpWorldSectorForAllLocalLights);
	InjectHook(0x74EFA0, _rpWorldFindBBox);
	InjectHook(0x74F020, _rpWorldSetupSectorBoundingBoxes);
	InjectHook(0x74F0C0, _rpWorldRegisterWorld);
	InjectHook(0x74F140, _rpWorldUnregisterWorld);
	InjectHook(0x74F1A0, RpWorldLock);
	InjectHook(0x74F210, RpWorldUnlock);
	InjectHook(0x74F4E0, RpWorldSectorGetWorld);
	InjectHook(0x74F570, RpWorldRender);
	//InjectHook(0x74F590, WorldSectorRender);
	InjectHook(0x74F610, RpWorldDestroy);
	InjectHook(0x74F730, RpWorldSetSectorRenderCallBack);
	InjectHook(0x74F750, RpWorldGetSectorRenderCallBack);
	InjectHook(0x74F760, RpWorldCreate);
	//InjectHook(0x74FB00, sub_74FB00);
	InjectHook(0x74FB80, RpWorldForAllClumps);
	InjectHook(0x74FBC0, RpWorldForAllMaterials);
	InjectHook(0x74FC00, RpWorldForAllLights);
	InjectHook(0x74FC70, RpWorldForAllWorldSectors);
	InjectHook(0x74FCD0, RpWorldRegisterPlugin);
	InjectHook(0x74FD00, RpWorldRegisterPluginStream);
	InjectHook(0x74FD30, RpWorldSetStreamAlwaysCallBack);
	InjectHook(0x74FD50, RpWorldSetStreamRightsCallBack);
	InjectHook(0x74FD70, RpWorldGetPluginOffset);
	InjectHook(0x74FD90, RpWorldValidatePlugins);
	InjectHook(0x74FDA0, RpWorldPluginAttach);
// 	InjectHook(0x74FE90, WorldClose);
// 	InjectHook(0x74FEE0, WorldOpen);
	InjectHook(0x74FF60, RpTieSetFreeListCreateParams);
	InjectHook(0x74FF80, RpLightTieSetFreeListCreateParams);
// 	InjectHook(0x74FFA0, _rpLightTieDestroy);
// 	InjectHook(0x74FFF0, _rpTieDestroy);
// 	InjectHook(0x750050, _rpWorldObjRegisterExtensions);
// 	InjectHook(0x750280, WorldObjectOpen);
// 	InjectHook(0x750370, WorldObjectClose);
// 	InjectHook(0x7503E0, WorldInitCameraExt);
// 	InjectHook(0x750430, WorldCameraBeginUpdate);
// 	InjectHook(0x750460, WorldCameraEndUpdate);
// 	InjectHook(0x750480, WorldCameraSync);
// 	InjectHook(0x750710, WorldCopyCameraExt);
// 	InjectHook(0x750760, WorldDeInitCameraExt);
// 	InjectHook(0x7507B0, WorldInitAtomicExt);
// 	InjectHook(0x7507E0, WorldAtomicSync);
// 	InjectHook(0x7509A0, WorldCopyAtomicExt);
// 	InjectHook(0x7509B0, WorldDeInitAtomicExt);
// 	InjectHook(0x750A30, WorldInitClumpExt);
// 	InjectHook(0x750A60, WorldCopyClumpExt);
// 	InjectHook(0x750A90, WorldDeInitClumpExt);
// 	InjectHook(0x750AA0, WorldInitLightExt);
// 	InjectHook(0x750AC0, WorldLightSync);
// 	InjectHook(0x750CD0, WorldCopyLightExt);
// 	InjectHook(0x750D30, WorldDeInitLightExt);
// 	InjectHook(0x750D90, writeGeometryMesh);
// 	InjectHook(0x750DB0, readGeometryMesh);
// 	InjectHook(0x750DE0, sizeGeometryMesh);
// 	InjectHook(0x750E00, writeGeometryNative);
// 	InjectHook(0x750E20, readGeometryNative);
// 	InjectHook(0x750E40, sizeGeometryNative);
// 	InjectHook(0x750E50, writeWorldSectorNative);
// 	InjectHook(0x750E70, readWorldSectorNative);
// 	InjectHook(0x750E90, sizeWorldSectorNative);
// 	InjectHook(0x750EA0, writeSectorMesh);
// 	InjectHook(0x750ED0, readSectorMesh);
// 	InjectHook(0x750F00, sizeSectorMesh);
	InjectHook(0x750F20, RpWorldAddCamera);
	InjectHook(0x750F50, RpWorldRemoveCamera);
	InjectHook(0x750F80, RwCameraGetWorld);
	InjectHook(0x750F90, RpWorldAddAtomic);
	InjectHook(0x750FC0, RpWorldRemoveAtomic);
	InjectHook(0x751050, RpAtomicGetWorld);
	InjectHook(0x751060, RpAtomicForAllWorldSectors);
	InjectHook(0x7510A0, RpWorldSectorForAllAtomics);
	InjectHook(0x751140, RpWorldSectorForAllCollisionAtomics);
	InjectHook(0x7511E0, RpWorldSectorForAllLights);
	InjectHook(0x751300, RpWorldAddClump);
// 	InjectHook(0x751390, WorldAddClumpAtomic);
// 	InjectHook(0x7513D0, WorldAddClumpLight);
// 	InjectHook(0x751420, WorldAddClumpCamera);
	InjectHook(0x751460, RpWorldRemoveClump);
// 	InjectHook(0x751500, WorldRemoveClumpAtomic);
// 	InjectHook(0x7515A0, WorldRemoveClumpLight);
// 	InjectHook(0x751630, WorldRemoveClumpCamera);
	InjectHook(0x751660, RwCameraForAllSectorsInFrustum);
	InjectHook(0x7516B0, RpClumpGetWorld);
	InjectHook(0x7516C0, RwCameraForAllClumpsInFrustum);
	InjectHook(0x7517F0, RwCameraForAllAtomicsInFrustum);
	InjectHook(0x751910, RpWorldAddLight);
	InjectHook(0x751960, RpWorldRemoveLight);
	InjectHook(0x7519E0, RpLightGetWorld);
	InjectHook(0x7519F0, RpLightForAllWorldSectors);
	InjectHook(0x751A70, RpLightSetRadius);
	InjectHook(0x751A90, RpLightSetColor);
	InjectHook(0x751AE0, RpLightGetConeAngle);
	InjectHook(0x751D20, RpLightSetConeAngle);
	InjectHook(0x751D60, RpLightRegisterPlugin);
	InjectHook(0x751D90, RpLightRegisterPluginStream);
	InjectHook(0x751DC0, RpLightSetStreamAlwaysCallBack);
	InjectHook(0x751DE0, RpLightGetPluginOffset);
	InjectHook(0x751E00, RpLightValidatePlugins);
	InjectHook(0x751E10, RpLightStreamGetSize);
	/*InjectHook(0x751E30, RwLightStreamWrite);*/
	InjectHook(0x751F00, RpLightStreamRead);
	InjectHook(0x752060, _rpLightChunkInfoRead);
	InjectHook(0x7520D0, RpLightDestroy);
	InjectHook(0x752110, RpLightCreate);
// 	InjectHook(0x7521A0, LightSync);
// 	InjectHook(0x7521B0, _rpLightClose);
// 	InjectHook(0x752210, LightTidyDestroyLight);
	InjectHook(0x752250, RpLightSetFreeListCreateParams);
// 	InjectHook(0x752270, _rpLightOpen);
// 	InjectHook(0x7522E0, _rpD3D9VertexDeclarationGetSize);
// 	InjectHook(0x7522F0, _rpD3D9VertexDeclarationGetStride);
// 	InjectHook(0x752320, _rpD3D9VertexDeclarationInstWeights);
// 	InjectHook(0x752AD0, _rpD3D9VertexDeclarationInstV3d);
// 	InjectHook(0x7531B0, _rpD3D9VertexDeclarationInstV3dComp);
// 	InjectHook(0x753B60, _rpD3D9VertexDeclarationInstV3dMorph);
// 	InjectHook(0x7544E0, _rpD3D9VertexDeclarationInstV2d);
// 	InjectHook(0x754AE0, _rpD3D9VertexDeclarationInstColor);
// 	InjectHook(0x754B40, _rpD3D9VertexDeclarationInstIndices);
// 	InjectHook(0x754C80, _rpD3D9VertexDeclarationInstIndicesRemap);
// 	InjectHook(0x754E20, _rpD3D9VertexDeclarationInstTangent);
// 	InjectHook(0x7551F0, _rpD3D9VertexDeclarationUnInstV3d);
// 	InjectHook(0x7555E0, _rpD3D9VertexDeclarationUnInstV2d);
// 	InjectHook(0x755830, _rpD3D9GetMinMaxValuesV3d);
// 	InjectHook(0x7558F0, _rpD3D9GetMinMaxValuesV2d);
// 	InjectHook(0x755980, _rpD3D9FindFormatV3d);
// 	InjectHook(0x755AA0, _rpD3D9FindFormatV2d);
// 	InjectHook(0x755B90, _rpD3D9LightPluginAttach);
// 	InjectHook(0x755BC0, _rpD3D9LightConstructor);
// 	InjectHook(0x755BF0, _rpD3D9LightDestructor);
// 	InjectHook(0x755CE0, _rpD3D9LightCopy);
// 	InjectHook(0x755D20, RpD3D9LightSetAttenuationParams);
// 	InjectHook(0x755D50, RpD3D9LightGetAttenuationParams);
// 	InjectHook(0x755D80, _rwD3D9LightsOpen);
// 	InjectHook(0x755FE0, _rwD3D9LightsClose);
// 	InjectHook(0x756070, _rwD3D9LightsGlobalEnable);
// 	InjectHook(0x756260, _rwD3D9LightDirectionalEnable);
// 	InjectHook(0x756390, _rwD3D9LightLocalEnable);
// 	InjectHook(0x756600, _rwD3D9LightsEnable);
// 	InjectHook(0x756710, _rwD3D9MeshGetNumVerticesMinIndex);
// 	InjectHook(0x756770, _rwD3D9SortTriListIndices);
// 	InjectHook(0x7567A0, SortTriangles);
// 	InjectHook(0x756830, _rwD3D9ConvertToTriList);
// 	InjectHook(0x756960, _rxD3D9Instance);
// 	InjectHook(0x756D90, _rwD3D9EnableClippingIfNeeded);
// 	InjectHook(0x756DF0, _rxD3D9DefaultRenderCallback);
 	InjectHook(0x757380, nullsub);
 	//InjectHook(0x757390, RxD3D9AllInOneGetInstanceCallBack);
 	InjectHook(0x7573A0, nullsub);
 	//InjectHook(0x7573B0, RxD3D9AllInOneGetReinstanceCallBack);
 	InjectHook(0x7573C0, nullsub);
 	//InjectHook(0x7573D0, RxD3D9AllInOneGetLightingCallBack);
 	InjectHook(0x7573E0, nullsub);
 //	InjectHook(0x7573F0, RxD3D9AllInOneGetRenderCallBack);
// 	InjectHook(0x757400, _rwD3D9AtomicDefaultLightingCallback);
// 	InjectHook(0x7575F0, D3D9AtomicAllInOneNode);
// 	InjectHook(0x757890, D3D9AtomicAllInOnePipelineInit);
// 	InjectHook(0x7578C0, D3D9AtomicDefaultInstanceCallback);
// 	InjectHook(0x758240, D3D9VertexElementSortCB);
// 	InjectHook(0x758270, D3D9AtomicDefaultReinstanceCallback);
 	InjectHook(0x7582E0, RxNodeDefinitionGetOpenGLAtomicAllInOne);
// 	InjectHook(0x7582F0, _rxD3D9VertexShaderAtomicAllInOneNode);
// 	InjectHook(0x7587D0, _rpD3D9UsageFlagPluginAttach);
// 	InjectHook(0x758850, _rpD3D9UsageFlagConst);
// 	InjectHook(0x758860, _rpD3D9UsageFlagDest);
// 	InjectHook(0x758870, _rpD3D9UsageFlagCopy);
// 	InjectHook(0x758890, _rpD3D9UsageFlagOpen);
// 	InjectHook(0x7588A0, _rpD3D9UsageFlagClose);
// 	InjectHook(0x7588B0, RpD3D9GeometrySetUsageFlags);
// 	InjectHook(0x7588D0, RpD3D9GeometryGetUsageFlags);
// 	InjectHook(0x7588E0, RpD3D9WorldSectorSetUsageFlags);
// 	InjectHook(0x758900, RpD3D9WorldSectorGetUsageFlags);
	InjectHook(0x758910, _rpMeshHeaderDestroy);
	InjectHook(0x758920, _rpMeshHeaderCreate);
	InjectHook(0x758940, _rpMeshClose);
	InjectHook(0x758970, _rpMeshOpen);
	InjectHook(0x758A90, _rpBuildMeshCreate);
	InjectHook(0x758B80, _rpBuildMeshDestroy);
	InjectHook(0x758BC0, _rpMeshDestroy);
	InjectHook(0x758C00, _rpBuildMeshAddTriangle);
	InjectHook(0x758D30, _rpMeshHeaderForAllMeshes);
	InjectHook(0x758D70, _rpMeshWrite);
	InjectHook(0x758EC0, _rpMeshRead);
	InjectHook(0x759090, _rpMeshSize);
	InjectHook(0x7590E0, _rpMeshGetNextSerialNumber);
	InjectHook(0x759100, RpBuildMeshGenerateTrivialTriStrip);
	InjectHook(0x7591B0, RpBuildMeshGenerateDefaultTriStrip);
// 	InjectHook(0x7591D0, TriStripMeshGenerate);
// 	InjectHook(0x759640, SortPolygons);
// 	InjectHook(0x759790, TriStripStripTris);
// 	InjectHook(0x75A090, TriStripBinEntryArrayCreate);
// 	InjectHook(0x75A1B0, TriStripAddEdge);
// 	InjectHook(0x75A250, TriStripMarkTriUsed);
// 	InjectHook(0x75A370, TriStripFollow);
// 	InjectHook(0x75A660, TriStripJoin);
	InjectHook(0x75A8E0, RpBuildMeshGenerateDefaultIgnoreWindingTriStrip);
	InjectHook(0x75A900, RpBuildMeshGeneratePreprocessTriStrip);
	InjectHook(0x75A920, RpBuildMeshGeneratePreprocessIgnoreWindingTriStrip);
	InjectHook(0x75A940, RpBuildMeshGenerateExhaustiveTriStrip);
	//InjectHook(0x75A960, BuildMeshGenerateExhaustiveTriStrip);
	InjectHook(0x75B4E0, RpBuildMeshGenerateExhaustiveIgnoreWindingTriStrip);
	InjectHook(0x75B500, RpTriStripDefaultCost);
	InjectHook(0x75B780, RpTriStripMeshTunnel);
	InjectHook(0x75BD80, RpTriStripMeshQuick);
	//InjectHook(0x75C290, _rpTriStripEdgeFindNext);
	InjectHook(0x75C330, RpTriStripPolygonFollowStrip);
	InjectHook(0x75C380, RpBuildMeshGenerateTriStrip);
// 	InjectHook(0x75C660, _rpTriStripMeshCreate);
// 	InjectHook(0x75C9D0, _rpTriStripMeshCreateOutput);
// 	InjectHook(0x75D240, _rpTriStripGetStrip);
	InjectHook(0x75D4C0, RpMeshSetTriStripMethod);
	InjectHook(0x75D500, RpMeshGetTriStripMethod);
	InjectHook(0x75D530, _rpTriListMeshGenerate);
//	InjectHook(0x75D730, SortPolygonsInTriListMesh);
	InjectHook(0x75D970, _rpMeshOptimise);
	InjectHook(0x75D9D0, RpGeometryIsCorrectlySorted);
	InjectHook(0x75DAE0, RpGeometrySortByMaterial);
//	InjectHook(0x75E230, SortVertsByMaterialCB);
	InjectHook(0x75E270, RpWorldSetDefaultSectorPipeline);
	InjectHook(0x75E2A0, RpAtomicSetDefaultPipeline);
	InjectHook(0x75E2D0, RpMaterialSetDefaultPipeline);
// 	InjectHook(0x75E300, _rpWorldPipelineClose);
// 	InjectHook(0x75E310, _rpWorldPipelineOpen);
// 	InjectHook(0x75E3E0, _rpWorldPipeAttach);
// 	InjectHook(0x75E3F0, D3D9WorldSectorAllInOneNode);
// 	InjectHook(0x75E540, D3D9WorldSectorAllInOnePipelineInit);
// 	InjectHook(0x75E570, D3D9WorldSectorDefaultLightingCallback);
// 	InjectHook(0x75E5E0, D3D9WorldSectorDefaultInstanceCallback);
// 	InjectHook(0x75E9F0, RxNodeDefinitionGetD3D9WorldSectorAllInOne);
// 	InjectHook(0x75EA00, _rxD3D9VertexShaderWorldSectorAllInOneNode);
// 	InjectHook(0x75EDD0, _rpD3D9GetNumConstantsUsed);
// 	InjectHook(0x75EE60, _rpD3D9VertexShaderCachePurge);
// 	InjectHook(0x75EED0, _rpD3D9GetVertexShader);
// 	InjectHook(0x75F0B0, _rpD3D9GenerateVertexShader);
// 	InjectHook(0x760CF0, _rpD3D9VertexShaderCacheOpen);
// 	InjectHook(0x760DF0, _rxD3D9VertexShaderDefaultBeginCallBack);
// 	InjectHook(0x761000, _rxD3D9VertexShaderDefaultEndCallBack);
// 	InjectHook(0x761010, _rxD3D9VertexShaderDefaultGetMaterialShaderCallBack);
// 	InjectHook(0x761030, _rxD3D9VertexShaderDefaultMeshRenderCallBack);
// 	InjectHook(0x761170, _rxD3D9VertexShaderDefaultLightingCallBack);
// 	InjectHook(0x761720, _rpD3D9VertexShaderUpdateLightsColors);
// 	InjectHook(0x761820, _rpD3D9VertexShaderUpdateMaterialColor);
// 	InjectHook(0x7618B0, _rpD3D9VertexShaderUpdateFogData);
// 	InjectHook(0x761950, _rpD3D9VertexShaderUpdateMorphingCoef);
// 	InjectHook(0x7619A0, _rpD3DVertexShaderSetUVAnimMatrix);
// 	InjectHook(0x761A10, _rpD3D9VertexShaderSetEnvMatrix);
// 	InjectHook(0x761B70, _rpD3D9VertexShaderSetBumpMatrix);
// 	InjectHook(0x761C30, _rpSectorOpen);
// 	InjectHook(0x761C40, _rpSectorClose);
	InjectHook(0x761C50, RpWorldSectorRender);
	InjectHook(0x761C60, RpWorldSectorForAllMeshes);
	InjectHook(0x761C90, RpWorldSectorRegisterPlugin);
	InjectHook(0x761CC0, RpWorldSectorRegisterPluginStream);
	InjectHook(0x761CF0, RpWorldSectorSetStreamAlwaysCallBack);
	InjectHook(0x761D10, RpWorldSectorSetStreamRightsCallBack);
	InjectHook(0x761D30, RpWorldSectorGetPluginOffset);
	InjectHook(0x761D50, RpWorldSectorValidatePlugins);
// 	InjectHook(0x761D60, _rpReadWorldRights);
// 	InjectHook(0x761DA0, _rpWriteWorldRights);
// 	InjectHook(0x761DE0, _rpSizeWorldRights);
// 	InjectHook(0x761E00, _rpReadSectRights);
// 	InjectHook(0x761E40, _rpWriteSectRights);
// 	InjectHook(0x761E80, _rpSizeSectRights);
	InjectHook(0x761EA0, RpWorldStreamGetSize);
	/*InjectHook(0x762040, PlaneSectorStreamGetSize);*/
	InjectHook(0x762150, RpWorldStreamWrite);
// 	InjectHook(0x7624A0, WorldSectorStreamWrite);
// 	InjectHook(0x762740, PlaneSectorStreamWrite);
	InjectHook(0x762960, RpWorldStreamRead);
// 	InjectHook(0x762F30, WorldSectorStreamRead);
// 	InjectHook(0x763310, PlaneSectorStreamRead);
	InjectHook(0x7635B0, _rpWorldSectorChunkInfoRead);
	InjectHook(0x763620, _rpPlaneSectorChunkInfoRead);
	InjectHook(0x763690, _rpWorldChunkInfoRead);
// 	InjectHook(0x7637D0, _rpBinaryWorldClose);
// 	InjectHook(0x7637E0, _rpBinaryWorldOpen);
// 	InjectHook(0x7637F0, _rpGeometryNativeWrite);
// 	InjectHook(0x763A80, _rpGeometryNativeRead);
// 	InjectHook(0x763EB0, _rpGeometryNativeSize);
// 	InjectHook(0x763F20, _rpWorldSectorNativeWrite);
// 	InjectHook(0x7641C0, _rpWorldSectorNativeRead);
// 	InjectHook(0x7645C0, _rpWorldSectorNativeSize);
// 	InjectHook(0x764650, _rwD3D9VSSetActiveWorldMatrix);
// 	InjectHook(0x7646E0, _rwD3D9VSGetComposedTransformMatrix);
// 	InjectHook(0x764730, _rwD3D9VSGetWorldViewTransposedMatrix);
// 	InjectHook(0x764760, _rwD3D9VSGetWorldViewMatrix);
// 	InjectHook(0x7647B0, _rwD3D9VSGetInverseWorldMatrix);
// 	InjectHook(0x764890, D3D9MatrixInvertOrthoNormalized);
// 	InjectHook(0x764920, _rwD3D9VSGetWorldMultiplyMatrix);
// 	InjectHook(0x764960, _rwD3D9VSGetWorldMultiplyTransposeMatrix);
// 	InjectHook(0x7649D0, _rwD3D9VSGetWorldViewMultiplyTransposeMatrix);
// 	InjectHook(0x764A70, _rwD3D9VSGetWorldNormalizedMultiplyTransposeMatrix);
// 	InjectHook(0x764B50, _rwD3D9VSGetWorldNormalizedViewMultiplyTransposeMatrix);
// 	InjectHook(0x764C60, _rwD3D9VSGetWorldNormalizedTransposeMatrix);
// 	InjectHook(0x764D20, _rwD3D9VSGetProjectionTransposedMatrix);
// 	InjectHook(0x764D30, _rwD3D9VSGetNormalInLocalSpace);
// 	InjectHook(0x764E70, _rwD3D9VSGetPointInLocalSpace);
// 	InjectHook(0x764F60, _rwD3D9VSGetRadiusInLocalSpace);
	InjectHook(0x7C45E0, RpHAnimHierarchySetFreeListCreateParams);
	InjectHook(0x7C4600, RpHAnimPluginAttach);
// 	InjectHook(0x7C4670, HAnimOpen);
// 	InjectHook(0x7C4720, HAnimClose);
// 	InjectHook(0x7C4750, HAnimConstructor);
// 	InjectHook(0x7C4770, HAnimDestructor);
// 	InjectHook(0x7C4830, HAnimCopy);
// 	InjectHook(0x7C48D0, HAnimWrite);
// 	InjectHook(0x7C4A00, HAnimRead);
// 	InjectHook(0x7C4BF0, HAnimSize);
	InjectHook(0x7C4C30, RpHAnimHierarchyCreate);
	InjectHook(0x7C4D30, RpHAnimHierarchyDestroy);
	InjectHook(0x7C4DB0, RpHAnimHierarchyCreateSubHierarchy);
	InjectHook(0x7C4ED0, RpHAnimHierarchyCreateFromHierarchy);
	InjectHook(0x7C4F40, RpHAnimHierarchyAttach);
	//InjectHook(0x7C4F90, HAnimFramesLinkToHierarchy);
	InjectHook(0x7C4FF0, RpHAnimHierarchyDetach);
	InjectHook(0x7C5020, RpHAnimHierarchyAttachFrameIndex);
	//InjectHook(0x7C50A0, HAnimFrameLinkToHierarchy);
	InjectHook(0x7C5100, RpHAnimHierarchyDetachFrameIndex);
	InjectHook(0x7C5120, RpHAnimHierarchyGetMatrixArray);
	InjectHook(0x7C5130, RpHAnimFrameSetHierarchy);
	InjectHook(0x7C5160, RpHAnimFrameGetHierarchy);
	InjectHook(0x7C5170, RpHAnimFrameSetID);
	InjectHook(0x7C5190, RpHAnimFrameGetID);
	InjectHook(0x7C51A0, RpHAnimIDGetIndex);
	InjectHook(0x7C51D0, RpHAnimHierarchyUpdateMatrices);
	InjectHook(0x7C5B80, RpHAnimKeyFrameApply);
	InjectHook(0x7C5CA0, RpHAnimKeyFrameInterpolate);
	InjectHook(0x7C60C0, RpHAnimKeyFrameBlend);
	InjectHook(0x7C64C0, RpHAnimKeyFrameStreamRead);
	InjectHook(0x7C6540, RpHAnimKeyFrameStreamWrite);
	InjectHook(0x7C65B0, RpHAnimKeyFrameStreamGetSize);
	InjectHook(0x7C65C0, RpHAnimKeyFrameMulRecip);
	InjectHook(0x7C6720, RpHAnimKeyFrameAdd);
	InjectHook(0x7C67F0, RpSkinSetFreeListCreateParams);
	//InjectHook(0x7C6810, _rpSkinGetAlignedMatrixCache);
	InjectHook(0x7C6820, RpSkinPluginAttach);
// 	InjectHook(0x7C68E0, SkinOpen);
// 	InjectHook(0x7C69A0, SkinClose);
// 	InjectHook(0x7C69F0, SkinGeometryConstructor);
// 	InjectHook(0x7C6A10, SkinGeometryDestructor);
// 	InjectHook(0x7C6A70, SkinGeometryCopy);
// 	InjectHook(0x7C6A80, SkinAtomicConstructor);
// 	InjectHook(0x7C6AA0, SkinAtomicDestructor);
// 	InjectHook(0x7C6AC0, SkinAtomicCopy);
// 	InjectHook(0x7C6AE0, SkinAtomicAlways);
// 	InjectHook(0x7C6B70, SkinAtomicRights);
// 	InjectHook(0x7C6BE0, SkinGeometrySize);
// 	InjectHook(0x7C6C30, SkinGeometryWrite);
// 	InjectHook(0x7C6D20, SkinGeometryRead);
// 	InjectHook(0x7C7190, SkinCreate);
// 	InjectHook(0x7C72A0, SkinAtomicRead);
// 	InjectHook(0x7C7500, SkinAtomicWrite);
// 	InjectHook(0x7C7510, SkinAtomicGetSize);
	InjectHook(0x7C7520, RpSkinAtomicSetHAnimHierarchy);
	InjectHook(0x7C7540, RpSkinAtomicGetHAnimHierarchy);
	InjectHook(0x7C7550, RpSkinGeometryGetSkin);
	InjectHook(0x7C7560, RpSkinGeometrySetSkin);
	InjectHook(0x7C75B0, RpSkinCreate);
	InjectHook(0x7C77A0, RpSkinDestroy);
	InjectHook(0x7C77E0, RpSkinGetNumBones);
	InjectHook(0x7C77F0, RpSkinGetVertexBoneWeights);
	InjectHook(0x7C7800, RpSkinGetVertexBoneIndices);
	InjectHook(0x7C7810, RpSkinGetSkinToBoneMatrices);
	InjectHook(0x7C7820, RpSkinIsSplit);
	InjectHook(0x7C7830, RpSkinAtomicSetType);
	InjectHook(0x7C7880, RpSkinAtomicGetType);
// 	InjectHook(0x7C78A0, _rpD3D9SkinVertexShaderMatrixUpdate);
// 	InjectHook(0x7C7B90, _rwSkinD3D9AtomicAllInOneNode);
// 	InjectHook(0x7C8060, _rpD3D9SkinVertexShaderAtomicRender);
// 	InjectHook(0x7C85B0, _rpD3D9SkinFFPAtomicRender);
// 	InjectHook(0x7C86F0, _rpSkinPipelinesDestroy);
// 	InjectHook(0x7C8720, BonesUsedSort);
	InjectHook(0x7C8740, _rpSkinInitialize);
	InjectHook(0x7C8820, _rpSkinDeinitialize);
// 	InjectHook(0x7C8830, _rpSkinGeometryNativeSize);
// 	InjectHook(0x7C8840, _rpSkinGeometryNativeWrite);
// 	InjectHook(0x7C88B0, _rpSkinGeometryNativeRead);
// 	InjectHook(0x7C8980, _rpSkinPipelinesCreate);
// 	InjectHook(0x7C89B0, _rpSkinPipelinesAttach);
// 	InjectHook(0x7C89C0, _rwD3D9SkinNeedsAManagedVertexBuffer);
// 	InjectHook(0x7C8A00, _rwD3D9SkinUseVertexShader);
	InjectHook(0x7C8A40, _rpSkinGetMeshBoneRemapIndices);
	InjectHook(0x7C8A50, _rpSkinGetMeshBoneRLECount);
	InjectHook(0x7C8A60, _rpSkinGetMeshBoneRLE);
	InjectHook(0x7C8A70, _rpSkinSplitDataCreate);
	InjectHook(0x7C8B10, _rpSkinSplitDataDestroy);
// 	InjectHook(0x7C8B50, _rpSkinSplitDataStreamWrite);
// 	InjectHook(0x7C8BE0, _rpSkinSplitDataStreamRead);
// 	InjectHook(0x7C8D40, _rpSkinSplitDataStreamGetSize);
// 	InjectHook(0x7C8D60, _rxD3D9SkinInstance);
// 	InjectHook(0x7C90D0, _rpD3D9SkinGeometryReinstance);
// 	InjectHook(0x7C9680, _rpD3D9SkinAtomicCreateVertexBuffer);
// 	InjectHook(0x7C9DA0, _rpD3D9SkinGenericMatrixBlend);
// 	InjectHook(0x7CA240, _rpD3D9SkinGenericMatrixBlend1Weight);
// 	InjectHook(0x7CA330, _rpD3D9SkinGenericMatrixBlend1Matrix);
// 	InjectHook(0x7CA410, _rpD3D9SkinGenericMatrixBlend2Weights);
// 	InjectHook(0x7CA6C0, _rwD3D9MatrixMultiplyTranspose);
// 	InjectHook(0x7CA800, _rwD3D9MatrixCopyTranspose);
// 	InjectHook(0x7CA850, _rwD3D9SkinPrepareMatrixTransposed);
// 	InjectHook(0x7CAA30, _rwD3D9SkinPrepareMatrix);
// 	InjectHook(0x7CAAD0, _rpSkinIntelSSEMatrixBlend1Matrix);
// 	InjectHook(0x7CAB80, _rpSkinIntelSSEMatrixBlend1Weight);
// 	InjectHook(0x7CAC60, _rpSkinIntelSSEMatrixBlend);
// 	InjectHook(0x7CAF50, _rpSkinIntelSSEMatrixBlend2Weights);
// 	InjectHook(0x7CB190, _rpD3D9SkinVertexShaderBeginCallBack);
// 	InjectHook(0x7CB240, _rxSkinD3D9AtomicAllInOnePipelineInit);
// 	InjectHook(0x7CB2A0, RxNodeDefinitionGetD3D9SkinAtomicAllInOne);
// 	InjectHook(0x7CB2B0, _rpSkinD3D9CreatePlainPipe);
	InjectHook(0x7CB940, RpUVAnimPluginAttach);
// 	InjectHook(0x7CB9B0, UVAnimOpen);
// 	InjectHook(0x7CBA20, UVAnimClose);
	InjectHook(0x7CBA60, RpUVAnimSetFreeListCreateParams);
// 	InjectHook(0x7CBA80, UVAnimConstructor);
// 	InjectHook(0x7CBAA0, UVAnimDestructor);
// 	InjectHook(0x7CBB30, UVAnimCopy);
// 	InjectHook(0x7CBB70, UVAnimWrite);
// 	InjectHook(0x7CBC20, UVAnimRead);
// 	InjectHook(0x7CBD80, _rpUVAnimCreateConsistantMatrices);
// 	InjectHook(0x7CBE00, UVAnimRead_internal);
// 	InjectHook(0x7CBF20, UVAnimSize);
	InjectHook(0x7CBF70, _rpUVAnimCustomDataStreamRead);
	InjectHook(0x7CBFD0, _rpUVAnimCustomDataStreamWrite);
	InjectHook(0x7CC010, _rpUVAnimCustomDataStreamGetSize);
	InjectHook(0x7CC020, RpUVAnimCreate);
	InjectHook(0x7CC0C0, RpUVAnimDestroy);
	InjectHook(0x7CC100, RpUVAnimAddRef);
	InjectHook(0x7CC110, RpMaterialUVAnimApplyUpdate);
	InjectHook(0x7CC3A0, RpUVAnimGetName);
	InjectHook(0x7CC3B0, RpMaterialSetUVAnim);
	InjectHook(0x7CC430, RpMaterialUVAnimGetInterpolator);
	InjectHook(0x7CC450, RpMaterialUVAnimSetInterpolator);
	InjectHook(0x7CC470, RpMaterialUVAnimSetCurrentTime);
	InjectHook(0x7CC4B0, RpMaterialUVAnimAddAnimTime);
	InjectHook(0x7CC4F0, RpMaterialUVAnimSubAnimTime);
	InjectHook(0x7CC530, RpMaterialUVAnimExists);
	InjectHook(0x7CC560, RpUVAnimParamKeyFrameApply);
	InjectHook(0x7CC600, RpUVAnimParamKeyFrameInterpolate);
	InjectHook(0x7CC6A0, RpUVAnimParamKeyFrameBlend);
	InjectHook(0x7CC740, RpUVAnimParamKeyFrameMulRecip);
	InjectHook(0x7CC750, RpUVAnimParamKeyFrameAdd);
	InjectHook(0x7CC870, RpUVAnimKeyFrameStreamRead);
	InjectHook(0x7CC920, RpUVAnimKeyFrameStreamWrite);
	InjectHook(0x7CC9D0, RpUVAnimKeyFrameStreamGetSize);
	InjectHook(0x7CC9F0, RpUVAnimLinearKeyFrameApply);
	InjectHook(0x7CCA40, RpUVAnimLinearKeyFrameInterpolate);
	InjectHook(0x7CCAC0, RpUVAnimLinearKeyFrameBlend);
	InjectHook(0x7CCB30, RpUVAnimLinearKeyFrameMulRecip);
	InjectHook(0x7CCBE0, RpUVAnimLinearKeyFrameAdd);
	InjectHook(0x7CCC80, RtAnimAnimationFreeListCreateParams);
	InjectHook(0x7CCCA0, RtAnimInitialize);
// 	InjectHook(0x7CCCD0, AnimOpen);
// 	InjectHook(0x7CCD10, AnimClose);
	InjectHook(0x7CCD40, RtAnimRegisterInterpolationScheme);
	InjectHook(0x7CCDE0, RtAnimGetInterpolatorInfo);
	InjectHook(0x7CCE40, RtAnimAnimationCreate);
	InjectHook(0x7CCF10, RtAnimAnimationDestroy);
	InjectHook(0x7CCF30, RtAnimAnimationRead);
	InjectHook(0x7CD160, RtAnimAnimationWrite);
	InjectHook(0x7CD220, RtAnimAnimationStreamRead);
	InjectHook(0x7CD410, RtAnimAnimationStreamWrite);
	InjectHook(0x7CD4D0, RtAnimAnimationStreamGetSize);
	InjectHook(0x7CD4F0, RtAnimAnimationGetNumNodes);
	InjectHook(0x7CD520, RtAnimInterpolatorCreate);
	InjectHook(0x7CD590, RtAnimInterpolatorDestroy);
	InjectHook(0x7CD5A0, RtAnimInterpolatorSetCurrentAnim);
	InjectHook(0x7CD660, RtAnimInterpolatorSetKeyFrameCallBacks);
	InjectHook(0x7CD6F0, RtAnimInterpolatorSetAnimLoopCallBack);
	InjectHook(0x7CD710, RtAnimInterpolatorSetAnimCallBack);
	InjectHook(0x7CD730, RtAnimInterpolatorCopy);
	InjectHook(0x7CD760, RtAnimInterpolatorSubAnimTime);
	InjectHook(0x7CD8D0, RtAnimInterpolatorAddAnimTime);
	InjectHook(0x7CDAB0, RtAnimInterpolatorSetCurrentTime);
	InjectHook(0x7CDB00, RtAnimAnimationMakeDelta);
	InjectHook(0x7CDBF0, RtAnimInterpolatorBlend);
	InjectHook(0x7CDC50, RtAnimInterpolatorAddTogether);
	InjectHook(0x7CDCB0, RtAnimInterpolatorCreateSubInterpolator);
	InjectHook(0x7CDCF0, RtAnimInterpolatorBlendSubInterpolator);
	InjectHook(0x7CDEF0, RtAnimInterpolatorAddSubInterpolator);
// 	InjectHook(0x7CDF60, RtBMPImageRead);
// 	InjectHook(0x7CE5D0, BMPImageSetSpan);
// 	InjectHook(0x7CE990, RtBMPImageWrite);
// 	InjectHook(0x7CEC40, BMPWriteBits ? );
	InjectHook(0x7CED40, RtDictSchemaInit);
	InjectHook(0x7CED70, RtDictSchemaDestruct);
	InjectHook(0x7CED90, RtDictSchemaCreateDict);
	InjectHook(0x7CEE50, RtDictSchemaAddDict);
	InjectHook(0x7CEE80, RtDictSchemaRemoveDict);
	InjectHook(0x7CEEE0, RtDictSchemaGetCurrentDict);
	InjectHook(0x7CEEF0, RtDictSchemaSetCurrentDict);
	InjectHook(0x7CEF00, _rtDictSchemaInitDict);
	InjectHook(0x7CEF60, _rtDictDestruct);
	InjectHook(0x7CEFB0, RtDictAddEntry);
	InjectHook(0x7CEFE0, RtDictFindNamedEntry);
	InjectHook(0x7CF060, RtDictForAllEntries);
	InjectHook(0x7CF0C0, RtDictRemoveEntry);
	InjectHook(0x7CF130, RtDictDestroy);
	InjectHook(0x7CF1F0, RtDictStreamGetSize);
	InjectHook(0x7CF240, RtDictSchemaStreamReadDict);
	InjectHook(0x7CF490, RtDictStreamWrite);
	InjectHook(0x7CF5B0, RtDictSchemaForAllDictionaries);
// 	InjectHook(0x7CF9A0, ullsub_72);
// 	InjectHook(0x7D11E0, sub_7D11E0);
// 	InjectHook(0x7D4EC3, _CxxQueryExceptionSize);
// 	InjectHook(0x7D4F72, _RTC_NumErrors);
// 	InjectHook(0x7D5022, _RTC_NumErrors_0);
	InjectHook(0x7EB5C0, RtQuatConvertFromMatrix);
// 	InjectHook(0x7EB6A0, QuatFromXDiagDomMatrix);
// 	InjectHook(0x7EB700, QuatFromYDiagDomMatrix);
// 	InjectHook(0x7EB760, QuatFromZDiagDomMatrix);
	InjectHook(0x7EB7C0, RtQuatRotate);
	InjectHook(0x7EBA80, RtQuatQueryRotate);
	InjectHook(0x7EBBB0, RtQuatTransformVectors);
	InjectHook(0x7EBD10, RtQuatModulus);
	InjectHook(0x7EC760, RwStreamSetFreeListCreateParams);
// 	InjectHook(0x7EC780, _rwStreamModuleOpen);
// 	InjectHook(0x7EC7E0, _rwStreamModuleClose);
	InjectHook(0x7EC810, _rwStreamInitialize);
	InjectHook(0x7EC9D0, RwStreamRead);
	InjectHook(0x7ECB30, RwStreamWrite);
	InjectHook(0x7ECD00, RwStreamSkip);
	InjectHook(0x7ECE20, RwStreamClose);
	InjectHook(0x7ECEF0, RwStreamOpen);
// 	InjectHook(0x7ED0F0, _rwStreamReadChunkHeader);
// 	InjectHook(0x7ED1E0, ChunkIsComplex);
	InjectHook(0x7ED270, _rwStreamWriteVersionedChunkHeader);
	InjectHook(0x7ED2D0, RwStreamFindChunk);
	InjectHook(0x7ED3D0, RwStreamWriteReal);
	InjectHook(0x7ED460, RwStreamWriteInt32);
	InjectHook(0x7ED480, RwStreamWriteInt16);
	InjectHook(0x7ED4A0, RwStreamReadInt16);
	InjectHook(0x7ED4F0, RwStreamReadReal);
	InjectHook(0x7ED540, RwStreamReadInt32);
	InjectHook(0x7ED590, RwStreamReadChunkHeaderInfo);
// 	InjectHook(0x7ED5F0, _rwVectorSetMultFn);
// 	InjectHook(0x7ED670, VectorMultPoints);
// 	InjectHook(0x7ED730, VectorMultPoint);
// 	InjectHook(0x7ED7D0, VectorMultVectors);
// 	InjectHook(0x7ED880, VectorMultVector);
	InjectHook(0x7ED910, _rwV3dNormalize);
	InjectHook(0x7ED9B0, RwV3dNormalize);
	InjectHook(0x7EDAC0, RwV3dLength);
	InjectHook(0x7EDB30, _rwSqrt);
	InjectHook(0x7EDB90, _rwInvSqrt);
	InjectHook(0x7EDBF0, RwV2dLength);
	InjectHook(0x7EDC60, RwV2dNormalize);
	InjectHook(0x7EDD60, RwV3dTransformPoint);
	InjectHook(0x7EDD90, RwV3dTransformPoints);
	InjectHook(0x7EDDC0, RwV3dTransformVector);
	InjectHook(0x7EDDF0, RwV3dTransformVectors);
// 	InjectHook(0x7EDE20, _rwVectorClose);
// 	InjectHook(0x7EDE90, _rwVectorOpen);
// 	InjectHook(0x7EE0B0, _rwCameraClose);
	InjectHook(0x7EE0F0, RwCameraSetFreeListCreateParams);
	//InjectHook(0x7EE110, _rwCameraOpen);
	InjectHook(0x7EE180, RwCameraEndUpdate);
	InjectHook(0x7EE190, RwCameraBeginUpdate);
	InjectHook(0x7EE1A0, RwCameraSetViewOffset);
	InjectHook(0x7EE1D0, RwCameraSetNearClipPlane);
	//InjectHook(0x7EE200, CameraUpdateZShiftScale);
	InjectHook(0x7EE2A0, RwCameraSetFarClipPlane);
	InjectHook(0x7EE2D0, RwCameraFrustumTestSphere);
	InjectHook(0x7EE340, RwCameraClear);
	InjectHook(0x7EE370, RwCameraShowRaster);
	InjectHook(0x7EE3A0, RwCameraSetProjection);
	InjectHook(0x7EE410, RwCameraSetViewWindow);
	InjectHook(0x7EE450, RwCameraRegisterPlugin);
	InjectHook(0x7EE480, RwCameraGetPluginOffset);
	InjectHook(0x7EE4A0, RwCameraValidatePlugins);
	InjectHook(0x7EE4B0, RwCameraDestroy);
	InjectHook(0x7EE4F0, RwCameraCreate);
// 	InjectHook(0x7EE5A0, CameraSync);
// 	InjectHook(0x7EE740, CameraBuildPerspClipPlanes);
// 	InjectHook(0x7EEDE0, CameraBuildParallelClipPlanes);
// 	InjectHook(0x7EF1F0, CameraBuildParallelViewMatrix);
// 	InjectHook(0x7EF340, CameraEndUpdate);
// 	InjectHook(0x7EF370, CameraBeginUpdate);
	InjectHook(0x7EF3B0, RwCameraClone);
	InjectHook(0x7EF450, RwIm3DTransformWrap);
	InjectHook(0x7EF520, RwIm3DEnd);
	InjectHook(0x7EF550, RwIm3DRenderIndexedPrimitiveWrap);
	InjectHook(0x7EF6B0, RwIm3DRenderPrimitive);
	InjectHook(0x7EF810, RwIm3DRenderTriangle);
	InjectHook(0x7EF900, RwIm3DRenderLine);
	InjectHook(0x7EF9D0, RwIm3DGetTransformPipeline);
	InjectHook(0x7EF9E0, RwIm3DGetRenderPipeline);
	InjectHook(0x7EFAC0, RwIm3DSetTransformPipeline);
	InjectHook(0x7EFB10, RwIm3DSetRenderPipeline);
	InjectHook(0x7EFDD0, _rwIm3DGetPool);
// 	InjectHook(0x7EFDE0, _rwIm3DClose);
// 	InjectHook(0x7EFE20, _rwIm3DOpen);
	InjectHook(0x7EFED0, RwFrameSetFreeListCreateParams);
// 	InjectHook(0x7EFEF0, _rwFrameOpen);
// 	InjectHook(0x7EFF70, _rwFrameClose);
	InjectHook(0x7EFFB0, _rwFrameCloneAndLinkClones);
/*	InjectHook(0x7F0050, rwFrameCloneRecurse);*/
	InjectHook(0x7F01A0, _rwFramePurgeClone);
	/*InjectHook(0x7F0210, rwSetHierarchyRoot);*/
	InjectHook(0x7F0250, RwFrameCloneHierarchy);
	InjectHook(0x7F0340, RwFrameDirty);
	//InjectHook(0x7F0360, rwFrameInternalInit);
	InjectHook(0x7F0410, RwFrameCreate);
	InjectHook(0x7F0450, _rwFrameInit);
	//InjectHook(0x7F0470, _rwFrameInternalDeInit);
	InjectHook(0x7F05A0, RwFrameDestroy);
	InjectHook(0x7F06F0, _rwFrameDeInit);
	//InjectHook(0x7F0830, rwFrameDestroyRecurse);
	InjectHook(0x7F08A0, RwFrameDestroyHierarchy);
	//InjectHook(0x7F0910, RwFrameUpdateObject);
	InjectHook(0x7F0990, RwFrameGetLTM);
	InjectHook(0x7F09B0, RwFrameGetRoot);
	InjectHook(0x7F09C0, RwFrameAddChildNoUpdate);
	InjectHook(0x7F0B00, RwFrameAddChild);
	InjectHook(0x7F0CD0, RwFrameRemoveChild);
	InjectHook(0x7F0DC0, RwFrameForAllChildren);
	InjectHook(0x7F0E00, RwFrameCount);
	InjectHook(0x7F0E30, RwFrameTranslate);
	InjectHook(0x7F0ED0, RwFrameScale);
	InjectHook(0x7F0F70, RwFrameTransform);
	InjectHook(0x7F1010, RwFrameRotate);
	InjectHook(0x7F10B0, RwFrameSetIdentity);
	InjectHook(0x7F1170, RwFrameOrthoNormalize);
	InjectHook(0x7F1200, RwFrameForAllObjects);
	InjectHook(0x7F1240, _rwFrameSetStaticPluginsSize);
	InjectHook(0x7F1260, RwFrameRegisterPlugin);
	InjectHook(0x7F1290, RwFrameGetPluginOffset);
	InjectHook(0x7F12B0, RwFrameValidatePlugins);
// 	InjectHook(0x7F12C0, _rwMatrixSetMultFn);
// 	InjectHook(0x7F12F0, MatrixMultiply);
// 	InjectHook(0x7F1430, _rwMatrixSetOptimizations);
	InjectHook(0x7F1450, _rwMatrixDeterminant);
	InjectHook(0x7F14A0, _rwMatrixOrthogonalError);
	InjectHook(0x7F1500, _rwMatrixNormalError);
	InjectHook(0x7F1590, _rwMatrixIdentityError);
	//InjectHook(0x7F1660, _rwMatrixClose);
	InjectHook(0x7F16A0, RwMatrixSetFreeListCreateParams);
	//InjectHook(0x7F16C0, _rwMatrixOpen);
	InjectHook(0x7F1780, RwEngineGetMatrixTolerances);
	InjectHook(0x7F17B0, RwEngineSetMatrixTolerances);
	InjectHook(0x7F17E0, RwMatrixOptimize);
	InjectHook(0x7F18A0, RwMatrixUpdate);
	InjectHook(0x7F18B0, RwMatrixMultiply);
	InjectHook(0x7F1920, RwMatrixOrthoNormalize);
	InjectHook(0x7F1D00, RwMatrixRotateOneMinusCosineSine);
	InjectHook(0x7F1FD0, RwMatrixRotate);
	InjectHook(0x7F2070, RwMatrixInvert);
	//InjectHook(0x7F2160, MatrixInvertGeneric);
	InjectHook(0x7F22C0, RwMatrixScale);
	InjectHook(0x7F2450, RwMatrixTranslate);
	InjectHook(0x7F25A0, RwMatrixTransform);
	InjectHook(0x7F2720, RwMatrixQueryRotate);
	//InjectHook(0x7F2930, MatrixQueryRotateDegenerateUnitAxis);
	InjectHook(0x7F2A20, RwMatrixDestroy);
	InjectHook(0x7F2A50, RwMatrixCreate);
	//InjectHook(0x7F2AB0, _rwDeviceSystemRequest);
	//InjectHook(0x7F2B90, _rwGetNumEngineInstances);
	InjectHook(0x7F2BA0, RwEngineGetVersion);
	InjectHook(0x7F2BB0, RwEngineRegisterPlugin);
	InjectHook(0x7F2BE0, RwEngineGetPluginOffset);
	InjectHook(0x7F2C00, RwEngineGetNumSubSystems);
	InjectHook(0x7F2C30, RwEngineGetSubSystemInfo);
	InjectHook(0x7F2C60, RwEngineGetCurrentSubSystem);
	InjectHook(0x7F2C90, RwEngineSetSubSystem);
	InjectHook(0x7F2CC0, RwEngineGetNumVideoModes);
	InjectHook(0x7F2CF0, RwEngineGetVideoModeInfo);
	InjectHook(0x7F2D20, RwEngineGetCurrentVideoMode);
	InjectHook(0x7F2D50, RwEngineSetVideoMode);
	InjectHook(0x7F2D80, RwEngineGetTextureMemorySize);
	InjectHook(0x7F2DB0, RwEngineGetMaxTextureSize);
	InjectHook(0x7F2DE0, RwEngineSetFocus);
	InjectHook(0x7F2E10, RwEngineGetMetrics);
	InjectHook(0x7F2E20, RwEngineStop);
	InjectHook(0x7F2E70, RwEngineStart);
	InjectHook(0x7F2F00, RwEngineClose);
	InjectHook(0x7F2F70, RwEngineOpenWrapper);
	InjectHook(0x7F3130, RwEngineTerm);
	InjectHook(0x7F3170, RwEngineInit);
// 	InjectHook(0x7F3490, MallocWrapper);
// 	InjectHook(0x7F34B0, FreeWrapper);
	InjectHook(0x7F34D0, RwTextureSetFindCallBack);
	InjectHook(0x7F34F0, RwTextureGetFindCallBack);
	InjectHook(0x7F3500, RwTextureSetReadCallBack);
	InjectHook(0x7F3520, RwTextureGetReadCallBack);
	InjectHook(0x7F3530, RwTextureSetMipmapping);
	InjectHook(0x7F3550, RwTextureGetMipmapping);
	InjectHook(0x7F3560, RwTextureSetAutoMipmapping);
	InjectHook(0x7F3580, RwTextureGetAutoMipmapping);
	InjectHook(0x7F3590, _rwTextureSetAutoMipMapState);
	InjectHook(0x7F35C0, _rwTextureGetAutoMipMapState);
	InjectHook(0x7F35D0, RwTextureSetRaster);
	InjectHook(0x7F3600, RwTexDictionaryCreate);
	InjectHook(0x7F36A0, RwTexDictionaryDestroy);
	InjectHook(0x7F3730, RwTexDictionaryForAllTextures);
	InjectHook(0x7F3770, RwTexDictionaryForAllTexDictionaries);
	InjectHook(0x7F37C0, RwTextureCreate);
	InjectHook(0x7F3820, RwTextureDestroy);
	InjectHook(0x7F38A0, RwTextureSetName);
	InjectHook(0x7F3910, RwTextureSetMaskName);
	InjectHook(0x7F3980, RwTexDictionaryAddTexture);
	InjectHook(0x7F39C0, RwTexDictionaryRemoveTexture);
	InjectHook(0x7F39F0, RwTexDictionaryFindNamedTexture);
	InjectHook(0x7F3A70, RwTexDictionarySetCurrent);
	InjectHook(0x7F3A90, RwTexDictionaryGetCurrent);
	InjectHook(0x7F3AA0, RwTextureGenerateMipmapName);
	InjectHook(0x7F3AC0, RwTextureRead);
	InjectHook(0x7F3BB0, RwTextureRegisterPlugin);
	InjectHook(0x7F3BE0, RwTextureGetPluginOffset);
	InjectHook(0x7F3C00, RwTextureValidatePlugins);
	InjectHook(0x7F3C10, RwTexDictionaryRegisterPlugin);
	InjectHook(0x7F3C40, RwTexDictionaryGetPluginOffset);
	InjectHook(0x7F3C60, RwTexDictionaryValidatePlugins);
	InjectHook(0x7F3C70, RwTextureSetMipmapGenerationCallBack);
	InjectHook(0x7F3C90, RwTextureGetMipmapGenerationCallBack);
	InjectHook(0x7F3CA0, RwTextureSetMipmapNameCallBack);
	InjectHook(0x7F3CC0, RwTextureGetMipmapNameCallBack);
	InjectHook(0x7F3CD0, RwTextureRasterGenerateMipmaps);
	//InjectHook(0x7F3D00, _rwTextureClose);
	InjectHook(0x7F3E60, RwTextureSetFreeListCreateParams);
	InjectHook(0x7F3E80, RwTexDictionarySetFreeListCreateParams);
// 	InjectHook(0x7F3EA0, _rwTextureOpen);
// 	InjectHook(0x7F4080, TextureDefaultMipmapName);
// 	InjectHook(0x7F40F0, TextureDefaultRead);
// 	InjectHook(0x7F44C0, PalettizeMipmaps);
// 	InjectHook(0x7F4690, TextureImageReadAndSize);
// 	InjectHook(0x7F4AE0, TextureDefaultMipmapRead);
// 	InjectHook(0x7F5140, TextureRasterDefaultBuildMipmaps);
// 	InjectHook(0x7F53D0, TextureDefaultFind);
// 	InjectHook(0x7F5500, RwD3D9CreateVertexBuffer);
// 	InjectHook(0x7F56A0, RwD3D9DestroyVertexBuffer);
// 	InjectHook(0x7F57F0, RwD3D9VertexBufferManagerChangeDefaultSize);
// 	InjectHook(0x7F5800, _rwD3D9DynamicVertexBufferManagerForceDiscard);
// 	InjectHook(0x7F5840, _rwD3D9DynamicVertexBufferRelease);
// 	InjectHook(0x7F58D0, _rwD3D9DynamicVertexBufferRestore);
// 	InjectHook(0x7F5940, D3D9DynamicVertexBufferManagerCreate);
// 	InjectHook(0x7F5A00, RwD3D9DynamicVertexBufferCreate);
// 	InjectHook(0x7F5AE0, RwD3D9DynamicVertexBufferDestroy);
// 	InjectHook(0x7F5B10, RwD3D9DynamicVertexBufferLock);
// 	InjectHook(0x7F5C90, RwD3D9DynamicVertexBufferUnlock);
// 	InjectHook(0x7F5CB0, _rwD3D9VertexBufferManagerOpen);
// 	InjectHook(0x7F5D20, _rwD3D9VertexBufferManagerClose);
// 	InjectHook(0x7F5EF0, _rwD3D9SetDepthStencilSurface);
// 	InjectHook(0x7F5F20, _rwD3D9SetRenderTarget);
// 	InjectHook(0x7F5F60, _rwDeviceRegisterPlugin);
// 	InjectHook(0x7F5F70, D3D9System);
// 	InjectHook(0x7F6BF0, D3D9CalculateMaxMultisamplingLevels);
// 	InjectHook(0x7F6CB0, D3D9SetPresentParameters);
// 	InjectHook(0x7F72E0, D3D9DeviceSystemStandards);
// 	InjectHook(0x7F7530, D3D9NullStandard);
// 	InjectHook(0x7F7540, D3D9CreateDisplayModesList);
// 	InjectHook(0x7F7730, _rwD3D9CameraClear);
// 	InjectHook(0x7F7F70, D3D9DeviceReleaseVideoMemory);
// 	InjectHook(0x7F8300, _rwD3D9TestState);
// 	InjectHook(0x7F84E0, RwD3D9EngineGetMaxMultiSamplingLevels);
 	InjectHook(0x7F84F0, nullsub);
 	InjectHook(0x7F8580, nullsub);

// 	InjectHook(0x7F8620, RwD3D9EngineSetMultiThreadSafe);
// 	InjectHook(0x7F8630, RwD3D9EngineSetSoftwareVertexProcessing);
// 	InjectHook(0x7F8640, RwD3D9ChangeVideoMode);
// 	InjectHook(0x7F8A90, RwD3D9ChangeMultiSamplingLevels);
// 	InjectHook(0x7F8D70, RwD3D9CameraAttachWindow);
// 	InjectHook(0x7F8F20, _rwD3D9CameraBeginUpdate);
// 	InjectHook(0x7F98D0, _rwD3D9CameraEndUpdate);
// 	InjectHook(0x7F99B0, _rwD3D9RasterShowRaster);
// 	InjectHook(0x7F9C20, _rwDeviceGetHandle);
 	InjectHook(0x7F9C30, nullsub);
// 	InjectHook(0x7F9D30, RwD3D9SetStencilClear);
// 	InjectHook(0x7F9D40, RwD3D9GetStencilClear);
 	InjectHook(0x7F9D50, breakpoint);
// 	InjectHook(0x7F9E80, RwD3D9GetCurrentD3DRenderTarget);
// 	InjectHook(0x7F9E90, RwD3D9SetRenderTarget);
// 	InjectHook(0x7F9F30, _rwD3D9SetFVF);
// 	InjectHook(0x7F9F70, _rwD3D9SetVertexDeclaration);
// 	InjectHook(0x7F9FB0, _rwD3D9SetVertexShader);
// 	InjectHook(0x7F9FF0, _rwD3D9SetPixelShader);
// 	InjectHook(0x7FA030, RwD3D9SetStreamSource);
// 	InjectHook(0x7FA090, _rwD3D9SetStreams);
// 	InjectHook(0x7FA1C0, _rwD3D9SetIndices);
// 	InjectHook(0x7FA1F0, _rwD3D9DrawIndexedPrimitiveUP);
// 	InjectHook(0x7FA290, _rwD3D9DrawPrimitiveUP);
// 	InjectHook(0x7FA320, _rwD3D9DrawIndexedPrimitive);
// 	InjectHook(0x7FA360, _rwD3D9DrawPrimitive);
// 	InjectHook(0x7FA390, RwD3D9SetTransform);
// 	InjectHook(0x7FA4F0, RwD3D9GetTransform);
// 	InjectHook(0x7FA520, RwD3D9SetTransformWorld);
// 	InjectHook(0x7FA660, RwD3D9SetLight);
// 	InjectHook(0x7FA750, _rwD3D9ForceLight);
// 	InjectHook(0x7FA820, RwD3D9GetLight);
// 	InjectHook(0x7FA860, RwD3D9EnableLight);
// 	InjectHook(0x7FA8B0, _rwD3D9LightsEqual);
// 	InjectHook(0x7FA9B0, _rwD3D9FindSimilarLight);
// 	InjectHook(0x7FAA00, RwD3D9IndexBufferCreate_ ? );
// 	InjectHook(0x7FAA30, RwD3D9CreateVertexDeclaration);
// 	InjectHook(0x7FAC10, RwD3D9DeleteVertexDeclaration);
// 	InjectHook(0x7FAC60, RwD3D9CreateVertexShader);
// 	InjectHook(0x7FAC90, RwD3D9DeleteVertexShader);
// 	InjectHook(0x7FACA0, _rwD3D9SetVertexShaderConstant);
// 	InjectHook(0x7FACC0, RwD3D9CreatePixelShader);
// 	InjectHook(0x7FACF0, RwD3D9DeletePixelShader);
// 	InjectHook(0x7FAD00, _rwD3D9SetPixelShaderConstant);
// 	InjectHook(0x7FAD20, RwD3D9GetCaps);
// 	InjectHook(0x7FAD30, RwD3D9CameraIsSphereFullyInsideFrustum);
// 	InjectHook(0x7FAD90, RwD3D9CameraIsBBoxFullyInsideFrustum);
// 	InjectHook(0x7FAE20, _rwD3D9DeviceSetRestoreCallback);
// 	InjectHook(0x7FAE30, _rwD3D9DeviceGetRestoreCallback);
	InjectHook(0x7FAE40, RwRasterGetPluginOffset);
	InjectHook(0x7FAE60, RwRasterGetCurrentContext);
	InjectHook(0x7FAE80, RwRasterRenderScaled);
	InjectHook(0x7FAEA0, RwRasterGetOffset);
	InjectHook(0x7FAEC0, RwRasterUnlock);
	InjectHook(0x7FAEE0, RwRasterClear);
//	InjectHook(0x7FAF40, RwRasterValidatePlugin);
	InjectHook(0x7FAF50, RwRasterRenderFast);
	InjectHook(0x7FAF90, RwRasterClearRect);
	InjectHook(0x7FAFB0, RwRasterRender);
	InjectHook(0x7FAFF0, RwRasterUnlockPalette);
	InjectHook(0x7FB020, RwRasterDestroy);
	InjectHook(0x7FB060, RwRasterPushContext);
	InjectHook(0x7FB0B0, RwRasterRegisterPlugin);
	InjectHook(0x7FB0E0, RwRasterLockPalette);
	InjectHook(0x7FB110, RwRasterPopContext);
	InjectHook(0x7FB160, RwRasterGetNumLevels);
	InjectHook(0x7FB1A0, RwRasterShowRaster);
	InjectHook(0x7FB1D0, RwRasterSubRaster);
	InjectHook(0x7FB230, RwRasterCreate);
	InjectHook(0x7FB2D0, RwRasterLock);
//	InjectHook(0x7FB310, _rwRasterClose);
	InjectHook(0x7FB350, RwRasterSetFreeListCreateParams);
// 	InjectHook(0x7FB370, _rwRasterOpen);
// 	InjectHook(0x7FB480, _rwD3D9Im2DRenderOpen);
// 	InjectHook(0x7FB5F0, _rwD3D9Im2DRenderClose);
// 	InjectHook(0x7FB650, _rwD3D9Im2DRenderLine);
// 	InjectHook(0x7FB810, rxD3D9Im2DRenderFlush);
// 	InjectHook(0x7FB8E0, _rwD3D9Im2DRenderTriangle);
// 	InjectHook(0x7FBB00, _rwD3D9Im2DRenderPrimitive);
// 	InjectHook(0x7FBD30, _rwD3D9Im2DRenderIndexedPrimitive);
// 	InjectHook(0x7FC200, _rwD3D9RenderStateFlushCache);
// 	InjectHook(0x7FC2D0, RwD3D9SetRenderState);
// 	InjectHook(0x7FC320, RwD3D9GetRenderState);
// 	InjectHook(0x7FC340, RwD3D9SetTextureStageState);
// 	InjectHook(0x7FC3A0, RwD3D9GetTextureStageState);
// 	InjectHook(0x7FC3C0, RwD3D9SetSamplerState);
// 	InjectHook(0x7FC400, RwD3D9GetSamplerState);
// 	InjectHook(0x7FC430, RwD3D9SetMaterial);
// 	InjectHook(0x7FC4A0, RwD3D9SetClipPlane);
// 	InjectHook(0x7FC4D0, RwD3D9SetSurfaceProperties);
// 	InjectHook(0x7FCAC0, _rwD3D9RenderStateOpen);
// 	InjectHook(0x7FD100, _rwD3D9RenderStateReset);
// 	InjectHook(0x7FD810, _rwD3D9RWGetRenderState);
// 	InjectHook(0x7FDA90, _rwD3D9RenderStateTextureAddress);
// 	InjectHook(0x7FDB00, _rwD3D9RenderStateTextureAddressU);
// 	InjectHook(0x7FDB40, _rwD3D9RenderStateTextureAddressV);
// 	InjectHook(0x7FDB80, _rwD3D9RenderStateTextureFilter);
// 	InjectHook(0x7FDC30, _rwD3D9RenderStateSrcBlend);
// 	InjectHook(0x7FDC80, _rwD3D9RenderStateDestBlend);
// 	InjectHook(0x7FDCD0, _rwD3D9RWSetRasterStage);
// 	InjectHook(0x7FDE50, _rwD3D9RWGetRasterStage);
// 	InjectHook(0x7FDE60, _rwD3D9SetTextureAnisotropyOffset);
// 	InjectHook(0x7FDE70, RwD3D9SetTexture);
// 	InjectHook(0x7FE0A0, _rwD3D9RenderStateVertexAlphaEnable);
// 	InjectHook(0x7FE190, _rwD3D9RenderStateIsVertexAlphaEnable);
// 	InjectHook(0x7FE1A0, _rwD3D9RenderStateStencilEnable);
// 	InjectHook(0x7FE1F0, _rwD3D9RenderStateStencilFail);
// 	InjectHook(0x7FE240, _rwD3D9RenderStateStencilZFail);
// 	InjectHook(0x7FE290, _rwD3D9RenderStateStencilPass);
// 	InjectHook(0x7FE2E0, _rwD3D9RenderStateStencilFunction);
// 	InjectHook(0x7FE330, _rwD3D9RenderStateStencilFunctionRef);
// 	InjectHook(0x7FE380, _rwD3D9RenderStateStencilFunctionMask);
// 	InjectHook(0x7FE3D0, _rwD3D9RenderStateStencilFunctionWriteMask);
// 	InjectHook(0x7FE420, _rwD3D9RWSetRenderState);
// 	InjectHook(0x7FEA70, rwD3D9RenderStateZWriteEnable);
// 	InjectHook(0x7FEB70, rwD3D9RenderStateZTestEnable);
// 	InjectHook(0x7FEC70, rwD3D9RenderStateShadeMode);
// 	InjectHook(0x7FECC0, rwD3D9RenderStateBorderColor);
// 	InjectHook(0x7FED00, rwD3D9RenderStateCullMode);
// 	InjectHook(0x7FED50, _rwD3D9FindMSB);
// 	InjectHook(0x7FED70, _rwD3D9CheckRasterSize);
// 	InjectHook(0x7FEE20, _rwD3D9RGBToPixel);
// 	InjectHook(0x7FF070, _rwD3D9PixelToRGB);
// 	InjectHook(0x7FF270, _rwD3D9ImageGetFromRaster);
// 	InjectHook(0x7FF450, D3D9Image32GetFromRaster);
// 	InjectHook(0x7FF8C0, D3D9Image32GetFromD3D9Raster);
// 	InjectHook(0x7FFBE0, D3D9ConvR5G5B5ToR8G8B8A8);
// 	InjectHook(0x7FFC10, D3D9ConvR5G6B5ToR8G8B8A8);
// 	InjectHook(0x7FFC40, D3D9ConvL6V5U5ToR8G8B8A8_0);
// 	InjectHook(0x7FFC70, D3D9ConvA1R5G5B5ToR8G8B8A8);
// 	InjectHook(0x7FFCB0, D3D9ConvA4R4G4B4ToR8G8B8A8);
// 	InjectHook(0x7FFD00, D3D9ConvA8R3G3B2ToR8G8B8A8_0);
// 	InjectHook(0x7FFD30, D3D9ConvR3G3B2ToR8G8B8A8);
// 	InjectHook(0x7FFD60, D3D9ConvX4R4G4B4ToR8G8B8A8);
// 	InjectHook(0x7FFD90, D3D9ConvX8R8G8B8ToR8G8B8A8_0);
// 	InjectHook(0x7FFDB0, D3D9ConvX8B8G8R8ToR8G8B8A8);
// 	InjectHook(0x7FFDD0, D3D9ConvV8U8ToR8G8B8A8_0);
// 	InjectHook(0x7FFDF0, D3D9ConvA8R8G8B8ToR8G8B8A8);
// 	InjectHook(0x7FFE20, D3D9ConvA8B8G8R8ToR8G8B8A8);
// 	InjectHook(0x7FFE50, D3D9ConvL8ToR8G8B8A8);
// 	InjectHook(0x7FFE70, D3D9ConvA8ToR8G8B8A8);
// 	InjectHook(0x7FFE90, D3D9ConvA4L4ToR8G8B8A8);
// 	InjectHook(0x7FFEC0, D3D9ConvA8L8ToR8G8B8A8);
// 	InjectHook(0x7FFEE0, D3D9ConvL16ToR8G8B8A8);
// 	InjectHook(0x7FFF00, _rwD3D9ImageFindRasterFormat);
// 	InjectHook(0x7FFFF0, _rwD3D9ImageFindFormat);
// 	InjectHook(0x8001E0, _rwD3D9RasterSetFromImage);
// 	InjectHook(0x800810, D3D9SelectConvertFn);
// 	InjectHook(0x800930, D3D9ConvR8G8B8A8ToR5G5B5);
// 	InjectHook(0x800960, D3D9ConvR8G8B8A8ToR5G6B5);
// 	InjectHook(0x800990, D3D9ConvR8G8B8A8ToA1R5G5B5);
// 	InjectHook(0x8009D0, D3D9ConvR8G8B8A8ToA4R4G4B4);
// 	InjectHook(0x800A10, D3D9ConvR8G8B8A8ToA8R3G3B2);
// 	InjectHook(0x800A40, D3D9ConvR8G8B8A8ToR4G4B4);
// 	InjectHook(0x800A70, D3D9ConvR8G8B8A8ToL6V5U5);
// 	InjectHook(0x800AA0, D3D9ConvR8G8B8A8ToX8R8G8B8);
// 	InjectHook(0x800AD0, D3D9ConvR8G8B8A8ToX8B8G8R8);
// 	InjectHook(0x800B00, D3D9ConvR8G8B8A8ToA8R8G8B8);
// 	InjectHook(0x800B30, D3D9ConvR8G8B8A8ToA8B8G8R8);
// 	InjectHook(0x800B60, D3D9ConvR8G8B8A8ToV8U8);
// 	InjectHook(0x800B70, D3D9ConvR8G8B8A8ToL8);
// 	InjectHook(0x800BD0, D3D9ConvR8G8B8A8ToA8L8);
// 	InjectHook(0x800C20, D3D9ConvR8G8B8A8ToL16);
// 	InjectHook(0x800C80, D3D9RasterPalletizedSetFromImage);
// 	InjectHook(0x801390, D3D9Raster24_32LinearSetFromImage);
// 	InjectHook(0x801970, _rwFreeListEnable);
//	InjectHook(0x801980, _rwFreeListCreate);
	//InjectHook(0x8019B0, FreeListCreate);
	InjectHook(0x801B70, RwFreeListCreateAndPreallocateSpace);
	InjectHook(0x801B80, RwFreeListDestroy);
	InjectHook(0x801C10, RwFreeListSetFlags);
	InjectHook(0x801C20, RwFreeListGetFlags);
	//InjectHook(0x801C30, _rwFreeListAllocReal);
	//InjectHook(0x801D50, _rwFreeListFreeReal);
	InjectHook(0x801E00, RwFreeListPurge);
	InjectHook(0x801E90, RwFreeListForAllUsed);
	InjectHook(0x801F90, RwFreeListPurgeAllFreeLists);
// 	InjectHook(0x801FD0, _rwMemoryOpen);
// 	InjectHook(0x8020A0, HMalloc);
// 	InjectHook(0x8020B0, HRealloc);
// 	InjectHook(0x8020D0, HCalloc);
// 	InjectHook(0x8020F0, _rwMemoryClose);
	InjectHook(0x802230, RwOsGetMemoryInterface);
	//InjectHook(0x802280, _rwImageOpen);
	//InjectHook(0x8024E0, _rwImageClose);
	InjectHook(0x8026E0, RwImageCreate);
	InjectHook(0x802740, RwImageDestroy);
	InjectHook(0x8027A0, RwImageAllocatePixels);
	InjectHook(0x802860, RwImageFreePixels);
	InjectHook(0x802A20, RwImageMakeMask);
	InjectHook(0x802AF0, RwImageApplyMask);
	InjectHook(0x802EA0, RwImageSetPath);
	InjectHook(0x802FD0, RwImageRead);
	InjectHook(0x8035C0, RwImageReadMaskedImage);
	InjectHook(0x803950, RwImageCopy);
	InjectHook(0x803E30, RwImageGammaCorrect);
	InjectHook(0x803FE0, RwImageSetGamma);
	//InjectHook(0x804130, RwOsGetFileInterface);
	//InjectHook(0x804140, _rwFileOpen);
	//InjectHook(0x804200, _RwCheckFileReadable);
	//InjectHook(0x804240, ullsub_73);
	InjectHook(0x804250, RwImageSetFromRaster);
	InjectHook(0x804290, RwRasterSetFromImage);
	InjectHook(0x8042C0, RwImageFindRasterFormat);
	InjectHook(0x8043F0, RwRasterRead);
	InjectHook(0x8044E0, RwRasterReadMaskedRaster);
	InjectHook(0x804550, RwTextureRegisterPluginStream);
	InjectHook(0x804580, RwTextureSetStreamAlwaysCallBack);
	InjectHook(0x8045A0, RwTextureStreamGetSize);
	InjectHook(0x8045E0, RwTextureStreamWrite);
	InjectHook(0x8046E0, RwTextureStreamRead);
	InjectHook(0x8048E0, RwTexDictionaryRegisterPluginStream);
	InjectHook(0x804910, RwTexDictionarySetStreamAlwaysCallBack);
	InjectHook(0x804930, RwTexDictionaryStreamGetSize);
	//InjectHook(0x804990, addNativeTextureSize);
	InjectHook(0x8049F0, RwTexDictionaryStreamWrite);
	//InjectHook(0x804B60, countTexturesInDictionary);
	//InjectHook(0x804B70, writeNativeTexture);
	InjectHook(0x804C30, RwTexDictionaryStreamRead);
	//InjectHook(0x804E40, destroyTexture);
	InjectHook(0x804E60, _rwTextureChunkInfoRead);
	InjectHook(0x804EF0, _rwObjectHasFrameSetFrame);
	InjectHook(0x804F40, _rwObjectHasFrameReleaseFrame);
	InjectHook(0x804F60, _rxPipelineClose);
	InjectHook(0x804FC0, RxPipelineSetFreeListCreateParams);
	InjectHook(0x804FE0, _rxPipelineOpen);
	//InjectHook(0x8050C0, PipelineNodeDestroy);
	InjectHook(0x8052F0, RxHeapGetGlobalHeap);
	InjectHook(0x805300, RxPacketCreate);
	InjectHook(0x805330, RxClusterSetStride);
	InjectHook(0x805340, RxClusterSetExternalData);
	InjectHook(0x8053A0, RxClusterSetData);
	InjectHook(0x805400, RxClusterInitializeData);
	InjectHook(0x805470, RxClusterResizeData);
	InjectHook(0x8054C0, RxClusterDestroyData);
	InjectHook(0x8054F0, RxClusterLockWrite);
	InjectHook(0x8055C0, RxClusterUnlock);
	InjectHook(0x8055D0, RxPipelineNodeSendConfigMsg);
	InjectHook(0x8055F0, RxPipelineNodeForAllConnectedOutputs);
	//InjectHook(0x805670, _rwPipelineCheckForTramplingOfNodePrivateSpace);
	InjectHook(0x805680, RxPipelineNodeGetPipelineCluster);
	InjectHook(0x8056B0, RxPipelineClusterGetCreationAttributes);
	InjectHook(0x8056C0, RxPipelineClusterSetCreationAttributes);
	InjectHook(0x8056E0, RxClusterGetAttributes);
	InjectHook(0x8056F0, RxClusterSetAttributes);
	InjectHook(0x805710, RxPipelineExecute);
	InjectHook(0x8057B0, RxPipelineCreate);
	InjectHook(0x805820, _rxPipelineDestroy);
// 	InjectHook(0x805890, StalacTiteAlloc);
// 	InjectHook(0x8058F0, StalacMiteAlloc);
// 	InjectHook(0x805950, PipelineCalcNumUniqueClusters);
	InjectHook(0x8059C0, RxPipelineNodeCloneDefinition);
	//InjectHook(0x805A20, _NodeClone);
	InjectHook(0x805D10, RxPipelineNodeFindOutputByName);
	InjectHook(0x805D70, RxPipelineNodeFindOutputByIndex);
	InjectHook(0x805DA0, RxPipelineNodeFindInput);
	InjectHook(0x805DB0, RxPipelineNodeRequestCluster);
	InjectHook(0x805E20, RxPipelineNodeReplaceCluster);
	InjectHook(0x805EA0, RxPipelineNodeCreateInitData);
	InjectHook(0x805F30, RxPipelineNodeGetInitData);
	InjectHook(0x805F40, RxLockedPipeUnlock);
// 	InjectHook(0x8064D0, ReallocAndFixupSuperBlock);
// 	InjectHook(0x8065E0, LockPipelineExpandData);
// 	InjectHook(0x8067B0, PipelineTopSort);
	InjectHook(0x806990, RxPipelineLock);
	InjectHook(0x806AC0, RxPipelineClone);
	InjectHook(0x806B30, RxPipelineFindNodeByName);
	InjectHook(0x806BC0, RxPipelineFindNodeByIndex);
	InjectHook(0x806BE0, RxLockedPipeAddFragment);
	//InjectHook(0x806DE0, _NodeCreate);
	InjectHook(0x806F20, RxLockedPipeReplaceNode);
	InjectHook(0x807040, RxLockedPipeDeleteNode);
	InjectHook(0x807070, RxLockedPipeSetEntryPoint);
	InjectHook(0x8070D0, RxLockedPipeGetEntryPoint);
	InjectHook(0x807100, RxLockedPipeAddPath);
	InjectHook(0x807170, RxLockedPipeDeletePath);
	InjectHook(0x8071B0, RxPipelineInsertDebugNode);
	InjectHook(0x807570, RwFrameListSetAutoUpdate);
	InjectHook(0x807580, RwFrameRegisterPluginStream);
	InjectHook(0x8075B0, RwFrameSetStreamAlwaysCallBack);
	InjectHook(0x8075D0, _rwFrameListInitialize);
	//InjectHook(0x8076A0, breadthFirst);
	InjectHook(0x8076E0, _rwFrameListFindFrame);
	InjectHook(0x807720, _rwFrameListDeinitialize);
	InjectHook(0x807750, _rwFrameListStreamGetSize);
	InjectHook(0x8077A0, _rwFrameListStreamWrite);
	InjectHook(0x807970, _rwFrameListStreamRead);
// 	InjectHook(0x807C10, sub_807C10);
// 	InjectHook(0x807C2B, irect3DCreate9);
// 	InjectHook(0x807C40, _rwRenderPipelineOpen);
// 	InjectHook(0x807C60, _rwRenderPipelineClose);
// 	InjectHook(0x807C70, _rwPipeAttach);
// 	InjectHook(0x807C80, _rwPipeInitForCamera);
// 	InjectHook(0x807C90, _rwResourcesOpen);
// 	InjectHook(0x807D80, _rwResourcesClose);
	InjectHook(0x807DE0, RwResourcesFreeResEntry);
	InjectHook(0x807E50, _rwResourcesPurge);
	InjectHook(0x807ED0, RwResourcesAllocateResEntry);
	InjectHook(0x8080C0, RwResourcesSetArenaSize);
	InjectHook(0x8081B0, RwResourcesGetArenaSize);
	InjectHook(0x8081C0, RwResourcesGetArenaUsage);
	InjectHook(0x8081F0, RwResourcesEmptyArena);
// 	InjectHook(0x8082E0, _rwPluginRegistryOpen);
// 	InjectHook(0x808320, _rwPluginRegistryClose);
//	InjectHook(0x8083F0, rwDestroyEntry);
	InjectHook(0x808430, _rwPluginRegistrySetStaticPluginsSize);
	InjectHook(0x808470, _rwPluginRegistryGetPluginOffset);
	InjectHook(0x8084A0, _rwPluginRegistryAddPlugin);
// 	InjectHook(0x8086B0, PluginDefaultConstructor);
// 	InjectHook(0x8086C0, PluginDefaultDestructor);
// 	InjectHook(0x8086D0, PluginDefaultCopy);
	InjectHook(0x8086E0, _rwPluginRegistryInitObject);
	InjectHook(0x808740, _rwPluginRegistryDeInitObject);
	InjectHook(0x808770, _rwPluginRegistryCopyObject);
	InjectHook(0x8087B0, RwPluginRegistrySetFreeListCreateParams);
// 	InjectHook(0x8087D0, _rwErrorOpen);
// 	InjectHook(0x808810, _rwErrorClose);
	InjectHook(0x808820, RwErrorSet);
	InjectHook(0x808880, RwErrorGet);
	InjectHook(0x8088D0, _rwerror);
	InjectHook(0x8088E0, _rwPluginRegistryAddPluginStream);
	InjectHook(0x808920, _rwPluginRegistryAddPlgnStrmlwysCB);
	InjectHook(0x808950, _rwPluginRegistryAddPlgnStrmRightsCB);
	InjectHook(0x808980, _rwPluginRegistryReadDataChunks);
	InjectHook(0x808AB0, _rwPluginRegistryInvokeRights);
	InjectHook(0x808B00, _rwPluginRegistryGetSize);
	InjectHook(0x808B40, _rwPluginRegistryWriteDataChunks);
	InjectHook(0x808C10, _rwPluginRegistrySkipDataChunks);
	InjectHook(0x808C90, RwCameraRegisterPluginStream);
	InjectHook(0x808CC0, RwCameraSetStreamAlwaysCallBack);
	InjectHook(0x808CE0, RwCameraStreamGetSize);
	InjectHook(0x808D00, RwCameraStreamWrite);
	InjectHook(0x808DE0, RwCameraStreamRead);
	InjectHook(0x808EF0, RwCameraChunkInfoRead);
	InjectHook(0x808F60, RwBBoxCalculate);
	InjectHook(0x809020, RwBBoxInitialize);
	InjectHook(0x809060, RwBBoxAddPoint);
	InjectHook(0x8090E0, RwBBoxContainsPoint);
	InjectHook(0x809160, _rwSListCreate);
	InjectHook(0x809240, _rwSListGetNewEntry);
	InjectHook(0x809400, _rwSListDestroyEndEntries);
	InjectHook(0x809440, _rwSListDestroy);
	InjectHook(0x8094B0, _rwSListGetNumEntries);
	InjectHook(0x809510, _rwSListGetEntry);
	InjectHook(0x809530, _rwSListGetBegin);
	InjectHook(0x809540, _rwSListGetEnd);
	InjectHook(0x809550, _rwFrameSyncDirty);
// 	InjectHook(0x809700, FrameSyncHierarchyRecurse);
// 	InjectHook(0x809780, FrameSyncHierarchyRecurseNoLTM);
	InjectHook(0x8097D0, _rwFrameSyncHierarchyLTM);
//	InjectHook(0x809850, FrameSyncHierarchyLTMRecurse);
	InjectHook(0x8098B0, RxHeapFree);
//	InjectHook(0x8099F0, HeapFreeBlocksNewEntry);
	InjectHook(0x809AA0, RxHeapAlloc);
//	InjectHook(0x809BF0, HeapSuperBlockReset);
//	InjectHook(0x809C70, HeapAllocFrom);
	InjectHook(0x809D10, RxHeapRealloc);
	InjectHook(0x809EC0, _rxHeapReset);
	InjectHook(0x809F30, RxHeapDestroy);
	InjectHook(0x809F90, RxHeapCreate);
// 	InjectHook(0x80A0A0, _rwIm3DCreatePlatformTransformPipeline);
// 	InjectHook(0x80A110, _rwIm3DDestroyPlatformTransformPipeline);
// 	InjectHook(0x80A140, _rwIm3DDestroyPlatformRenderPipelines);
// 	InjectHook(0x80A1A0, _rwIm3DCreatePlatformRenderPipelines);
	InjectHook(0x80A240, _rwStringOpen);
// 	InjectHook(0x80A350, StrICmp);
// 	InjectHook(0x80A3A0, StrUpr);
// 	InjectHook(0x80A3D0, StrLwr);
// 	InjectHook(0x80A400, StrChr);
// 	InjectHook(0x80A420, StrRChr);
	InjectHook(0x80A440, _rwStringClose);
	InjectHook(0x80A450, _rwStringDestroy);
	InjectHook(0x80A470, _rwStringStreamGetSize);
	InjectHook(0x80A4A0, _rwStringStreamWrite);
	InjectHook(0x80A510, _rwStringStreamFindAndRead);
// 	InjectHook(0x80A780, _rwChunkGroupOpen);
// 	InjectHook(0x80A7E0, _rwChunkGroupClose);
// 	InjectHook(0x80AA40, _rwColorOpen);
// 	InjectHook(0x80AA50, _rwColorClose);
	InjectHook(0x80AA80, RwPalQuantAddImage);
	InjectHook(0x80AF60, RwPalQuantResolvePalette);
	InjectHook(0x80BF20, RwPalQuantMatchImage);
	InjectHook(0x80C470, RwPalQuantInit);
	InjectHook(0x80C520, RwPalQuantTerm);
	InjectHook(0x80C600, RwImageResample);
// 	InjectHook(0x80CA20, ImageResampleGetSpan);
	InjectHook(0x80CD10, RwImageCreateResample);
// 	InjectHook(0x80CE10, _rwIntelSSEMatrixMultiply);
// 	InjectHook(0x80CF80, _rwIntelSSEAlignedMatrixMultiply);
// 	InjectHook(0x80D070, _rwIntelSSEV3dTransformPoints);
// 	InjectHook(0x80D100, _rwIntelSSEV3dTransformPointsAlignedMatrix);
// 	InjectHook(0x80D180, _rwIntelSSEV3dTransformVectors);
// 	InjectHook(0x80D210, _rwIntelSSEV3dTransformVectorsAlignedMatrix);
// 	InjectHook(0x80D280, _rwIntelSSEV3dTransformPoint);
// 	InjectHook(0x80D300, _rwIntelSSEV3dTransformPointAlignedMatrix);
// 	InjectHook(0x80D370, _rwIntelSSEV3dTransformVector);
// 	InjectHook(0x80D3E0, _rwIntelSSEV3dTransformVectorAlignedMatrix);
// 	InjectHook(0x80D440, _rwIntelSSEP4V3dTransformPoints);
// 	InjectHook(0x80D4E0, _rwIntelSSEP4V3dTransformVectors);
// 	InjectHook(0x80D570, _rwIntelSSEP4V3dTransformPoint);
// 	InjectHook(0x80D600, _rwIntelSSEP4V3dTransformVector);
// 	InjectHook(0x80DBE0, _rwCPUHaveMMX);
// 	InjectHook(0x80DC40, _rwCPUHaveSSE);
// 	InjectHook(0x80DCA0, _rwCPUHaveSSE2);
// 	InjectHook(0x80DD00, _rwCPUHave3DNow);
// 	InjectHook(0x80E020, _rwD3D9Im3DRenderOpen);
	InjectHook(0x80EA40, RxRenderStateVectorSetDefaultRenderStateVector);
	InjectHook(0x80F070, _rxPacketDestroy);
// 	InjectHook(0x80F0D0, _rxChaseDependencies);
// 	InjectHook(0x80F4B0, _ReqAddEntry);
// 	InjectHook(0x80F570, _PropDownElimPath);
// 	InjectHook(0x80F690, _TraceClusterScopes_1);
// 	InjectHook(0x80F970, _TraceClusterScopes);
// 	InjectHook(0x80FC30, _ForAllNodesWriteClusterAllocations);
// 	InjectHook(0x80FFE0, _rwResHeapInit);
// 	InjectHook(0x810030, _rwResHeapClose);
// 	InjectHook(0x8100C0, _rwResHeapAlloc);
// 	InjectHook(0x8103D0, _rx_rxRadixExchangeSort);
	InjectHook(0x810700, RpMatFXMaterialDataSetFreeListCreateParams);
// 	InjectHook(0x810720, _rpMatFXStreamWriteTexture);
// 	InjectHook(0x810770, _rpMatFXStreamReadTexture);
// 	InjectHook(0x810810, _rpMatFXStreamSizeTexture);
// 	InjectHook(0x810830, _rpMatFXTextureMaskCreate);
	InjectHook(0x810AA0, RpMatFXPluginAttachWrap);
// 	InjectHook(0x810BB0, MatFXClose);
// 	InjectHook(0x810BF0, MatFXOpen);
// 	InjectHook(0x810C40, MatFXMaterialConstructor);
// 	InjectHook(0x810C60, MatFXMaterialDestructor);
// 	InjectHook(0x810CA0, MatFXMaterialDataClean);
// 	InjectHook(0x810D20, MatFXMaterialCopy);
// 	InjectHook(0x811150, MatFXGetData);
// 	InjectHook(0x8111A0, MatFXGetConstData);
// 	InjectHook(0x8111F0, MatFXMaterialStreamWrite);
// 	InjectHook(0x811410, MatFXMaterialStreamRead);
// 	InjectHook(0x811970, MatFXMaterialStreamGetSize);
// 	InjectHook(0x811A00, MatFXAtomicConstructor);
// 	InjectHook(0x811A20, MatFXAtomicDestructor);
// 	InjectHook(0x811A40, MatFXAtomicCopy);
// 	InjectHook(0x811A60, MatFXAtomicStreamWrite);
// 	InjectHook(0x811A90, MatFXAtomicStreamRead);
// 	InjectHook(0x811AE0, MatFXAtomicStreamGetSize);
// 	InjectHook(0x811B00, MatFXWorldSectorConstructor);
// 	InjectHook(0x811B20, MatFXWorldSectorDestructor);
// 	InjectHook(0x811B40, MatFXWorldSectorCopy);
// 	InjectHook(0x811B60, MatFXWorldSectorStreamWrite);
// 	InjectHook(0x811B90, MatFXWorldSectorStreamRead);
// 	InjectHook(0x811BE0, MatFXWorldSectorStreamGetSize);
	InjectHook(0x811C00, RpMatFXAtomicEnableEffects);
	InjectHook(0x811C30, RpMatFXAtomicQueryEffects);
	InjectHook(0x811C40, RpMatFXWorldSectorEnableEffects);
	InjectHook(0x811C70, RpMatFXWorldSectorQueryEffects);
	InjectHook(0x811C80, RpMatFXMaterialSetEffects);
	InjectHook(0x811E00, RpMatFXMaterialSetupBumpMap);
	InjectHook(0x811ED0, RpMatFXMaterialSetupEnvMap);
	InjectHook(0x812040, RpMatFXMaterialSetupDualTexture);
	InjectHook(0x812140, RpMatFXMaterialGetEffects);
	InjectHook(0x812160, RpMatFXMaterialSetBumpMapTexture);
	InjectHook(0x812320, RpMatFXMaterialSetBumpMapFrame);
	InjectHook(0x812380, RpMatFXMaterialSetBumpMapCoefficient);
	InjectHook(0x8123E0, RpMatFXMaterialGetBumpMapBumpedTexture);
	InjectHook(0x812430, RpMatFXMaterialGetBumpMapTexture);
	InjectHook(0x812480, RpMatFXMaterialGetBumpMapFrame);
	InjectHook(0x8124D0, RpMatFXMaterialGetBumpMapCoefficient);
	InjectHook(0x812530, RpMatFXMaterialSetEnvMapTexture);
	InjectHook(0x8125B0, RpMatFXMaterialSetEnvMapFrame);
	InjectHook(0x812610, RpMatFXMaterialSetEnvMapFrameBufferAlpha);
	InjectHook(0x812680, RpMatFXMaterialSetEnvMapCoefficient);
	InjectHook(0x8126F0, RpMatFXMaterialGetEnvMapTexture);
	InjectHook(0x812740, RpMatFXMaterialGetEnvMapFrame);
	InjectHook(0x812790, RpMatFXMaterialGetEnvMapFrameBufferAlpha);
	InjectHook(0x8127E0, RpMatFXMaterialGetEnvMapCoefficient);
	InjectHook(0x812830, RpMatFXMaterialSetDualTexture);
	InjectHook(0x8128C0, RpMatFXMaterialSetDualBlendModes);
	InjectHook(0x812930, RpMatFXMaterialGetDualTexture);
	InjectHook(0x812980, RpMatFXMaterialGetDualBlendModes);
	InjectHook(0x8129E0, RpMatFXMaterialSetUVTransformMatrices);
	InjectHook(0x812A50, RpMatFXMaterialGetUVTransformMatrices);
// 	InjectHook(0x812AC0, _rpMatFXD3D9AtomicMatFXDefaultRender);
// 	InjectHook(0x812C90, _rpMatFXD3D9AtomicMatFXRenderBlack);
// 	InjectHook(0x812D40, _rpMatFXD3D9AtomicMatFXDualPassRender);
// 	InjectHook(0x813A10, _rpMatFXD3D9AtomicMatFXEnvRender);
// 	InjectHook(0x814270, ApplyEnvMapTextureMatrix);
// 	InjectHook(0x814460, _rpMatFXD3D9AtomicMatFXBumpMapRender);
// 	InjectHook(0x8149E0, CalculatePerturbedUVs);
// 	InjectHook(0x815C20, _rwD3D9AtomicMatFXRenderCallback);
// 	InjectHook(0x815E30, _rwD3D9MaterialMatFXHasBumpMap);
// 	InjectHook(0x815E60, _rpMatFXPipelinesCreate);
// 	InjectHook(0x8160A0, D3D9AtomicMatFXInstanceCallback);
// 	InjectHook(0x816120, D3D9WorldSectorMatFXInstanceCallback);
// 	InjectHook(0x816190, _rpMatFXPipelinesDestroy);
// 	InjectHook(0x816260, _rpMatFXPipelineAtomicSetup);
// 	InjectHook(0x816270, _rpMatFXPipelineWorldSectorSetup);
// 	InjectHook(0x816280, _rpMatFXSetupDualRenderState);
// 	InjectHook(0x816290, _rpMatFXSetupBumpMapTexture);
// 	InjectHook(0x8162F0, RpMatFXGetD3D9Pipeline);
// 	InjectHook(0x816310, _rpMatFXD3D9VertexShaderAtomicEnvRender);
// 	InjectHook(0x816A80, _rpMatFXD3D9VertexShaderAtomicDualRender);
// 	InjectHook(0x8172F0, _rpMatFXD3D9VertexShaderAtomicBumpMapRender);

	/*RwDebugSetHandler([] (RwDebugType type, const RwChar* message)
	{
		OutputDebugStringA(message);
	});*/

	for (char* ptr = (char*)0x401000; ptr < (char*)0x856F00; ptr++)
	{
		if (*(uint32_t*)ptr == 0xC97B24)
		{
			Patch(ptr, &RwEngineInstance);
		}
	}

	Patch<uint8_t>(0x747180, 0xC3);
}

extern "C"
{
	void* RwFopen(const char* name, const char* access)
	{
		return fopen(name, access);
	}

	bool RwFexist(const char* name)
	{
		FILE* f = fopen(name, "rb");

		if (f)
		{
			fclose(f);
		}

		return (f != 0);
	}

	int RwFclose(void* handle)
	{
		fclose((FILE*)handle);

		return 0;
	}

	int RwFeof(void* handle)
	{
		return feof((FILE*)handle);
	}

	char* RwFgets(char* buffer, int maxLen, void* handle)
	{
		return fgets(buffer, maxLen, (FILE*)handle);
	}

	size_t RwFread(void* a, size_t s, size_t c, void* f)
	{
		return fread(a, s, c, (FILE*)f);
	}

	int RwFtell(void* h)
	{
		return ftell((FILE*)h);
	}

	int RwFseek(void* f, long o, int or)
	{
		return fseek((FILE*)f, o, or);
	}

	size_t RwFwrite(void* a, size_t s, size_t c, void* f)
	{
		return fwrite(a, s, c, (FILE*)f);
	}
}
#endif