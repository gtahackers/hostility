#include "StdInc.h"

float g_pathDivisor = (1 / 4.0f);

void PatchPathTransfer();

void PatchPathDivisors()
{
	using namespace MemoryVP;

	auto quickPatch = [&] (int funcStart, int funcEnd)
	{
		for (char* ptr = (char*)funcStart; ptr <= (char*)funcEnd; ptr++)
		{
			if (!memcmp(ptr, "\xD8\x0D\x48\x8C\x85\x00", 6))
			{
				Patch<float*>(&ptr[2], &g_pathDivisor);
			}
		}
	};

	quickPatch(0x420A25, 0x420A4B);
	quickPatch(0x41BB1D, 0x41BBAB);
	quickPatch(0x420A7B, 0x420A91);
	quickPatch(0x42266E, 0x422684);
	quickPatch(0x424210, 0x424CD7);
	quickPatch(0x426FAA, 0x427733);
	quickPatch(0x427B36, 0x427F47);
	quickPatch(0x428110, 0x428386);
	quickPatch(0x42B646, 0x42B6C7);
	quickPatch(0x42B646, 0x42B6C7);

	// in fact, let's try some blind rage
	quickPatch(0x42BA54, 0x4528D0);

	// related-ish
	PatchPathTransfer();
}