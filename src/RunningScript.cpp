#include "StdInc.h"

void CRunningScript::ProcessOneCommand()
{
	DWORD dwFunc = 0x469EB0;

	__asm
	{
		mov ecx, this
		mov eax, dwFunc
		call eax
	}
}

void DumpLocalVars(int* localVars)
{
	if (!localVars)
	{
		return;
	}

	for (int i = 0; i < 32; i++)
	{
		//Trace("localVars[%i] = %i\n", i, localVars[i]);
	}
}

// method from most 'SCM hooks'
bool CRunningScript::ProcessBuffer(std::string name, const char* buffer, int* localVars)
{
	CRunningScript script;
	memset(&script, 0, sizeof(script));
	script.bUnknown = 0xFF;
	script.bStartNewScript = 1;
	script.dwBaseIP = (DWORD)buffer;
	script.dwScriptIP = (DWORD)buffer;
	strncpy(script.strName, name.c_str(), 8);
	script.strName[7] = 0;

	//DumpLocalVars(localVars);

	if (localVars)
	{
		memcpy(script.dwLocalVar, localVars, 32 * sizeof(int));
	}

	script.ProcessOneCommand();

	if (localVars)
	{
		memcpy(localVars, script.dwLocalVar, 32 * sizeof(int));
	}

	//DumpLocalVars(localVars);

	return (script.bCompareFlag) ? true : false;
}