#include "StdInc.h"

// original values:
// model max: 20000
// txd max: 25000
// col max: 25255
// ipl max: 25511
// node max: 25575
// ifp max: 25755
// rrr max: 26230
// scm max: ...

static char* g_imgEntryList;

void AtRuntimeInitDummiesDo()
{
	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 0)] + 2) = 0xFFFF;
	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 0)]) = STREAMING_SCM_MAX + 1;
	
	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 1)] + 2) = STREAMING_SCM_MAX + 0;
	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 1)]) = STREAMING_SCM_MAX + 2;

	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 2)] + 2) = STREAMING_SCM_MAX + 1;
	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 2)]) = STREAMING_SCM_MAX + 3;

	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 3)] + 2) = STREAMING_SCM_MAX + 2;
	*(uint16_t*)(&g_imgEntryList[20 * (STREAMING_SCM_MAX + 3)]) = 0xFFFF;

	for (int i = 374; i <= 381; i++)
	{
		*(uint8_t*)(&g_imgEntryList[20 * i] + 16) = 1;
	}
}

void __declspec(naked) AtRuntimeInitDummies()
{
	__asm
	{
		push eax
		push ecx
		call AtRuntimeInitDummiesDo
		pop ecx
		pop eax

		push 5B8B7Ah
		retn
	}
}

void __declspec(naked) CmpEsi40CD10()
{
	__asm
	{
		cmp si, 0FFFFh

		jz ret40CEF8h

		push 40CD19h
		retn

	ret40CEF8h:
		push 40CEF8h
		retn
	}
}

void __declspec(naked) CmpEsi408F72()
{
	__asm
	{
		cmp ax, 0FFFFh

		jz ret408FC2h

		push 408F77h
		retn

	ret408FC2h:
		push 408FC2h
		retn
	}
}

void ReportRequest(int id)
{
	char buf[64];
	sprintf(buf, "req %d\n", id);

	if (id == 35064)
	{
		//__asm int 3
	}

	OutputDebugStringA(buf);
}

void __declspec(naked) RequestModelStub()
{
	__asm
	{
		push dword ptr [esp + 4]
		call ReportRequest
		add esp, 4h

		push ebx
		mov ebx, [esp + 4 + 8]

		push 4087E5h
		retn
	}
}

void ReportBuffer(int id)
{
	char buf[64];
	sprintf(buf, "load %d\n", id);

	OutputDebugStringA(buf);
}

void __declspec(naked) ConvertBufferStub()
{
	__asm
	{
		push dword ptr [esp + 8h]
		call ReportBuffer
		add esp, 4h

		sub esp, 20h
		mov ecx, [esp + 20h + 4]

		push 40C6B7h
		retn
	}
}

void ReportStream(int id)
{
	char buf[64];
	sprintf(buf, "strm %d\n", id);

	//OutputDebugStringA(buf);
}

void __declspec(naked) ThisStreamStub()
{
	__asm
	{
		push esi
		call ReportStream
		add esp, 4h

		lea ebx, [esi + esi * 4]
		shl ebx, 2

		push 40CD1Fh
		retn
	}
}

void PatchStreamingIndices()
{
	static_assert(STREAMING_SCM_MAX < 65535, "Exceeded uint16_t for model info!");

	using namespace MemoryVP;

	//InjectHook(0x4087E0, RequestModelStub);
	//InjectHook(0x40C6B0, ConvertBufferStub);

	//InjectHook(0x40CD19, ThisStreamStub);*/

	Patch(0x407106, STREAMING_TXD_MIN);
	Patch(0x407B1B, STREAMING_TXD_MIN);
	Patch(0x407B6C, STREAMING_TXD_MIN);
	Patch(0x407B8C, STREAMING_TXD_MIN);

	Patch(0x40885A, STREAMING_TXD_MIN);
	Patch(0x4088FD, STREAMING_TXD_MIN);

	Patch(0x4089C8, STREAMING_TXD_MIN);
	Patch(0x408BA0, STREAMING_TXD_MIN);

	Patch(0x408CCA, STREAMING_TXD_MIN);
	Patch(0x408D15, STREAMING_TXD_MIN);

	Patch(0x4088FD, STREAMING_TXD_MIN);

	Patch(0x408EB6, STREAMING_TXD_MIN);
	Patch(0x408EED, STREAMING_TXD_MIN);
	Patch(0x408F78, STREAMING_TXD_MIN);

	Patch(0x409AE7, STREAMING_TXD_MIN);

	// lazy patching time!
	for (char* ptr = (char*)0x408930; ptr < (char*)0x40E1F7; ptr++)
	{
		int minDigit = -0x4E20;

		if (!memcmp(ptr, &minDigit, 4))
		{
			Patch(ptr, -STREAMING_TXD_MIN);
			ptr += 4;
		}

		if (!memcmp(ptr, "\x20\x4E\x00\x00", 4))
		{
			Patch(ptr, STREAMING_TXD_MIN);
			ptr += 4;
		}
	}

	Patch(0x4C487E, STREAMING_TXD_MIN);
	Patch(0x4C4886, STREAMING_TXD_MIN);

	Patch(0x4C5958, STREAMING_TXD_MIN);
	Patch(0x4C5973, STREAMING_TXD_MIN);
	Patch(0x4C59D3, STREAMING_TXD_MIN);

	Patch(0x4C6816, STREAMING_TXD_MIN);
	Patch(0x5384F5, STREAMING_TXD_MIN);

	Patch(0x584B8E, STREAMING_TXD_MIN);
	Patch(0x584C25, STREAMING_TXD_MIN);

	Patch(0x584CC4, STREAMING_TXD_MIN);
	Patch(0x584CFA, STREAMING_TXD_MIN);

	Patch(0x5A4297, STREAMING_TXD_MIN);

	Patch(0x5A5FF6, STREAMING_TXD_MIN);

	Patch(0x5A6186, STREAMING_TXD_MIN);
	Patch(0x5A625F, STREAMING_TXD_MIN);
	Patch(0x5A6393, STREAMING_TXD_MIN);
	Patch(0x5A64F0, STREAMING_TXD_MIN);

	Patch(0x5B5097, STREAMING_TXD_MIN);
	Patch(0x5B62CF, STREAMING_TXD_MIN);

	Patch(0x6D65D3, STREAMING_TXD_MIN);

	Patch(0x731A5B, STREAMING_TXD_MIN);
	Patch(0x731F02, STREAMING_TXD_MIN);

	Patch(0x408F67, -(STREAMING_TXD_MIN * 3));
	Patch(0x409B1B, -(STREAMING_TXD_MIN * 3));

	// col min
	for (char* ptr = (char*)0x408901; ptr < (char*)0x410E8C; ptr++)
	{
		int minDigit = -0x61A8;

		if (!memcmp(ptr, &minDigit, 4))
		{
			Patch(ptr, -STREAMING_COL_MIN);
			ptr += 4;
		}

		if (!memcmp(ptr, "\xA8\x61\x00\x00", 4))
		{
			Patch(ptr, STREAMING_COL_MIN);
			ptr += 4;
		}
	}

	Patch(0x5B6314, STREAMING_COL_MIN);

	// ipl min
	for (char* ptr = (char*)0x4044F5; ptr < (char*)0x40E2A5; ptr++)
	{
		int minDigit = -0x62A7;

		if (!memcmp(ptr, &minDigit, 4))
		{
			Patch(ptr, -STREAMING_IPL_MIN);
			ptr += 4;
		}

		if (!memcmp(ptr, "\xA7\x62\x00\x00", 4))
		{
			Patch(ptr, STREAMING_IPL_MIN);
			ptr += 4;
		}
	}

	Patch(0x5B6359, STREAMING_IPL_MIN);

	// node min
	for (char* ptr = (char*)0x4044F5; ptr < (char*)0x450CFA; ptr++)
	{
		int minDigit = -0x63A7;

		if (!memcmp(ptr, &minDigit, 4))
		{
			Patch(ptr, -STREAMING_NODE_MIN);
			ptr += 4;
		}

		if (!memcmp(ptr, "\xA7\x63\x00\x00", 4))
		{
			Patch(ptr, STREAMING_NODE_MIN);
			ptr += 4;
		}
	}

	Patch(0x5B6396, STREAMING_NODE_MIN);

	// ifp min, sadly we need to scan across almost the entire exe for this
	for (char* ptr = (char*)0x407125; ptr < (char*)0x6930DD; ptr++)
	{
		int minDigit = -0x63E7;

		// as otherwise we overwrite a CALL
		if (ptr <= (char*)0x40F000)
		{
			if (!memcmp(ptr, &minDigit, 4))
			{
				Patch(ptr, -STREAMING_IFP_MIN);
				ptr += 4;
			}
		}

		if (!memcmp(ptr, "\xE7\x63\x00\x00", 4))
		{
			Patch(ptr, STREAMING_IFP_MIN);
			ptr += 4;
		}
	}

	// rrr min
	for (char* ptr = (char*)0x408A92; ptr < (char*)0x45A5FA; ptr++)
	{
		int minDigit = -0x649B;

		if (!memcmp(ptr, &minDigit, 4))
		{
			Patch(ptr, -STREAMING_RRR_MIN);
			ptr += 4;
		}

		if (!memcmp(ptr, "\x9B\x64\x00\x00", 4))
		{
			Patch(ptr, STREAMING_RRR_MIN);
			ptr += 4;
		}
	}

	Patch(0x5B63F1, STREAMING_RRR_MIN);

	// streamed scripts min
	for (char* ptr = (char*)0x408A92; ptr < (char*)0x5E8686; ptr++)
	{
		int minDigit = -0x6676;

		if (!memcmp(ptr, &minDigit, 4))
		{
			Patch(ptr, -STREAMING_SCM_MIN);
			ptr += 4;
		}

		if (!memcmp(ptr, "\x76\x66\x00\x00", 4))
		{
			Patch(ptr, STREAMING_SCM_MIN);
			ptr += 4;
		}
	}

	// img entry list
	char* imgEntryList = new char[20 * (STREAMING_SCM_MAX + 5)];
	memset(imgEntryList, 0, 20 * (STREAMING_SCM_MAX + 5));

	g_imgEntryList = imgEntryList;

	for (char* ptr = (char*)0x401000; ptr < (char*)0x67FB30; ptr++)
	{
		if (*(uint32_t*)ptr >= 0x8E4CC0 && *(uint32_t*)ptr <= (0x8E4CC0 + 20))
		{
			uint32_t v = *(uint32_t*)ptr;
			v -= 0x8E4CC0;
			v += (uintptr_t)imgEntryList;

			Patch(ptr, v);
			ptr += 4;
		}

		if (*(uint32_t*)ptr == 0x946750)
		{
			Patch(ptr, &imgEntryList[20 * STREAMING_MODEL_MAX] + 16);
		}

		if (*(uint32_t*)ptr == 0x964E08)
		{
			Patch(ptr, &imgEntryList[20 * (STREAMING_SCM_MIN)] + 16);
			ptr += 4;
		}
	}

	Patch(0x5B8C51, &imgEntryList[20 * STREAMING_MODEL_MAX] + 6);
	Patch(0x5B8AFC, &imgEntryList[20 * (STREAMING_SCM_MAX + 5)]);
	Patch(0x5B8D8E, 96036);
	Patch(0x5B8C64, &imgEntryList[20 * (STREAMING_MODEL_MAX + 1)] + 16);
	Patch(0x40CDFA, &imgEntryList[20 * 7] + 16);
	Patch(0x408FBB, &imgEntryList[20 * 7] + 16);
	Patch(0x67B5C2, &imgEntryList[20 * (370)] + 16); // jetpack
	Patch(0x410B32, &imgEntryList[20 * (STREAMING_COL_MIN + 1)] + 16);
	Patch(0x410BE0, &imgEntryList[20 * (STREAMING_COL_MAX)] + 16);

	// temp: modelinfoptrs array init, we haven't expanded it yet
	//Patch(0x4C6816, 20000);

	// model info ptrs
	void** modelInfoPtrs = new void*[STREAMING_MODEL_MAX];
	memset(modelInfoPtrs, 0, sizeof(void*) * STREAMING_MODEL_MAX);

	Patch(0x5B92B0, &modelInfoPtrs[STREAMING_MODEL_MAX]); // ConvertAnimFileIndex iteration

	for (char* ptr = (char*)0x401000; ptr < (char*)0x856F00; ptr++)
	{
		if (*(uint32_t*)ptr == 0xA9B0C8 || *(uint32_t*)ptr == 0xA9B0CC)
		{
			uint32_t v = *(uint32_t*)ptr;
			v -= 0xA9B0C8;
			v += (uintptr_t)modelInfoPtrs;

			Patch(ptr, v);
			ptr += 4;
		}

		if (*(uint32_t*)ptr >= 0xA9B6F0 && *(uint32_t*)ptr <= 0xA9B700)
		{
			uint32_t v = *(uint32_t*)ptr;
			v -= 0xA9B0C8;
			v += (uintptr_t)modelInfoPtrs;

			Patch(ptr, v);
			ptr += 4;
		}
	}

	Patch(0x5A55B3, &modelInfoPtrs[383]);
	Patch(0x67B5E7, &modelInfoPtrs[370]);
	Patch(0x6CC3DD, &modelInfoPtrs[425]);

	Patch(0x4C687D, &modelInfoPtrs[374]);
	Patch(0x4C68CA, &modelInfoPtrs[375]);
	Patch(0x4C690E, &modelInfoPtrs[376]);
	Patch(0x4C694D, &modelInfoPtrs[377]);
	Patch(0x4C698C, &modelInfoPtrs[378]);
	Patch(0x4C69CB, &modelInfoPtrs[379]);
	Patch(0x4C6A0A, &modelInfoPtrs[380]);
	Patch(0x4C6A49, &modelInfoPtrs[381]);

	Patch(0x61BE26, &modelInfoPtrs[396]);
	Patch(0x61BE2E, &modelInfoPtrs[394]);

	Patch(0x61BE42, &modelInfoPtrs[397]);
	Patch(0x61BE4A, &modelInfoPtrs[395]);

	Patch(0x61BE6D, &modelInfoPtrs[396]);
	Patch(0x61BE7A, &modelInfoPtrs[397]);

	Patch(0x61BE86, &modelInfoPtrs[394]);
	Patch(0x61BE93, &modelInfoPtrs[395]);

	// some dummy model index references
	Patch(0x5B8B18, &imgEntryList[20 * (STREAMING_SCM_MAX)]);
	Patch(0x5B8B22, &imgEntryList[20 * (STREAMING_SCM_MAX + 1)]);
	Patch(0x5B8B2C, &imgEntryList[20 * (STREAMING_SCM_MAX + 2)]);
	Patch(0x5B8B36, &imgEntryList[20 * (STREAMING_SCM_MAX + 3)]);

	InjectHook(0x5B8B3A, AtRuntimeInitDummies, PATCH_JUMP);

	// txd pool size
	Patch(0x731F60, 8000);

	Patch<uint8_t>(0x408E48, 0xB7); // replace movsx with movzx
	Patch<uint8_t>(0x4074C7, 0xB7);
	Patch<uint8_t>(0x408E8C, 0xB7);
	Patch<uint8_t>(0x408EC8, 0xB7);
	Patch<uint8_t>(0x408F6F, 0xB7);
	Patch<uint8_t>(0x4074E1, 0xB7);
	Patch<uint8_t>(0x4074F7, 0xB7);
	Patch<uint8_t>(0x40CEDC, 0xB7);
	Patch<uint8_t>(0x40CD94, 0xB7);
	Patch<uint8_t>(0x408874, 0xB7);
	Patch<uint8_t>(0x40885F, 0xB7);
	Patch<uint8_t>(0x40EBE7, 0xB7);
	Patch<uint8_t>(0x40EC2E, 0xB7);
	Patch<uint8_t>(0x409BB1, 0xB7);
	Patch<uint8_t>(0x409B40, 0xB7);
	Patch<uint8_t>(0x409B25, 0xB7);
	Patch<uint8_t>(0x409AAD, 0xB7);
	Patch<uint8_t>(0x40C1FB, 0xB7);
	Patch<uint8_t>(0x40C238, 0xB7);
	Patch<uint8_t>(0x40A8B2, 0xB7);
	Patch<uint8_t>(0x40A97A, 0xB7);
	Patch<uint8_t>(0x408B23, 0xB7);
	Patch<uint8_t>(0x408B3F, 0xB7);
	Patch<uint8_t>(0x40B211, 0xB7);
	Patch<uint8_t>(0x40B2CD, 0xB7);
	Patch<uint8_t>(0x407AEB, 0xB7);
	Patch<uint8_t>(0x407B49, 0xB7);
	Patch<uint8_t>(0x40D048, 0xB7);
	Patch<uint8_t>(0x40D12A, 0xB7);
	Patch<uint8_t>(0x40CFED, 0xB7);
	Patch<uint8_t>(0x40D12A, 0xB7);
	Patch<uint8_t>(0x532B79, 0xB7);
	Patch<uint8_t>(0x532CC1, 0xB7);
	Patch<uint8_t>(0x532CD2, 0xB7);
	Patch<uint8_t>(0x40E4F9, 0xB7);
	Patch<uint8_t>(0x40E524, 0xB7);

	// cmp 0FFFFFFFF to 0FFFF (won't fit inline as >0x66 prefix for WORD)
	InjectHook(0x40CD10, CmpEsi40CD10, PATCH_JUMP);
	InjectHook(0x408F72, CmpEsi408F72, PATCH_JUMP);
	//InjectHook(0x40CD10, CmpEsi40CD10, PATCH_JUMP);

	// Heap corruption fix
	Nop(0x5C25D3, 5);

	// misc changes for pools
	Patch(0x411458, 512); // collision file pool
	Patch(0x551107, 30000); // colmodel
	Patch(0x55105F, 20000); // building
	Patch(0x405F26, 256); // ipl
	Patch(0x550F82, 0xc80 * 2); // ptrnode double
	Patch(0x550F46, 0x11170 * 2); // ptrnode single
	Patch(0x5510CF, 0x9c4 * 2); // dummys
	Patch(0x550FBA, 0x1F4 * 3); // entryinfo node

	Patch<float>(0x411413, -8000.f);
	Patch<float>(0x41141B, -8000.f);
	Patch<float>(0x411423, 8000.f);
	Patch<float>(0x41142B, 8000.f);

	// collision file limit in scanning
	Patch(0x410EAF, 512 * 44);
	Patch(0x410CC6, 512 * 44);
	Patch(0x410AA6, 512 * 44);
	Patch(0x410F6A, 512 * 44);

	// colaccel allocs, related to above
	Patch(0x5B31AB, 512);
	Patch(0x5B31BB, 512 * 0x34);
	Patch(0x5B31F8, 512 * 24);

	Patch(0x5B31DE, 512); // loop counters
	Patch(0x5B3211, 512);

	// some byte cutoff in setting of model range
	Patch<uint16_t>(0x5B50F4, 0x8B90); // replace a mov byte ptr with a mov dword ptr

	// static ipl limit thing
	void* IplEntityArrayList = new void*[256];

	for (char* ptr = (char*)0x404000; ptr < (char*)0x407000; ptr++)
	{
		if (!memcmp(ptr, "\x08\x3F\x8E\x00", 4))
		{
			Patch(ptr, IplEntityArrayList);
			ptr += 4;
		}
	}
}