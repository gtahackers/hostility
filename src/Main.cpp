#include "StdInc.h"
#include <functional>

void PatchPathDivisors();
void PatchStreamingIndices();
void PatchTimedObjects();

#define SECTOR_SIZE 50
#define LOD_SECTOR_SIZE 200
#define SECTOR_MIN_COORD -8000.0f
#define SECTOR_MAX_COORD 8000.0f
//#define SECTOR_MIN_COORD -3000.0f
//#define SECTOR_MAX_COORD 3000.0f

CSector* g_sectors;
//CStreamSectorDynamic* g_repeatSectors;
CRepeatSector* g_repeatSectors = (CRepeatSector*)0xB992B8;
int g_sectorCount;
int g_sectorCount1;
int g_lodSectorCount;
int g_lodSectorCount1;
int g_lodSectorOffset;
int g_sectorOffset;

uint16_t& CWorld__ms_currentScanCode = *(uint16_t*)0xB7CD78;

void WRAPPER CWorld__ClearScanCodes() { EAXJMP(0x563470); }

__forceinline void IncrementScanCode()
{
	if (CWorld__ms_currentScanCode >= UINT16_MAX)
	{
		CWorld__ClearScanCodes();
		CWorld__ms_currentScanCode = 1;
	}
	else
	{
		CWorld__ms_currentScanCode++;
	}
}

__forceinline void ValidateSectorPos(int& x, int& y)
{
	if (x >= g_sectorCount)
	{
		x = g_sectorCount - 1;
	}

	if (x < 0)
	{
		x = 0;
	}

	if (y >= g_sectorCount)
	{
		y = g_sectorCount - 1;
	}

	if (y < 0)
	{
		y = 0;
	}
}

#define SECTOR_COORD_TO_OFFSET(x) ((int)(((x) / SECTOR_SIZE) + g_sectorOffset))

struct SectorPos
{
	int x, y;

	SectorPos(int xx, int yy)
		: x(xx), y(yy)
	{

	}
};

inline SectorPos GetSectorPos(int x, int y)
{
	ValidateSectorPos(x, y);

	return SectorPos(x, y);
}

inline SectorPos GetSectorPos(float xc, float yc)
{
	int x = SECTOR_COORD_TO_OFFSET(xc);
	int y = SECTOR_COORD_TO_OFFSET(yc);

	ValidateSectorPos(x, y);

	return SectorPos(x, y);
}

inline CSector& GetSector(int x, int y)
{
	ValidateSectorPos(x, y);

	return g_sectors[(y * g_sectorCount) + x];
}

inline CSector& GetSector(float x, float y)
{
	return GetSector(SECTOR_COORD_TO_OFFSET(x), SECTOR_COORD_TO_OFFSET(y));
}

inline CRepeatSector& GetLegacyRepeatSector(int x, int y)
{
	ValidateSectorPos(x, y);

	x %= 16;
	y %= 16;

	return g_repeatSectors[(y * 16) + x];
}

inline CRepeatSector& GetRepeatSector(int x, int y)
{
	ValidateSectorPos(x, y);

	/*x -= g_sectorOffset;
	x += 60;

	if (x < 0)
	{
		x = 0;
	}

	if (x >= 120)
	{
		x = 119;
	}

	y -= g_sectorOffset;
	y += 60;

	if (y < 0)
	{
		y = 0;
	}

	if (y >= 120)
	{
		y = 119;
	}*/

	x %= 16;
	y %= 16;

	return g_repeatSectors[(y * 16) + x];
}

CPtrList* g_lodSectors;

inline CPtrList& GetLodSector(int x, int y)
{
	return g_lodSectors[(y * g_lodSectorCount) + x];
}

inline CRepeatSector& GetRepeatSector(float x, float y)
{
	return GetRepeatSector(SECTOR_COORD_TO_OFFSET(x), SECTOR_COORD_TO_OFFSET(y));
}

CSector& CWorld__GetSector(int x, int y)
{
	return GetSector(x, y);
}

void ForEachEntity(CPtrNodeSingle* ptrNode, std::function<void(CEntity*)> function)
{
	for (; ptrNode; ptrNode = ptrNode->next)
	{
		if (!ptrNode->entity)
		{
			continue;
		}

		// don't operate on destructed remnants
		if (*(uintptr_t*)ptrNode->entity != 0x863C40)
		{
			function(ptrNode->entity);
		}
	}
}

int& CGame__currArea = *(int*)0xB72914;

void CStreaming__InstanceLoadedModelsInSectorList(CPtrList& ptrNode)
{
	ForEachEntity(ptrNode.sNode, [] (CEntity* entity)
	{
		if (entity->m_areaCode == CGame__currArea || entity->m_areaCode == 13)
		{
			if (!entity->m_pRwObject)
			{
				entity->CreateRwObject();
			}
		}
	});
}

void CStreaming__InstanceLoadedModels(const CVector& pos)
{
	float size = (CGame__currArea > 0) ? 40.0f : 80.0f;

	float minX = pos.x - size;
	float minY = pos.y - size;

	float maxX = pos.x + size;
	float maxY = pos.y + size;

	IncrementScanCode();

	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			CStreaming__InstanceLoadedModelsInSectorList(staticSector.building);
			CStreaming__InstanceLoadedModelsInSectorList(staticSector.dummy);
			CStreaming__InstanceLoadedModelsInSectorList(dynamicSector.object);
		}
	}
}

void WRAPPER CStreaming__ProcessEntitiesInSectorList(CPtrNodeSingle* ptrNode, float x, float y, float minx, float miny, float maxx, float maxy, float size, uint32_t a2) { EAXJMP(0x40C270); }
void WRAPPER CStreaming__ProcessEntitiesInSectorList(CPtrNodeSingle* ptrNode, uint32_t a2) { EAXJMP(0x40C450); }

void CStreaming__AddModelsToRequestList(const CVector& pos, uint32_t a2)
{
	float size = (CGame__currArea > 0) ? 40.0f : 80.0f;

	float minX = pos.x - size;
	float minY = pos.y - size;

	float maxX = pos.x + size;
	float maxY = pos.y + size;

	SectorPos baseSector = GetSectorPos(pos.x, pos.y);
	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	int sizeSectors = ((int)size) / SECTOR_SIZE;
	int sizeLen = (sizeSectors - 1) * (sizeSectors - 1);
	int sizeLen2 = (sizeSectors + 2) * (sizeSectors + 2);

	IncrementScanCode();

	int xLen = minSector.x - baseSector.x;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		int yLen = minSector.y - baseSector.y;
		int xLenMul = xLen * xLen;

		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			int len = xLenMul + (yLen * yLen);

			if (len > sizeLen)
			{
				if (len < sizeLen2)
				{
					CStreaming__ProcessEntitiesInSectorList(staticSector.building, pos.x, pos.y, minX, minY, maxX, maxY, size, a2);
					CStreaming__ProcessEntitiesInSectorList(staticSector.dummy, pos.x, pos.y, minX, minY, maxX, maxY, size, a2);
					CStreaming__ProcessEntitiesInSectorList(dynamicSector.object, pos.x, pos.y, minX, minY, maxX, maxY, size, a2);
				}
			}
			else
			{
				CStreaming__ProcessEntitiesInSectorList(staticSector.building, a2);
				CStreaming__ProcessEntitiesInSectorList(staticSector.dummy, a2);
				CStreaming__ProcessEntitiesInSectorList(dynamicSector.object, a2);
			}

			yLen++;
		}

		xLen++;
	}
}

void CStreaming__DeleteRwObjectsInSectorList(CPtrList& list)
{
	ForEachEntity(list.sNode, [] (CEntity* entity)
	{
		if (!entity->bImBeingRendered && !entity->bStreamingDontDelete)
		{
			entity->DeleteRwObject();
		}
	});
}

void CStreaming__DeleteAllRwObjects()
{
	// unused stuff using camera not reused
	IncrementScanCode();

	for (int x = 0; x < g_sectorCount; x++)
	{
		for (int y = 0; y < g_sectorCount; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			CStreaming__DeleteRwObjectsInSectorList(staticSector.building);
			CStreaming__DeleteRwObjectsInSectorList(staticSector.dummy);
			CStreaming__DeleteRwObjectsInSectorList(dynamicSector.object);
		}
	}
}

void CStreaming__DeleteRwObjectsAfterDeath(CVector* vector)
{
	IncrementScanCode();

	SectorPos sectorPos = GetSectorPos(vector->x, vector->y);

	for (int x = sectorPos.x - 3; x <= sectorPos.x + 3; x++)
	{
		for (int y = sectorPos.y - 3; y <= sectorPos.y + 3; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			CStreaming__DeleteRwObjectsInSectorList(staticSector.building);
			CStreaming__DeleteRwObjectsInSectorList(staticSector.dummy);
			CStreaming__DeleteRwObjectsInSectorList(dynamicSector.object);
		}
	}
}

int& CStreaming__ms_memoryUsed = *(int*)0x8E4CB4;

inline CVector* GetCameraPos()
{
	if (*(DWORD*)0xB6F03C)
	{
		return (CVector*)0xB6F02C;
	}
	
	return (CVector*)((*(DWORD*)0xB6F03C) + 48);
}

inline CMatrix* GetCameraMatrix()
{
	return (CMatrix*)0xB6F99C;
}

bool WRAPPER _CleanTexDictionaries(int memoryCap) { EAXJMP(0x40D2F0); }
bool WRAPPER CStreaming__DeleteRwObjectsNotInFrustumInSectorList(CPtrNodeSingle* ptr, int memoryCap) { EAXJMP(0x4099E0); }
bool WRAPPER CStreaming__DeleteRwObjectsBehindCameraInSectorList(CPtrNodeSingle* ptr, int memoryCap) { EAXJMP(0x409940); }

#define BREAKON(x) if (x) return;

void __forceinline DoCleanup(int a1, int x1, int x2, int y1, int y2, int step)
{
	IncrementScanCode();

	for (int y = y1; y != y2; y++)
	{
		for (int x = x1; x != x2; x += step)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			BREAKON(CStreaming__DeleteRwObjectsNotInFrustumInSectorList(staticSector.building, a1));
			BREAKON(CStreaming__DeleteRwObjectsNotInFrustumInSectorList(staticSector.dummy, a1));
			BREAKON(CStreaming__DeleteRwObjectsNotInFrustumInSectorList(dynamicSector.object, a1));
		}
	}

	_CleanTexDictionaries(a1);

	for (int y = y1; y != y2; y++)
	{
		for (int x = x1; x != x2; x += step)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			BREAKON(CStreaming__DeleteRwObjectsBehindCameraInSectorList(staticSector.building, a1));
			BREAKON(CStreaming__DeleteRwObjectsBehindCameraInSectorList(staticSector.dummy, a1));
			BREAKON(CStreaming__DeleteRwObjectsBehindCameraInSectorList(dynamicSector.object, a1));
		}
	}

	MessageBox(NULL, L"this is bad", L"streaming cleanup didn't really woahk", MB_OK | MB_ICONSTOP);
}

void CStreaming__DeleteRwObjectsBehindCamera(int a1)
{
	if (CStreaming__ms_memoryUsed < a1)
	{
		return;
	}

	CVector* cameraPos = GetCameraPos();
	auto camSector = GetSectorPos(cameraPos->x, cameraPos->y);

	CMatrix* cameraMatrix = GetCameraMatrix();

	if (fabs(cameraMatrix->top.y) < fabs(cameraMatrix->top.x))
	{
		// establish a sector range behind the camera
		int y1 = camSector.y - 10;
		int y2 = camSector.y + 10;

		int x1;
		int x2;

		int step;

		if (cameraMatrix->top.x <= 0.0f)
		{
			x1 = camSector.x + 2;
			x2 = camSector.x + 10;

			step = -1;
		}
		else
		{
			x1 = camSector.x - 2;
			x2 = camSector.x - 10;

			step = 1;
		}

		ValidateSectorPos(x1, y1);
		ValidateSectorPos(x2, y2);

		if (x1 == x2)
		{
			if (cameraMatrix->top.x <= 0.0f)
			{
				x1 = camSector.x + 2;
				x2 = camSector.x - 10;

				step = -1;
			}
			else
			{
				x1 = camSector.x - 2;
				x2 = camSector.x + 10;

				step = 1;
			}

			ValidateSectorPos(x1, y1);
			ValidateSectorPos(x2, y2);

			if (x1 == x2)
			{
				// some sort of 'easy cleanup'
				_CleanTexDictionaries(a1);
			}
			else
			{
				DoCleanup(a1, x1, x2, y1, y2, step);
			}
		}
		else
		{
			DoCleanup(a1, x1, x2, y1, y2, step);
		}
	}
	else
	{
		// establish a sector range behind the camera
		int x1 = camSector.x - 10;
		int x2 = camSector.x + 10;

		int y1;
		int y2;

		int step;

		if (cameraMatrix->top.y <= 0.0f)
		{
			y1 = camSector.y + 2;
			y2 = camSector.y + 10;

			step = -1;
		}
		else
		{
			y1 = camSector.y - 2;
			y2 = camSector.y - 10;

			step = 1;
		}

		ValidateSectorPos(x1, y1);
		ValidateSectorPos(x2, y2);

		if (y1 == y2)
		{
			if (cameraMatrix->top.y <= 0.0f)
			{
				y1 = camSector.y + 2;
				y2 = camSector.y - 10;

				step = -1;
			}
			else
			{
				y1 = camSector.y - 2;
				y2 = camSector.y + 10;

				step = 1;
			}

			ValidateSectorPos(x1, y1);
			ValidateSectorPos(x2, y2);

			if (y1 == y2)
			{
				// some sort of 'easy cleanup'
				_CleanTexDictionaries(a1);
			}
			else
			{
				DoCleanup(a1, x1, x2, y1, y2, step);
			}
		}
		else
		{
			DoCleanup(a1, x1, x2, y1, y2, step);
		}
	}
}

int& gnTop = *(int*)0x965590;
int& gnRight = *(int*)0x965594;
int& gnBottom = *(int*)0x965598;
int& gnLeft = *(int*)0x96559C;

int CCollision__BuildCacheOfCameraCollisionHookDo(float* coordinates, int* outStuff)
{
	SectorPos sec1 = GetSectorPos(coordinates[0], coordinates[1]);
	SectorPos sec2 = GetSectorPos(coordinates[3], coordinates[4]);

	gnTop = sec2.y;
	gnRight = sec2.x;
	gnBottom = sec1.y;
	gnLeft = sec1.x;

	outStuff[0] = gnLeft;
	outStuff[1] = gnTop;
	outStuff[2] = gnRight;

	return gnBottom;
}

void __declspec(naked) CCollision__BuildCacheOfCameraCollisionHook()
{
	__asm
	{
		lea eax, [esp + 70h - 1Ch]
		lea ecx, [esp + 70h - 5Ch]

		push ecx
		push eax

		call CCollision__BuildCacheOfCameraCollisionHookDo

		add esp, 8h

		mov edi, eax

		cmp word ptr ds:[0B7CD78h], 0FFFFh
	}

	EAXJMP(0x41AE42);
}

static DWORD _41A83B = 0x41A83B;
static DWORD _41A856 = 0x41A856;

void __declspec(naked) CCollision__CheckCameraCollisionBuildings_clampX()
{
	__asm
	{
		cmp ecx, g_sectorCount1
		jl justReturn

		mov ecx, g_sectorCount1

	justReturn:
		jmp _41A83B
	}
}

void __declspec(naked) CCollision__CheckCameraCollisionBuildings_clampY()
{
	__asm
	{
		cmp eax, g_sectorCount1
		jl justReturn

		mov eax, g_sectorCount1

	justReturn:
		imul eax, g_sectorCount
		jmp _41A856
	}
}

inline SectorPos GetLodSectorPos(float x, float y)
{
	int xx = (x / LOD_SECTOR_SIZE) + g_lodSectorOffset;
	int yy = (y / LOD_SECTOR_SIZE) + g_lodSectorOffset;

	if (xx < 0)
	{
		xx = 0;
	}

	if (yy < 0)
	{
		yy = 0;
	}

	if (xx >= g_lodSectorCount)
	{
		xx = g_lodSectorCount - 1;
	}

	if (yy >= g_lodSectorCount)
	{
		yy = g_lodSectorCount - 1;
	}

	return SectorPos(xx, yy);
}

void WRAPPER CPtrList::Add(CEntity* entity) { EAXJMP(0x5335E0); }
void WRAPPER CPtrList::AddDouble(CEntity* entity) { EAXJMP(0x533670); }

CRect NormalizeRect(const CRect& rect)
{
	int minX = (rect.x1 <= rect.x2) ? rect.x1 : rect.x2;
	int maxX = (rect.x1 > rect.x2) ? rect.x1 : rect.x2;

	int minY = (rect.y1 <= rect.y2) ? rect.y1 : rect.y2;
	int maxY = (rect.y1 > rect.y2) ? rect.y1 : rect.y2;

	return CRect(minX, minY, maxX, maxY);
}

void CEntity::Add_CRect(CRect& rectIn)
{
	CRect rect = NormalizeRect(rectIn);

	if (bIsBIGBuilding)
	{
		SectorPos pos1r = GetLodSectorPos(rect.x1, rect.y1);
		SectorPos pos2r = GetLodSectorPos(rect.x2, rect.y2);

		for (int x = pos1r.x; x <= pos2r.x; x++)
		{
			for (int y = pos1r.y; y <= pos2r.y; y++)
			{
				auto& sector = GetLodSector(x, y);

				sector.Add(this);
			}
		}

		return;
	}
	
	SectorPos pos1 = GetSectorPos(rect.x1, rect.y1);
	SectorPos pos2 = GetSectorPos(rect.x2, rect.y2);

	for (int x = pos1.x; x <= pos2.x; x++)
	{
		for (int y = pos1.y; y <= pos2.y; y++)
		{
			auto& sector = GetSector(x, y);
			auto& repeatSector = GetRepeatSector(x, y);
			auto& legacySector = GetLegacyRepeatSector(x, y);

			switch (nType)
			{
				case 1:
					sector.building.Add(this);
					break;
				case 2:
					repeatSector.vehicle.AddDouble(this);
					//legacySector.vehicle.AddDouble(this);
					break;
				case 3:
					repeatSector.ped.AddDouble(this);
					//legacySector.ped.AddDouble(this);
					break;
				case 4:
					repeatSector.object.AddDouble(this);
					//legacySector.object.AddDouble(this);
					break;
				case 5:
					sector.dummy.AddDouble(this);
					break;
			}
		}
	}
}

void WRAPPER CPtrList::Remove(CEntity* entity) { EAXJMP(0x533610); }
void WRAPPER CPtrList::RemoveDouble(CEntity* entity) { EAXJMP(0x5336B0); }

void CEntity::Remove()
{
	CRect rect = NormalizeRect(GetBoundRect());

	if (!bIsBIGBuilding)
	{
		SectorPos pos1 = GetSectorPos(rect.x1, rect.y1);
		SectorPos pos2 = GetSectorPos(rect.x2, rect.y2);

		for (int x = pos1.x; x <= pos2.x; x++)
		{
			for (int y = pos1.y; y <= pos2.y; y++)
			{
				auto& sector = GetSector(x, y);
				auto& repeatSector = GetRepeatSector(x, y);
				auto& legacySector = GetLegacyRepeatSector(x, y);

				switch (nType)
				{
					case 1:
						sector.building.Remove(this);
						break;
					case 2:
						repeatSector.vehicle.RemoveDouble(this);
						//legacySector.vehicle.RemoveDouble(this);
						break;
					case 3:
						repeatSector.ped.RemoveDouble(this);
						//legacySector.ped.RemoveDouble(this);
						break;
					case 4:
						repeatSector.object.RemoveDouble(this);
						//legacySector.object.RemoveDouble(this);
						break;
					case 5:
						sector.dummy.RemoveDouble(this);
						break;
				}
			}
		}
	}
	else
	{
		SectorPos pos1r = GetLodSectorPos(rect.x1, rect.y1);
		SectorPos pos2r = GetLodSectorPos(rect.x2, rect.y2);

		for (int x = pos1r.x; x <= pos2r.x; x++)
		{
			for (int y = pos1r.y; y <= pos2r.y; y++)
			{
				auto& sector = GetLodSector(x, y);

				sector.Remove(this);
			}
		}

		return;
	}
}

enum class Register
{
	Eax,
	Ebx,
	Ecx,
	Edx,
	Esi,
	Edi
};

#define MOVE_TO_EAX(reg) if (Reg == Register::##reg) { __asm { mov eax, reg } }
#define MOVE_FROM_EAX(reg) if (Reg == Register::##reg) { __asm { mov reg, eax } }

void genericClampDo(int* count)
{
	if (*count > g_sectorCount1)
	{
		*count = g_sectorCount1;
	}
}

template<Register Reg>
__forceinline void __declspec(naked) MoveToEax()
{
	__asm retn
}

template<Register Reg>
__forceinline void __declspec(naked) MoveFromEax()
{
	__asm retn
}

#define DEFINE_SPECIALIZATION_TO_EAX(reg) \
	template<> \
	__forceinline void __declspec(naked) MoveToEax<Register::##reg>() { \
		__asm { __asm mov eax, reg __asm retn } \
	}

DEFINE_SPECIALIZATION_TO_EAX(Ebx);
DEFINE_SPECIALIZATION_TO_EAX(Ecx);
DEFINE_SPECIALIZATION_TO_EAX(Edx);
DEFINE_SPECIALIZATION_TO_EAX(Edi);
DEFINE_SPECIALIZATION_TO_EAX(Esi);

#define DEFINE_SPECIALIZATION_FROM_EAX(reg) \
	template<> \
	__forceinline void __declspec(naked) MoveFromEax<Register::##reg>() { \
		__asm { __asm mov reg, eax __asm retn } \
	}

DEFINE_SPECIALIZATION_FROM_EAX(Ebx);
DEFINE_SPECIALIZATION_FROM_EAX(Ecx);
DEFINE_SPECIALIZATION_FROM_EAX(Edx);
DEFINE_SPECIALIZATION_FROM_EAX(Edi);
DEFINE_SPECIALIZATION_FROM_EAX(Esi);

template<bool DoMul>
__forceinline void __declspec(naked) MulSectorCount()
{
	__asm retn
}

template<>
__forceinline void __declspec(naked) MulSectorCount<true>()
{
	__asm
	{
		imul eax, g_sectorCount
		retn
	}
}

template<Register Reg, bool IsY>
void __declspec(naked) genericClamp()
{
	__asm
	{
		// our scratch register, unless eax is the actual register we clamp
		push ecx
		push eax
	}

	MoveToEax<Reg>();

	__asm
	{
		push eax
		lea eax, dword ptr [esp]
		push eax
		call genericClampDo
		add esp, 4h
		pop eax
	}

	MulSectorCount<IsY>();

	static int tempEax, tempEcx;

	MoveFromEax<Reg>();

	// as the following if may generate a spurious xor eax, eax in order to fulfill a jump
	__asm mov tempEax, eax
	__asm mov tempEcx, ecx

	if (Reg == Register::Eax)
	{
		__asm
		{
			add esp, 4h
			pop ecx

			mov eax, tempEax
		}
	}
	else if (Reg == Register::Ecx)
	{
		__asm
		{
			pop eax
			add esp, 4h

			mov ecx, tempEcx
		}
	}
	else
	{
		__asm
		{
			pop eax
			pop ecx
		}
	}

	__asm
	{
		retn
	}
}

void __declspec(naked) ProcessCollisionSectorList_EvilTail()
{
	__asm
	{
		fld [esp + 38h]

		retn
	}
}

void WRAPPER CWorld__FindObjectsInRangeSectorList(CPtrNodeSingle*, CVector& pos, float size, bool a3, uint16_t* a4, uint16_t a5, CEntity*& a6) { EAXJMP(0x563500); }

void CWorld__FindObjectsInRange(CVector& pos, float size, bool a3, uint16_t* a4, uint16_t a5, CEntity*& a6, bool building, bool vehicle, bool ped, bool object, bool dummy)
{
	float minX = pos.x - size;
	float minY = pos.y - size;

	float maxX = pos.x + size;
	float maxY = pos.y + size;

	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	IncrementScanCode();

	*a4 = 0;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			if (building)
			{
				CWorld__FindObjectsInRangeSectorList(staticSector.building, pos, size, a3, a4, a5, a6);
			}

			if (vehicle)
			{
				CWorld__FindObjectsInRangeSectorList(dynamicSector.vehicle, pos, size, a3, a4, a5, a6);
			}

			if (ped)
			{
				CWorld__FindObjectsInRangeSectorList(dynamicSector.ped, pos, size, a3, a4, a5, a6);
			}

			if (object)
			{
				CWorld__FindObjectsInRangeSectorList(dynamicSector.object, pos, size, a3, a4, a5, a6);
			}

			if (dummy)
			{
				CWorld__FindObjectsInRangeSectorList(staticSector.dummy, pos, size, a3, a4, a5, a6);
			}
		}
	}
}

void WRAPPER CWorld__FindObjectsOfTypeInRangeSectorList(int, CPtrNodeSingle*, CVector& pos, float size, bool a3, uint16_t* a4, uint16_t a5, CEntity*& a6) { EAXJMP(0x5635C0); }

void CWorld__FindObjectsOfTypeInRange(int a1, CVector& pos, float size, bool a4, uint16_t* a5, uint16_t a6, CEntity*& a7, bool building, bool vehicle, bool ped, bool object, bool dummy)
{
	float minX = pos.x - size;
	float minY = pos.y - size;

	float maxX = pos.x + size;
	float maxY = pos.y + size;

	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	IncrementScanCode();

	*a5 = 0;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			if (building)
			{
				CWorld__FindObjectsOfTypeInRangeSectorList(a1, staticSector.building, pos, size, a4, a5, a6, a7);
			}

			if (vehicle)
			{
				CWorld__FindObjectsOfTypeInRangeSectorList(a1, dynamicSector.vehicle, pos, size, a4, a5, a6, a7);
			}

			if (ped)
			{
				CWorld__FindObjectsOfTypeInRangeSectorList(a1, dynamicSector.ped, pos, size, a4, a5, a6, a7);
			}

			if (object)
			{
				CWorld__FindObjectsOfTypeInRangeSectorList(a1, dynamicSector.object, pos, size, a4, a5, a6, a7);
			}

			if (dummy)
			{
				CWorld__FindObjectsOfTypeInRangeSectorList(a1, staticSector.dummy, pos, size, a4, a5, a6, a7);
			}
		}
	}
}

struct CColLine
{
public:
	CVector from;
private:
	float pad;
public:
	CVector to;
private:
	float pad2;
};

void WRAPPER CWorld__ProcessVerticalLineSector(CSector& sector, CRepeatSector& repeatSector, CColLine& colLine, int, int, bool, bool, bool, bool, bool, int, int) { EAXJMP(0x564500); }

void CWorld__ProcessVerticalLine(const CVector& pos, float targetZ, int a3, int a4, bool a5, bool a6, bool a7, bool a8, bool a9, int a10, int a11)
{
	IncrementScanCode();

	auto& staticSector = GetSector(pos.x, pos.y);
	auto& dynamicSector = GetRepeatSector(pos.x, pos.y);

	CColLine line;
	line.from = pos;
	line.to = pos;

	line.to.z = targetZ;

	return CWorld__ProcessVerticalLineSector(staticSector, dynamicSector, line, a3, a4, a5, a6, a7, a8, a9, a10, a11);
}

void WRAPPER CWorld__FindObjectsKindaCollidingSectorList(CPtrNodeSingle*, CVector& pos, float size, bool a3, uint16_t* a4, uint16_t a5, CEntity*& a6) { EAXJMP(0x565000); }

void CWorld__FindObjectsKindaColliding(CVector& pos, float size, bool a4, uint16_t* a5, uint16_t a6, CEntity*& a7, bool building, bool vehicle, bool ped, bool object, bool dummy)
{
	float minX = pos.x - size;
	float minY = pos.y - size;

	float maxX = pos.x + size;
	float maxY = pos.y + size;

	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	IncrementScanCode();

	*a5 = 0;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			if (building)
			{
				CWorld__FindObjectsKindaCollidingSectorList(staticSector.building, pos, size, a4, a5, a6, a7);
			}

			if (vehicle)
			{
				CWorld__FindObjectsKindaCollidingSectorList(dynamicSector.vehicle, pos, size, a4, a5, a6, a7);
			}

			if (ped)
			{
				CWorld__FindObjectsKindaCollidingSectorList(dynamicSector.ped, pos, size, a4, a5, a6, a7);
			}

			if (object)
			{
				CWorld__FindObjectsKindaCollidingSectorList(dynamicSector.object, pos, size, a4, a5, a6, a7);
			}

			if (dummy)
			{
				CWorld__FindObjectsKindaCollidingSectorList(staticSector.dummy, pos, size, a4, a5, a6, a7);
			}
		}
	}
}

void WRAPPER CWorld__FindObjectsIntersectingCubeSectorList(CPtrNodeSingle*, const CVector& pos, const CVector& pos2, uint16_t* a4, uint16_t a5, CEntity*& a6) { EAXJMP(0x5650E0); }

void CWorld__FindObjectsIntersectingCube(const CVector& pos, const CVector& pos2, uint16_t* a5, uint16_t a6, CEntity*& a7, bool building, bool vehicle, bool ped, bool object, bool dummy)
{
	float minX = pos.x;
	float minY = pos.y;

	float maxX = pos2.x;
	float maxY = pos2.y;

	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	IncrementScanCode();

	*a5 = 0;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			if (building)
			{
				CWorld__FindObjectsIntersectingCubeSectorList(staticSector.building, pos, pos2, a5, a6, a7);
			}

			if (vehicle)
			{
				CWorld__FindObjectsIntersectingCubeSectorList(dynamicSector.vehicle, pos, pos2, a5, a6, a7);
			}

			if (ped)
			{
				CWorld__FindObjectsIntersectingCubeSectorList(dynamicSector.ped, pos, pos2, a5, a6, a7);
			}

			if (object)
			{
				CWorld__FindObjectsIntersectingCubeSectorList(dynamicSector.object, pos, pos2, a5, a6, a7);
			}

			if (dummy)
			{
				CWorld__FindObjectsIntersectingCubeSectorList(staticSector.dummy, pos, pos2, a5, a6, a7);
			}
		}
	}
}

void WRAPPER CWorld__FindObjectsIntersectingAngledCollisionBoxSectorList(CPtrNodeSingle*, int, int, int, int, uint16_t* a4, CEntity*& a6) { EAXJMP(0x565200); }

void CWorld__FindObjectsIntersectingAngledCollisionBox(int a1, int a2, int a3, float minX, float minY, float maxX, float maxY, int a4, uint16_t* a5, CEntity*& a6, bool building, bool vehicle, bool ped, bool object, bool dummy)
{
	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	IncrementScanCode();

	*a5 = 0;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			if (building)
			{
				CWorld__FindObjectsIntersectingAngledCollisionBoxSectorList(staticSector.building, a1, a2, a3, a4, a5, a6);
			}

			if (vehicle)
			{
				CWorld__FindObjectsIntersectingAngledCollisionBoxSectorList(dynamicSector.vehicle, a1, a2, a3, a4, a5, a6);
			}

			if (ped)
			{
				CWorld__FindObjectsIntersectingAngledCollisionBoxSectorList(dynamicSector.ped, a1, a2, a3, a4, a5, a6);
			}

			if (object)
			{
				CWorld__FindObjectsIntersectingAngledCollisionBoxSectorList(dynamicSector.object, a1, a2, a3, a4, a5, a6);
			}

			if (dummy)
			{
				CWorld__FindObjectsIntersectingAngledCollisionBoxSectorList(staticSector.dummy, a1, a2, a3, a4, a5, a6);
			}
		}
	}
}

void WRAPPER CWorld__FindNearestObjectOfTypeSectorList(int a1, CPtrNodeSingle*, const CVector&, float, bool, CEntity*&, float*) { EAXJMP(0x565450); }

CEntity* CWorld__FindNearestObjectOfType(int a1, CVector& pos, float size, bool a4, bool building, bool vehicle, bool ped, bool object, bool dummy)
{
	float minX = pos.x - size;
	float minY = pos.y - size;

	float maxX = pos.x + size;
	float maxY = pos.y + size;

	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	IncrementScanCode();

	CEntity* entity = nullptr;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			if (building)
			{
				CWorld__FindNearestObjectOfTypeSectorList(a1, staticSector.building, pos, size, a4, entity, &pos.x);
			}

			if (vehicle)
			{
				CWorld__FindNearestObjectOfTypeSectorList(a1, dynamicSector.vehicle, pos, size, a4, entity, &pos.x);
			}

			if (ped)
			{
				CWorld__FindNearestObjectOfTypeSectorList(a1, dynamicSector.ped, pos, size, a4, entity, &pos.x);
			}

			if (object)
			{
				CWorld__FindNearestObjectOfTypeSectorList(a1, dynamicSector.object, pos, size, a4, entity, &pos.x);
			}

			if (dummy)
			{
				CWorld__FindNearestObjectOfTypeSectorList(a1, staticSector.dummy, pos, size, a4, entity, &pos.x);
			}
		}
	}

	return entity;
}

int WRAPPER CWorld__TestSphereAgainstSectorList(CPtrNodeSingle*, CVector, float, int, bool) { EAXJMP(0x566140); }

int CWorld__TestSphereAgainstWorld(CVector pos, float size, int a4, bool building, bool vehicle, bool ped, bool object, bool dummy, bool a10)
{
	float minX = pos.x - size;
	float minY = pos.y - size;

	float maxX = pos.x + size;
	float maxY = pos.y + size;

	SectorPos minSector = GetSectorPos(minX, minY);
	SectorPos maxSector = GetSectorPos(maxX, maxY);

	IncrementScanCode();

	int retval = 0;

	for (int x = minSector.x; x <= maxSector.x; x++)
	{
		for (int y = minSector.y; y <= maxSector.y; y++)
		{
			auto& staticSector = GetSector(x, y);
			auto& dynamicSector = GetRepeatSector(x, y);

			if (building)
			{
				if ((retval = CWorld__TestSphereAgainstSectorList(staticSector.building, pos, size, a4, false)))
				{
					break;
				}
			}

			if (vehicle)
			{
				if ((retval = CWorld__TestSphereAgainstSectorList(dynamicSector.vehicle, pos, size, a4, false)))
				{
					break;
				}
			}

			if (ped)
			{
				if ((retval = CWorld__TestSphereAgainstSectorList(dynamicSector.ped, pos, size, a4, false)))
				{
					break;
				}
			}

			if (object)
			{
				if ((retval = CWorld__TestSphereAgainstSectorList(dynamicSector.object, pos, size, a4, a10)))
				{
					break;
				}
			}

			if (dummy)
			{
				if ((retval = CWorld__TestSphereAgainstSectorList(staticSector.dummy, pos, size, a4, false)))
				{
					break;
				}
			}
		}
	}

	return retval;
}

int WRAPPER CWorld__GetIsLineOfSightClearSector(CSector& sector, CRepeatSector& repeatSector, CColLine&, bool, bool, bool, bool, bool, int, bool) { EAXJMP(0x568AD0); }

int CWorld__GetIsLineOfSightClearSectorInt(int sX, int sY, const CVector& from, const CVector& to, bool a3, bool a4, bool a5, bool a6, bool a7, int a8, bool a9)
{
	auto& staticSector = GetSector(sX, sY);
	auto& dynamicSector = GetRepeatSector(sX, sY);

	CColLine line;
	line.from = from;
	line.to = to;

	return CWorld__GetIsLineOfSightClearSector(staticSector, dynamicSector, line, a3, a4, a5, a6, a7, a8, a9);
}

// TODO: figure out 'diagonal line' optimizations
int CWorld__GetIsLineOfSightClear(const CVector& from, const CVector& to, bool a3, bool a4, bool a5, bool a6, bool a7, int a8, bool a9)
{
	IncrementScanCode();

	SectorPos fromPos = GetSectorPos(from.x, from.y);
	SectorPos toPos = GetSectorPos(to.x, to.y);

	if (toPos.x == fromPos.x)
	{
		if (fromPos.y == toPos.y)
		{
			return CWorld__GetIsLineOfSightClearSectorInt(toPos.x, toPos.y, from, to, a3, a4, a5, a6, a7, a8, a9);
		}
		else if (fromPos.y < toPos.y)
		{
			for (int y = fromPos.y; y <= toPos.y; y++)
			{
				if (!CWorld__GetIsLineOfSightClearSectorInt(toPos.x, y, from, to, a3, a4, a5, a6, a7, a8, a9))
				{
					return 0;
				}
			}

			return 1;
		}
		else if (fromPos.y > toPos.y)
		{
			for (int y = fromPos.y; y >= toPos.y; y--)
			{
				if (!CWorld__GetIsLineOfSightClearSectorInt(toPos.x, y, from, to, a3, a4, a5, a6, a7, a8, a9))
				{
					return 0;
				}
			}

			return 1;
		}
	}
	else if (fromPos.x < toPos.x)
	{
		if (toPos.y == fromPos.y)
		{
			for (int x = fromPos.x; x <= toPos.x; x++)
			{
				if (!CWorld__GetIsLineOfSightClearSectorInt(x, fromPos.y, from, to, a3, a4, a5, a6, a7, a8, a9))
				{
					return 0;
				}
			}

			return 1;
		}
		else if (fromPos.y < toPos.y)
		{
			for (int y = fromPos.y; y <= toPos.y; y++)
			{
				for (int x = fromPos.x; x <= toPos.x; x++)
				{
					if (!CWorld__GetIsLineOfSightClearSectorInt(x, y, from, to, a3, a4, a5, a6, a7, a8, a9))
					{
						return 0;
					}
				}
			}

			return 1;
		}
		else if (fromPos.y > toPos.y)
		{
			for (int y = fromPos.y; y >= toPos.y; y--)
			{
				for (int x = fromPos.x; x <= toPos.x; x++)
				{
					if (!CWorld__GetIsLineOfSightClearSectorInt(x, y, from, to, a3, a4, a5, a6, a7, a8, a9))
					{
						return 0;
					}
				}
			}

			return 1;
		}
	}
	else
	{
		if (toPos.y == fromPos.y)
		{
			for (int x = fromPos.x; x >= toPos.x; x--)
			{
				if (!CWorld__GetIsLineOfSightClearSectorInt(x, fromPos.y, from, to, a3, a4, a5, a6, a7, a8, a9))
				{
					return 0;
				}
			}

			return 1;
		}
		else if (fromPos.y < toPos.y)
		{
			for (int y = fromPos.y; y <= toPos.y; y++)
			{
				for (int x = fromPos.x; x >= toPos.x; x--)
				{
					if (!CWorld__GetIsLineOfSightClearSectorInt(x, y, from, to, a3, a4, a5, a6, a7, a8, a9))
					{
						return 0;
					}
				}
			}

			return 1;
		}
		else if (fromPos.y > toPos.y)
		{
			for (int y = fromPos.y; y >= toPos.y; y--)
			{
				for (int x = fromPos.x; x >= toPos.x; x--)
				{
					if (!CWorld__GetIsLineOfSightClearSectorInt(x, y, from, to, a3, a4, a5, a6, a7, a8, a9))
					{
						return 0;
					}
				}
			}

			return 1;
		}
	}
}

int WRAPPER CWorld__ProcessLineOfSightSector(CSector&, CRepeatSector&, CColLine&, int, float*, CEntity**, bool, bool, bool, bool, bool, int, int) { EAXJMP(0x56B5E0); }

__forceinline int CWorld__ProcessLineOfSightSectorInt(int sX, int sY, const CVector& from, const CVector& to, int a1, float* curFraction, CEntity** fraction, bool a3, bool a4, bool a5, bool a6, bool a7, int a8, int a9)
{
	auto& staticSector = GetSector(sX, sY);
	auto& dynamicSector = GetRepeatSector(sX, sY);

	CColLine line;
	line.from = from;
	line.to = to;

	return CWorld__ProcessLineOfSightSector(staticSector, dynamicSector, line, a1, curFraction, fraction, a3, a4, a5, a6, a7, a8, a9);
}

int CWorld__ProcessLineOfSight(const CVector& from, const CVector& to, int a1, CEntity** entity, bool a3, bool a4, bool a5, bool a6, bool a7, int a8, int a9)
{
	IncrementScanCode();

	SectorPos fromPos = GetSectorPos(from.x, from.y);
	SectorPos toPos = GetSectorPos(to.x, to.y);

	*entity = nullptr;
	*(DWORD*)0xB7CD60 = 0;
	float curFraction = 1.0f;

	if (toPos.x == fromPos.x)
	{
		if (fromPos.y == toPos.y)
		{
			return CWorld__ProcessLineOfSightSectorInt(toPos.x, toPos.y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
		}
		else if (fromPos.y < toPos.y)
		{
			for (int y = fromPos.y; y <= toPos.y; y++)
			{
				CWorld__ProcessLineOfSightSectorInt(toPos.x, y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
			}
		}
		else if (fromPos.y > toPos.y)
		{
			for (int y = fromPos.y; y >= toPos.y; y--)
			{
				CWorld__ProcessLineOfSightSectorInt(toPos.x, y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
			}
		}
	}
	else if (fromPos.x < toPos.x)
	{
		if (toPos.y == fromPos.y)
		{
			for (int x = fromPos.x; x <= toPos.x; x++)
			{
				CWorld__ProcessLineOfSightSectorInt(x, fromPos.y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
			}
		}
		else if (fromPos.y < toPos.y)
		{
			for (int y = fromPos.y; y <= toPos.y; y++)
			{
				for (int x = fromPos.x; x <= toPos.x; x++)
				{
					CWorld__ProcessLineOfSightSectorInt(x, y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
				}
			}
		}
		else if (fromPos.y > toPos.y)
		{
			for (int y = fromPos.y; y >= toPos.y; y--)
			{
				for (int x = fromPos.x; x <= toPos.x; x++)
				{
					CWorld__ProcessLineOfSightSectorInt(x, y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
				}
			}
		}
	}
	else
	{
		if (toPos.y == fromPos.y)
		{
			for (int x = fromPos.x; x >= toPos.x; x--)
			{
				CWorld__ProcessLineOfSightSectorInt(x, fromPos.y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
			}
		}
		else if (fromPos.y < toPos.y)
		{
			for (int y = fromPos.y; y <= toPos.y; y++)
			{
				for (int x = fromPos.x; x >= toPos.x; x--)
				{
					CWorld__ProcessLineOfSightSectorInt(x, y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
				}
			}
		}
		else if (fromPos.y > toPos.y)
		{
			for (int y = fromPos.y; y >= toPos.y; y--)
			{
				for (int x = fromPos.x; x >= toPos.x; x--)
				{
					CWorld__ProcessLineOfSightSectorInt(x, y, from, to, a1, &curFraction, entity, a3, a4, a5, a6, a7, a8, a9);
				}
			}
		}
	}

	return (curFraction < 1.0f);
}

void __declspec(naked) fixupMov70B95C()
{
	__asm
	{
		mov edx, dword ptr ds:[0C4A05Ch + esi]
		retn
	}
}

struct ScanData
{
	CPtrList* building;
	CPtrList* object;
	CPtrList* ped;
	CPtrList* vehicle;
	CPtrList* dummy;
};

ScanData& g_scanData = *(ScanData*)0xC8E0C8;

void SetUpSectorScan(int sX, int sY)
{
	if (sX < 0 || sY < 0 || sX > g_sectorCount1 || sY > g_sectorCount1)
	{
		auto& repeatSector = GetRepeatSector(sX, sY);

		g_scanData.building = nullptr;
		g_scanData.dummy = nullptr;
		g_scanData.object = &repeatSector.object;
		g_scanData.ped = &repeatSector.ped;
		g_scanData.vehicle = &repeatSector.vehicle;
	}
	else
	{
		auto& sector = GetSector(sX, sY);
		auto& repeatSector = GetRepeatSector(sX, sY);

		g_scanData.building = &sector.building;
		g_scanData.dummy = &sector.dummy;
		g_scanData.object = &repeatSector.object;
		g_scanData.ped = &repeatSector.ped;
		g_scanData.vehicle = &repeatSector.vehicle;
	}
}

bool ScanSectorList_Condition(int sX, int sY)
{
	float x = (sX - g_sectorOffset) * SECTOR_SIZE + 25.0f - *(float*)0xB76870;
	float y = (sY - g_sectorOffset) * SECTOR_SIZE + 25.0f - *(float*)0xB76874;

	if ((x * x + y * y) < 10000.0f)
	{
		return 1;
	}

	float dir = atan2f(-x, y) - *(float*)0xB7684C;
	float res = ((float(*)(float))0x53CB50)(dir);

	if (fabs(res) < 0.36f)
	{
		return 1;
	}

	return 0;
}

void WRAPPER ScanSectorList_ConditionSet()
{
	__asm
	{
		mov esi, [esp + 18h + 4]

		push edi
		
		mov edi, [esp + 1Ch + 8]

		push edi
		push esi

		call ScanSectorList_Condition

		add esp, 8h

		mov bl, al

		push 5548D8h
		retn
	}
}

void CStreaming__AddLodsToRequestList(const CVector& pos, uint32_t a2)
{
	float lodSize = *(float*)0xB76848;

	float minX = pos.x - lodSize;
	float minY = pos.y - lodSize;
	float maxX = pos.x + lodSize;
	float maxY = pos.y + lodSize;

	SectorPos minPos = GetLodSectorPos(minX, minY);
	SectorPos maxPos = GetLodSectorPos(maxX, maxY);

	IncrementScanCode();

	for (int x = minPos.x; x <= maxPos.x; x++)
	{
		for (int y = minPos.y; y <= maxPos.y; y++)
		{
			CPtrList& ptrList = GetLodSector(x, y);

			CStreaming__ProcessEntitiesInSectorList(ptrList, pos.x, pos.y, minX, minY, maxX, maxY, lodSize, a2);
		}
	}
}

int WRAPPER CRenderer__SetupBigBuildingVisibility(CEntity* entity, float&) { EAXJMP(0x554650); }
void WRAPPER CRenderer__AddEntityToRenderList(CEntity* entity, float) { EAXJMP(0x5534B0); }
void WRAPPER CStreaming__RequestModel(int modelIdx, int priority) { EAXJMP(0x4087E0); }

void CRenderer__ScanBigBuildingList(int sX, int sY)
{
	if (sX >= 0 && sX < g_lodSectorCount && sY >= 0 && sY < g_lodSectorCount)
	{
		CPtrList& sector = GetLodSector(sX, sY);

		float x = (sX - g_lodSectorOffset) * LOD_SECTOR_SIZE + (LOD_SECTOR_SIZE / 2.0f) - *(float*)0xB76870;
		float y = (sY - g_lodSectorOffset) * LOD_SECTOR_SIZE + (LOD_SECTOR_SIZE / 2.0f) - *(float*)0xB76874;

		bool request = false;

		if ((x * x + y * y) < 80000.0f)
		{
			request = true;
		}
		else
		{
			auto dir = atan2(-x, y);
			auto dirRel = dir - *(float*)0xB7684C;
			float res = ((float(*)(float))0x53CB50)(dirRel);

			if (fabs(res) < 0.7f)
			{
				request = true;
			}
		}

		ForEachEntity(sector.sNode, [&] (CEntity* entity)
		{
			if (entity->m_nScanCode != CWorld__ms_currentScanCode)
			{
				entity->m_nScanCode = CWorld__ms_currentScanCode;

				float outFloat;
				int result = CRenderer__SetupBigBuildingVisibility(entity, outFloat);

				if (result != 1)
				{
					if (result == 3 && !*(BYTE*)0x9654B0) // streaming disabled
					{
						if (request)
						{
							CStreaming__RequestModel(entity->m_nModelIndex, 0);
						}
					}
				}
				else
				{
					outFloat += 0.01f;

					CRenderer__AddEntityToRenderList(entity, sqrtf(x * x + y * y));

					entity->bOffscreen = false;
				}
			}
		});
	}
}

void PatchRW();

#include "ScriptRuntime.h"

void ScriptRuntimeInit();

/*DWORD WINAPI hi(LPVOID)
{
	auto rt = new HostilityScriptRuntime();
	rt->Init();
	rt->StartTestScript();

	while (true)
	{
		Sleep(15);

		rt->Process();
	}

	return 0;
}*/

void InitializeCode()
{
	//CreateThread(0, 0, hi, 0, 0, 0);
#ifdef LEGACY_SCRIPT_RUNTIME
	ScriptRuntimeInit();
#endif

	// set the sector list based on settings
	int sectorCount = (SECTOR_MAX_COORD - SECTOR_MIN_COORD) / SECTOR_SIZE;
	int sectorOffset = ((((SECTOR_MIN_COORD + SECTOR_MAX_COORD) / 2) - SECTOR_MIN_COORD) / SECTOR_SIZE);

	g_sectors = new CSector[sectorCount * sectorCount];
	memset(g_sectors, 0, sizeof(CSector) * sectorCount * sectorCount);

	//g_sectors = (CSector*)0xB7D0B8;

	//g_repeatSectors = new CStreamSectorDynamic[sectorCount * sectorCount];
	g_sectorCount = sectorCount;
	g_sectorCount1 = sectorCount - 1;
	g_sectorOffset = sectorOffset;

	int lodSectorCount = (SECTOR_MAX_COORD - SECTOR_MIN_COORD) / LOD_SECTOR_SIZE;
	int lodSectorOffset = ((((SECTOR_MIN_COORD + SECTOR_MAX_COORD) / 2) - SECTOR_MIN_COORD) / LOD_SECTOR_SIZE);

	g_lodSectors = new CPtrList[lodSectorCount * lodSectorCount];
	memset(g_lodSectors, 0, sizeof(CPtrList) * lodSectorCount * lodSectorCount);

	g_lodSectorCount = lodSectorCount;
	g_lodSectorCount1 = lodSectorCount - 1;
	g_lodSectorOffset = lodSectorOffset;

	// lod hooks
	MemoryVP::InjectHook(0x40C520, CStreaming__AddLodsToRequestList);

	MemoryVP::InjectHook(0x554B10, CRenderer__ScanBigBuildingList);

	auto quickPatchLod = [&] (int funcStart, int funcEnd)
	{
		static float g_1LodSectorSize = 1.0f / LOD_SECTOR_SIZE;
		static float g_lodSectorOffsetFlt = g_lodSectorOffset;

		for (char* ptr = (char*)funcStart; ptr <= (char*)funcEnd; ptr++)
		{
			if (!memcmp(ptr, "\xD8\x0D\x4C\x8B\x85\x00", 6))
			{
				MemoryVP::Patch<float*>(&ptr[2], &g_1LodSectorSize);
			}

			if (!memcmp(ptr, "\xD8\x05\x48\x8B\x85\x00", 6))
			{
				MemoryVP::Patch<float*>(&ptr[2], &g_lodSectorOffsetFlt);
			}
		}
	};

	quickPatchLod(0x555563, 0x55566D);

	MemoryVP::Patch(0x56405B, g_lodSectors);
	MemoryVP::Patch(0x56409E, &g_lodSectors[lodSectorCount * lodSectorCount]);
	MemoryVP::Patch(0x56409E, &g_lodSectors[lodSectorCount * lodSectorCount]);

	// hooks
	
	// unknown function, inlined in SAMobile
	MemoryVP::InjectHook(0x407260, CWorld__GetSector, PATCH_JUMP);

	// CStreaming::InstanceLoadedModels
	MemoryVP::InjectHook(0x4084F0, CStreaming__InstanceLoadedModels, PATCH_JUMP);

	// CStreaming::AddModelsToRequestList
	MemoryVP::InjectHook(0x40D3F0, CStreaming__AddModelsToRequestList, PATCH_JUMP);

	// CStreaming::DeleteAllRwObjects
	MemoryVP::InjectHook(0x4090A0, CStreaming__DeleteAllRwObjects, PATCH_JUMP);

	// CStreaming::DeleteRwObjectsAfterDeath
	MemoryVP::InjectHook(0x409210, CStreaming__DeleteRwObjectsAfterDeath, PATCH_JUMP);

	// CStreaming::DeleteRwObjectsBehindCamera, a hell of a function
	MemoryVP::InjectHook(0x40D7C0, CStreaming__DeleteRwObjectsBehindCamera, PATCH_JUMP);

	// CCollision::BuildCacheOfCameraCollision coordinate-to-sector conversion
	MemoryVP::InjectHook(0x41AE2B, CCollision__BuildCacheOfCameraCollisionHook, PATCH_JUMP);

	// CCollision::CheckCameraCollisionBuildings clamping
	MemoryVP::InjectHook(0x41A831, CCollision__CheckCameraCollisionBuildings_clampX, PATCH_JUMP);
	MemoryVP::InjectHook(0x41A849, CCollision__CheckCameraCollisionBuildings_clampY, PATCH_JUMP);

	// above function, sector array
	MemoryVP::Patch(0x41A85D, g_sectors);
	MemoryVP::Patch(0x41A864, g_sectors);

	// CEntity::Add
	InjectMethodVP(0x5347D0, CEntity::Add_CRect, PATCH_JUMP);

	// CEntity::Remove
	InjectMethodVP(0x534AE0, CEntity::Remove, PATCH_JUMP);

	// FIXME: following two need their callers altered too
	// CPhysical::ProcessCollisionSectorList
	MemoryVP::Nop(0x5466F4, 10);
	MemoryVP::Nop(0x54670F, 17);

	MemoryVP::InjectHook(0x5466F4, genericClamp<Register::Esi, false>, PATCH_CALL);
	MemoryVP::InjectHook(0x54670F, genericClamp<Register::Ecx, true>, PATCH_CALL);

	MemoryVP::InjectHook(0x54670F + 5, ProcessCollisionSectorList_EvilTail, PATCH_CALL);

	MemoryVP::Nop(0x54671D, 3);

	MemoryVP::Patch(0x54673B, g_sectors);

	// CPhysical::ProcessShiftSectorList
	MemoryVP::Nop(0x54BAF6, 10);
	MemoryVP::Nop(0x54BB11, 13); // including the imul

	MemoryVP::InjectHook(0x54BAF6, genericClamp<Register::Edi, false>, PATCH_CALL);
	MemoryVP::InjectHook(0x54BB11, genericClamp<Register::Ecx, true>, PATCH_CALL);

	MemoryVP::Patch(0x54BB26, g_sectors);

	// CWorld::ClearScanCodes
	MemoryVP::Patch(0x563471, g_sectors);
	MemoryVP::Patch(0x5634A6, g_sectors + (sectorCount * sectorCount));

	// CWorld::ShutDown
	MemoryVP::Patch(0x564172, g_sectors);
	MemoryVP::Patch(0x564220, g_sectors);

	MemoryVP::Patch(0x56420F, g_sectors + (sectorCount * sectorCount));
	MemoryVP::Patch(0x564283, g_sectors + (sectorCount * sectorCount));

	// CWorld::[unknown]
	MemoryVP::InjectHook(0x564A20, CWorld__FindObjectsInRange);

	// CWorld::FindObjectsOfTypeInRange
	MemoryVP::InjectHook(0x564C70, CWorld__FindObjectsOfTypeInRange);

	// CWorld::ProcessVerticalLine
	MemoryVP::InjectHook(0x5674E0, CWorld__ProcessVerticalLine);

	// CWorld::FindObjectsKindaColliding
	MemoryVP::InjectHook(0x568B80, CWorld__FindObjectsKindaColliding);

	// CWorld::FindObjectsIntersectingCube
	MemoryVP::InjectHook(0x568DD0, CWorld__FindObjectsIntersectingCube);

	// CWorld::FindObjectsIntersectingAngledCollisionBox
	MemoryVP::InjectHook(0x568FF0, CWorld__FindObjectsIntersectingAngledCollisionBox);

	// CWorld::FindNearestObjectOfType
	MemoryVP::InjectHook(0x5693F0, CWorld__FindNearestObjectOfType);

	// CWorld::TestSphereAgainstWorld
	MemoryVP::InjectHook(0x569E20, CWorld__TestSphereAgainstWorld);

	// CWorld::GetIsLineOfSightClear
	MemoryVP::InjectHook(0x56A490, CWorld__GetIsLineOfSightClear);

	// CWorld::ProcessLineOfSight
	MemoryVP::InjectHook(0x56BA00, CWorld__ProcessLineOfSight);

	// mhm
	static float g_1SectorSize = 1.0f / SECTOR_SIZE;
	static float g_sectorOffsetFlt = g_sectorOffset;

	// unknown func, manual patch
	MemoryVP::Patch<float*>(0x5DC5F3, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x5DC5F9, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x5DC649, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x5DC64F, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x5DC691, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x5DC697, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x5DC6E2, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x5DC6E8, &g_sectorOffsetFlt);

	MemoryVP::Patch(0x5DC6B0, g_sectorCount1);

	MemoryVP::Nop(0x5DC78A, 10);
	MemoryVP::InjectHook(0x5DC78A, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x5DC798, 13);
	MemoryVP::InjectHook(0x5DC798, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::Patch(0x5DC7AA, g_sectors);

	// unknown func #2, manual patch
	MemoryVP::Patch<float*>(0x606196, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x60619C, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x6061D0, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x6061D6, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x606215, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x60621B, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x606250, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x606256, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x60628C, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x606292, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x6062CB, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x6062D1, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x60630C, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x606312, &g_sectorOffsetFlt);

	MemoryVP::Patch(0x6062A7, g_sectorCount1);

	MemoryVP::Nop(0x6063AA, 10);
	MemoryVP::InjectHook(0x6063AA, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x6063B8, 13);
	MemoryVP::InjectHook(0x6063B8, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::Patch(0x6063CA, g_sectors);

	// unknown func #3, manual patch
	MemoryVP::Patch<float*>(0x67FE34, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x67FE3A, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x67FE5B, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x67FE61, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x67FE80, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x67FE86, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x67FEA7, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x67FEAD, &g_sectorOffsetFlt);

	MemoryVP::Nop(0x67FF40, 10);
	MemoryVP::InjectHook(0x67FF40, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x67FF4E, 13);
	MemoryVP::InjectHook(0x67FF4E, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::Patch(0x67FF60, g_sectors);

	// unknown func #4, manual patch
	MemoryVP::Patch<float*>(0x699BA4, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x699BAE, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x699BF0, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x699BF6, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x699C41, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x699C47, &g_sectorOffsetFlt);

	MemoryVP::Patch<float*>(0x699B5C, &g_1SectorSize);
	MemoryVP::Patch<float*>(0x699B62, &g_sectorOffsetFlt);

	MemoryVP::Patch(0x699C0F, g_sectorCount1);

	MemoryVP::Nop(0x699CDA, 10);
	MemoryVP::InjectHook(0x699CDA, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x699CE6, 13);
	MemoryVP::InjectHook(0x699CE6, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::Patch(0x699CF8, g_sectors);
	MemoryVP::Patch(0x699D01, g_sectors);

	auto quickPatch = [&](int funcStart, int funcEnd)
	{
		for (char* ptr = (char*)funcStart; ptr <= (char*)funcEnd; ptr++)
		{
			if (!memcmp(ptr, "\xD8\x0D\x38\x8B\x85\x00", 6))
			{
				MemoryVP::Patch<float*>(&ptr[2], &g_1SectorSize);
			}

			if (!memcmp(ptr, "\xD8\x05\x34\x8B\x85\x00", 6))
			{
				MemoryVP::Patch<float*>(&ptr[2], &g_sectorOffsetFlt);
			}
		}
	};

	// unknown func #5, getting boring
	quickPatch(0x6E2E50, 0x6E326F);

	// hard-to-patch jump that will be nullified later on
	MemoryVP::Nop(0x6E3075, 2);
	MemoryVP::Nop(0x6E30D6, 2);

	MemoryVP::Nop(0x6E317E, 10);
	MemoryVP::InjectHook(0x6E317E, genericClamp<Register::Edx, false>, PATCH_CALL);

	MemoryVP::Nop(0x6E318C, 13);
	MemoryVP::InjectHook(0x6E318C, genericClamp<Register::Ecx, true>, PATCH_CALL);

	MemoryVP::Patch(0x6E31AD, g_sectors);

	// render stored shadows or so, getting more boring by the minute
	quickPatch(0x70AADC, 0x70AC1D);

	MemoryVP::Nop(0x70ABA5, 2);
	MemoryVP::Nop(0x70ABF6, 2);

	MemoryVP::Nop(0x70AC90, 10);
	MemoryVP::InjectHook(0x70AC90, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x70AC9E, 13);
	MemoryVP::InjectHook(0x70AC9E, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::Patch(0x70ACB5, g_sectors);

	// more function, #7
	quickPatch(0x70B7B4, 0x70B8EC);

	MemoryVP::Patch(0x70B877, g_sectorCount1);

	MemoryVP::Nop(0x70B94E, 10);
	MemoryVP::InjectHook(0x70B94E, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x70B95C, 13 + 6);
	MemoryVP::InjectHook(0x70B95C, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::InjectHook(0x70B95C + 5, fixupMov70B95C, PATCH_CALL);

	MemoryVP::Patch(0x70B9BD, g_sectors);

	// and the final func for main sectors...
	quickPatch(0x711975, 0x711AC2);

	MemoryVP::Nop(0x711A40, 2);
	MemoryVP::Nop(0x711A91, 2);

	MemoryVP::Nop(0x711B12, 10);
	MemoryVP::InjectHook(0x711B12, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x711B20, 13);
	MemoryVP::InjectHook(0x711B20, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::Patch(0x711B32, g_sectors);

	// or not, this one came in between; CTaskSimpleClimb::ScanToGrab
	quickPatch(0x67FE2F, 0x67FEBC);

	MemoryVP::Nop(0x67FF40, 10);
	MemoryVP::InjectHook(0x67FF40, genericClamp<Register::Ecx, false>, PATCH_CALL);

	MemoryVP::Nop(0x67FF4E, 13);
	MemoryVP::InjectHook(0x67FF4E, genericClamp<Register::Eax, true>, PATCH_CALL);

	MemoryVP::Patch(0x67FF60, g_sectors);

	// ScanWorld callers
	quickPatch(0x554DE9, 0x554E7A);
	quickPatch(0x555445, 0x55556D);
	quickPatch(0x555BF7, 0x555C9B);
	quickPatch(0x555BF7, 0x555C9B);

	// one ScanWorld CB relating to repeat sectors
	MemoryVP::Nop(0x555915, 2);
	MemoryVP::Nop(0x55591A, 2);

	// inlined function on mobile; sets up some scan data
	MemoryVP::InjectHook(0x553540, SetUpSectorScan);

	// CRenderer::ScanSectorList 'conversion back to coords' and corresponding condition
	MemoryVP::InjectHook(0x55484A, ScanSectorList_ConditionSet);

	// more PtrNodeDouble!
	//MemoryVP::Patch(0x550F82, 0x1900 * 8);

	// ProcessCollisionSectorList caller
	quickPatch(0x54DC0F, 0x54DC8F);

	// ProcessShiftSectorList caller #1
	quickPatch(0x54D9E4, 0x54DA66);

	// "                    " caller #2
	quickPatch(0x54DD07, 0x54DD8A);

	// some stuff only using repeat sectors
	quickPatch(0x57066C, 0x5706FB); // sub_56F8D0

	// CPhysical::RemoveAndAdd
	quickPatch(0x542599, 0x542612);

	// CPhysical::Add
	quickPatch(0x544A76, 0x544AFC);

	// more funcs with repeat sectors
	quickPatch(0x43264A, 0x43279D);

	MemoryVP::Nop(0x432725, 2);
	MemoryVP::Nop(0x432771, 2);

	quickPatch(0x6123CC, 0x612596);

	MemoryVP::Patch(0x6124DB, g_sectorCount);

	// vehicle ped danger
	quickPatch(0x42CEC8, 0x42CFF6);

	MemoryVP::Nop(0x42CFCF, 2);
	MemoryVP::Nop(0x42CF83, 2);

	// vehicle ped/obstacle scans
	quickPatch(0x434489, 0x4345BB);

	MemoryVP::Nop(0x43454C, 2);
	MemoryVP::Nop(0x434598, 2);
	
	// CEntityScanner
	quickPatch(0x5FFAC5, 0x5FFC39);

	MemoryVP::Nop(0x5FFBA5, 2);
	MemoryVP::Nop(0x5FFC03, 2);

	// CWorld::TriggerExplosion
	quickPatch(0x56B79A, 0x56B82A);

	// UNRELATED STUFF: PATH DIVISORS
	PatchPathDivisors();

	PatchStreamingIndices();

	PatchTimedObjects();

#ifdef GL_RENDERER
	PatchRW();
#endif
}

BOOL WINAPI DllMain(HINSTANCE dll, DWORD reason, LPVOID reserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		InitializeCode();
	}

	return TRUE;
}