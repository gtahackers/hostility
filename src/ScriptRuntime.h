#include "StdInc.h"
#include <functional>
#include <memory>
#include <stack>
#include <mmsystem.h>

class HostilityScript;

class ScriptChain
{
public:
	typedef std::function<void(HostilityScript*)> ChainTaskCB;

	struct ChainTask
	{
		ChainTaskCB cb;
		uint32_t command;
	};

private:
	std::unordered_map<uint32_t, std::vector<ChainTask>> m_tasks;

public:
	void Add(uint32_t command, ChainTaskCB task);

	void ProcessTasks(uint16_t command, HostilityScript* script);
};

class ScriptChainFactory
{
public:
	virtual std::shared_ptr<ScriptChain> GetChain() = 0;
};

ScriptChainFactory* GetScriptChainFactory();

class ScriptParameter
{
private:
	union Value
	{
		int integer;
		float number;
		char string[8];
	} m_value;

	HostilityScript* m_script;

	enum class Type
	{
		Integer = 1,
		GlobalVar = 2,
		LocalVar = 3,
		Integer8 = 4,
		Integer16 = 5,
		Float = 6,
		String = 7
	} m_type;

public:
	ScriptParameter() {}

	ScriptParameter(int value)
	{
		m_type = Type::Integer;
		m_value.integer = value;
	}

	ScriptParameter(float value)
	{
		m_type = Type::Float;
		m_value.number = value;
	}

	ScriptParameter(const char* value)
	{
		m_type = Type::String;
		strncpy(m_value.string, value, 8);
		m_value.string[7] = 0;
	}

	void ReadFromScript(HostilityScript* script);

	int GetIntValue();
	float GetFloatValue();
	const char* GetStringValue();

	void SetIntValue(int integer);
	void SetStringValue(const char* string);
	void SetFloatValue(float value);

	std::string GetId();

	int AddToBuffer(char* buffer, int i);
};

extern ScriptParameter scriptParameters[32];
extern int numScriptParameters;

union ScriptVar
{
	int integer;
	float number;
};

class HostilityScriptRuntime;

class HostilityScript
{
private:
	HostilityScriptRuntime* m_runtime;

	char* m_baseScript;
	int m_curPC;

	ScriptVar m_localVars[34];

	bool m_compareFlag;

	bool m_waiting;

	int m_andOrValue;

	std::stack<int> m_returnStack;

	std::vector<std::function<void()>> m_tailCBs;

	int m_wakeTime;

	bool m_rechain;

	std::string m_name;

private:
	bool ProcessOneCommand();

public:
	void Init(HostilityScriptRuntime* runtime);

	void AddScriptToList(HostilityScript** list);
	void RemoveScriptFromList(HostilityScript** list);

	void CollectParameters(int numParams);

	void InvokeGameCommand(int gameID);

	void Process();

	void SetTailCB(std::function<void()> cb);

	void SetAndOr(int value);

	void GoSub(int sub);
	void Return();

	void RechainCommand(uint32_t commandID);

	// TODO: add time stuff
	inline void Wait(int time) { m_waiting = true; m_wakeTime = time; }

	inline HostilityScriptRuntime* GetRuntime() { return m_runtime; }

	inline char* GetScript() { return m_baseScript; }
	inline int GetPC() { return m_curPC; }
	inline void IncrementPC(int by) { m_curPC += by; }
	inline void SetPC(int value)
	{
		if (value >= 0)
		{
			m_curPC = value;
		}
		else
		{
			m_curPC = 225512 - value;
		}
	}

	inline void SetName(const char* value) { m_name = value; }

	inline bool GetCompareFlag() { return m_compareFlag; }
	inline void SetCompareFlag(bool val) { m_compareFlag = val; }

	inline ScriptVar* GetLocalVars() { return m_localVars; }
};

class HostilityScriptRuntime
{
private:
	char m_scriptSpace[225512 + 35000];

	HostilityScript* m_pIdleScripts[128];
	HostilityScript* m_pActiveScripts[128];

	HostilityScript m_scriptsArray[128];

	std::shared_ptr<ScriptChain> m_chain;

	int m_mainScriptSize;
	int m_largestMissionScriptSize;
	int m_numberOfMissionScripts;

	int m_multiScriptArray[100];

private:
	void ReadMultiScriptFileOffsetsFromScript();

public:
	HostilityScriptRuntime();

	void Init();

	void StartTestScript();

	void StartNewScript(int offset);

	void StopScript(HostilityScript* script);

	void Process();

	int GetMainStart();

	inline int GetMultiScriptStart(int idx) { return m_multiScriptArray[idx]; }

	inline std::shared_ptr<ScriptChain> GetChain() { return m_chain; }

	inline char* GetScriptSpace() { return m_scriptSpace; }
};